# -*- coding: utf-8 -*-
from sys import exit
import pymongo

class Database:
    __connectionString = None
    __fotoDBPath = None
    __databaseName = None

    def __init__(self, cStr, DBName, fDBPath):
        self.__setConnectionString(cStr)
        self.__setFotoDBPath(fDBPath)
        self.__setDatabaseName(DBName)

    def __setConnectionString(self, cStr):
        if type(cStr) != str:
            raise NameError('Wrong type on cStr parameter.')
            exit(1)
        if len(cStr) == 0:
            raise NameError('Empty string on cStr parameter.')
            exit(1)
        self.__connectionString = cStr

    def __setDatabaseName(self, name):
        if type(name) != str:
            raise NameError('Wrong type on name parameter.')
            exit(1)
        if len(name) == 0:
            raise NameError('Empty string on name parameter.')
            exit(1)
        self.__databaseName = name

    def __setFotoDBPath(self, fDBPath):
        if type(fDBPath) != str:
            raise NameError('Wrong type on fDBPath parameter.')
            exit(1)
        if len(fDBPath) == 0:
            raise NameError('Empty string on fDBPath parameter.')
            exit(1)
        self.__fotoDBPath = fDBPath

    def createEmptyDocumment(self, collectionName):
        connection = self.__connectOnDB()
        db = self.__getDatabase(connection)
        docummentID = None
        try:
            docummentID = db[collectionName].insert_one({})
        except:
            raise NameError('Não foi possível inserir o documento.')
        connection.close()
        return docummentID

    def __connectOnDB(self):
        return pymongo.MongoClient(self.__connectionString)

    def __getDatabase(self, connection):
        return connection[self.__databaseName]
