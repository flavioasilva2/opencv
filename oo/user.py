from sys import exit

class User:
    __name = None
    __label = None
    __DBid = None
    __id = None
    __fotoArray = None

    def __init__(self, userName):
        self.__setName(userName)

    def __setName(self, name):
        if type(name) != str:
            raise NameError('Wrong type on name parameter.')
            exit(1)
        if len(cStr) == 0:
            raise NameError('Empty string on name parameter.')
            exit(1)
        self.__name = name

    def setLabel(self, label):
        if type(label) != int:
            raise NameError('Wrong type on label parameter.')
            exit(1)
        self.__label = label

    def setDBId(self, id):
        if type(id) != str:
            raise NameError('Wrong type on id parameter.')
            exit(1)
        if len(id) == 0:
            raise NameError('Empty string on id parameter.')
            exit(1)
        self.__DBid = id

    def setId(self, id):
        if type(id) != str:
            raise NameError('Wrong type on id parameter.')
            exit(1)
        if len(id) == 0:
            raise NameError('Empty string on id parameter.')
            exit(1)
