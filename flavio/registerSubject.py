#!/usr/bin/env python

import cv2
import sys
import numpy
# import getopt
import os

miniScale = 2
faceRectColor = (0, 255, 0)
# eyeRectColor = (255, 0, 0)

def drawRects(img, pos, scale, color):
    for x1, y1, x2, y2 in pos:
        cv2.rectangle(img, (x1*scale, y1*scale), ((x2+x1)*scale, (y2+y1)*scale), color, 2)
        # cv2.putText(img,'OpenCV',(x1*scale+10, y1*scale-10), cv2.FONT_HERSHEY_SIMPLEX, 0.7,(0,255,0), 1, cv2.LINE_AA)

def detect(img, cascade):
    out = cascade.detectMultiScale(img, scaleFactor=1.1, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
    if len(out) == 0:
        return []
    return out

def roi(img, rects, scale):
    out = []
    for x1, y1, x2, y2 in rects:
        out.append(img[y1*scale:(y1+y2)*scale, x1*scale:(x1+x2)*scale])
    return out

def saveOnFotoDB(img, subjectName, dbPath):
    if not os.path.exists(dbPath):
        os.mkdir(dbPath)
    subjectDBPath = os.path.join(dbPath, subjectName)
    if not os.path.exists(subjectDBPath):
        os.mkdir(subjectDBPath)
    (sdir, empty, fotos) = os.walk(subjectDBPath).next()
    currentIndex = 0
    if(not(len(fotos)) == 0):
        indices = []
        for name in fotos:
            indices.append(name.split('.')[0])
        indices.sort()
        currentIndex = int(indices.pop()) + 1
    fotoName = os.path.join(subjectDBPath, str(currentIndex) + '.pgm')
    cv2.imwrite(fotoName, img)

def getArgs(arg, args):
    for v in args:
        if (v.split('=')[0] == arg) and (len(v.split('=')) > 1):
            return v.split('=')[1]

if len(sys.argv) != 3:
    print("use os parametros --subject=<nome> e --fotodb=<caminho da galeria de fotos>")
    sys.exit(1)

haarFacePath = "haarcascades/haarcascade_frontalface_alt.xml"
# haarEyePath = "haarcascades/haarcascade_eye.xml"

haarFace = cv2.CascadeClassifier(haarFacePath)
# haarEye = cv2.CascadeClassifier(haarEyePath)

if haarFace.empty() == True:
    print("Falha ao carregar o Cascade Classifier %s" % haarFacePath)
    sys.exit(1)

# if haarEye.empty() == True:
#     print("Falha ao carregar o Cascade Classifier %s" % haarEyePath)
#     sys.exit(1)

cap = cv2.VideoCapture(0)

while(True):
    ret, frame = cap.read()
    toSave = frame.copy()
    # vis = frame.copy()
    frameGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frameGrayEq = cv2.equalizeHist(frameGray)
    miniframeGrayEq = cv2.resize(frameGrayEq, (int(frameGrayEq.shape[1] / miniScale), int(frameGrayEq.shape[0] / miniScale)))
    rects = detect(miniframeGrayEq, haarFace)
    rois = roi(frameGrayEq, rects, miniScale)
    # for i in rois:
    #     print(model.predict(i))
    drawRects(frame, rects, miniScale, faceRectColor)
    # cv2.putText(frame,'Por favor, registre um usuario por vez.',(25, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.7,(0,255,0), 2, cv2.LINE_AA)
    frame=cv2.flip(frame,1,0)
    cv2.imshow('out',frame)
    key = cv2.waitKey(1)
    if key & 0xFF == ord('q'):
        break
    if key & 0xFF == ord('w'):
        if len(rects) > 1:
            print('Por favor, registre um usuario por vez.')
        else:
            imags = roi(toSave, rects, miniScale)
            if len(imags) != 1:
                continue
            fotoDBPath = getArgs('--fotodb', sys.argv)
            subjectName = getArgs('--subject', sys.argv)
            print('Registrando para ' + subjectName)
            saveOnFotoDB(imags[0], subjectName, fotoDBPath)

cap.release()
cv2.destroyAllWindows()
