#!/usr/bin/env python

import cv2
import sys
import os
import numpy

miniScale = 2
faceRectColor = (0, 255, 0)

def detect(img, cascade):
    out = cascade.detectMultiScale(img, scaleFactor=1.1, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
    if len(out) == 0:
        return []
    return out

def getArgs(arg, args):
    for v in args:
        if (v.split('=')[0] == arg) and (len(v.split('=')) > 1):
            return v.split('=')[1]

# def detect(img, cascade):
#     out = cascade.detectMultiScale(img, scaleFactor=1.1, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
#     if len(out) == 0:
#         return []
#     return out

def compare(img, rects, scale, recognizer, db):
    out = []
    imgs = []
    for x1, y1, x2, y2 in rects:
        imgs.append(img[y1*scale:(y1+y2)*scale, x1*scale:(x1+x2)*scale])
    for i in imgs:
        index = imgs.index(i)
        user = {}
        (label, recognitionThreshold) = recognizer.predict(i)
        # recognitionThreshold = recognizer.getThreshold()
        user['coord'] = rects[index]
        userName = getUserByLabel(label, db)
        user['name'] = userName
        user['threshold'] = recognitionThreshold
        out.append(user)
    return out

def loadDB():
    # out = {
    #     user: string,
    #     label: int,
    #     fotos: imgArray
    # }
    load = []
    dbPath = getArgs('--fotodb', sys.argv)
    if not os.path.exists(dbPath):
        print('Nao foi possivel abrir o DB de fotos')
        sys.exit(1)
    (db, usersDBFolders, empty) = os.walk(dbPath).next()
    if os.listdir(dbPath) == []:
        print('No registered users')
        sys.exit(1)
    for u in usersDBFolders:
        userDBFolder = os.path.join(db, u)
        (folder, empty, fotosFromDB) = os.walk(userDBFolder).next()
        tmpUser = {}
        tmpUser['user'] = u
        tmpUser['label'] = usersDBFolders.index(u)
        fotos = []
        for f in fotosFromDB:
            fotoPath = os.path.join(userDBFolder, f)
            fotos.append(cv2.imread(fotoPath, cv2.IMREAD_REDUCED_GRAYSCALE_2))
        tmpUser['fotos'] = fotos
        load.append(tmpUser)
    return load

def getUserByLabel(label, dbObj):
    for u in dbObj:
        if u['label'] == label:
            return u['user']

def draw(frame, toDrawObjs, scale, color):
    for obj in toDrawObjs:
        (x1, y1, x2, y2) = obj['coord']
        cv2.rectangle(frame, (x1*scale, y1*scale), ((x2+x1)*scale, (y2+y1)*scale), color, 2)
        cv2.putText(frame, obj['name'] + ', ' + ("%3.2f" % obj['threshold']) + '%', (x1*scale+10, y1*scale-10), cv2.FONT_HERSHEY_SIMPLEX, 0.7, color, 1, cv2.LINE_AA)

haarFacePath = "haarcascades/haarcascade_frontalface_alt.xml"
haarFace = cv2.CascadeClassifier(haarFacePath)

if len(sys.argv) != 2:
    print('Use o parametro --fotodb=<caminho da galeria de fotos>')
    sys.exit(1)

DB = loadDB()
trainImgs = []
trainLabels = []
for u in DB:
    for uImg in u['fotos']:
        trainImgs.append(uImg)
        trainLabels.append(u['label'])

(Imgs, Labels) = [numpy.array(lis) for lis in [trainImgs, trainLabels]]
model = cv2.face.createLBPHFaceRecognizer()
model.train(Imgs, Labels)

# model.setThreshold()
# print(model.getThreshold())

cap = cv2.VideoCapture(0)

while True:
    (ret, frame) = cap.read()
    frameGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frameGrayEq = cv2.equalizeHist(frameGray)
    miniframeGrayEq = cv2.resize(frameGrayEq, (int(frameGrayEq.shape[1] / miniScale), int(frameGrayEq.shape[0] / miniScale)))
    rects = detect(miniframeGrayEq, haarFace)
    # rois = roi(frameGrayEq, rects, miniScale)
    toDraw = compare(frameGrayEq, rects, miniScale, model, DB)
    draw(frame, toDraw, miniScale, faceRectColor)
    cv2.imshow('out',frame)
    key = cv2.waitKey(1)
    if key & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
