#!/usr/bin/env python

#University of Brasilia - CIC
#Abstract: This program is suposed to recognize and track people on you database system. It is used Mongodb as database system in this program.
#Flavio Amaral e Silva
#Lukas Lorenz de Andrade
#Marcella Pantarotto

import cv2
import classes
#import intaface

def main():
    key = None
    camera = classes.cam()
    pic = classes.picture()
    db = classes.Database()
    us = classes.user()
    while(not(key == 27)):
        frame = camera.getFrame()
        if(not(len(frame) == 0)):
            frame = camera.faceDetection(frame)
            frame = pic.imageProcessing(frame)
            cv2.imshow('OpenCV - Press Esc to exit', frame)
            load = db.loadDB()
            if(len(load) == 0):
                #us.Register(frame, imageRecognition(frame))
                db.saveOnFotoDB()
            pic.imageTraining(frame)
            pic.draw(frame, pic.imageRecognition(frame))
            print(' Do you want to register ?')
            cout << key
            #if(pic.imageRecognition(frame)['threshold']== 0  means person not registered 
            if(pic.imageRecognition(frame)['threshold']== 0 and (key == 59 or key == 79 )):
                adress = us.Register(frame)
                db.saveOnFotoDB()
            flip = cv2.flip(frame,1,0)
            cv2.imshow('OpenCV - Press Esc to exit', frame)
            key = cv2.waitKey(10)
        else: 
            key = 27

    camera.getCamera().release()
    cv2.destroyAllWindows()

main()