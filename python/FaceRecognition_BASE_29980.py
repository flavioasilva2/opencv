#!/usr/bin/env python

#University of Brasilia - CIC
#Abstract: This program is suposed to recognize and track people on you database system. It is used Mongodb as database system in this program.
#Flavio Amaral e Silva
#Lukas Lorenz de Andrade
#Marcella Pantarotto

import cv2
import classes

def main():
    key = None
    camera = classes.cam()
    pic = classes.picture()

    while(not(key == 27)):
        frame = camera.getFrame()
        if(not(frame == None)):
            frame = camera.faceDetection(frame)
            cv2.imshow('OpenCV - Press Esc to exit', frame)
            frame = pic.imageProcessing(frame)
            pic.imageTraining(frame)
            pic.imageRecognition(frame)
            key = cv2.waitKey(10)
        else: 
            key = 27
    camera.getCamera().release()
    cv2.destroyAllWindows()

main()