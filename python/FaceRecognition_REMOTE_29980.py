#!/usr/bin/env python

#University of Brasilia - CIC
#Abstract: This program is suposed to recognize and track people on you database system. It is used Mongodb as database system in this program.
#Flavio Amaral e Silva
#Lukas Lorenz de Andrade
#Marcella Pantarotto

import cv2
import classes


import threading
from time import sleep
from Tkinter import *

import threading

def main():
    key = None
    camera = classes.cam()
    pic = classes.picture()

    # t1 = hwThread('t1', 1)
    # t1.start()

    while(not(key == 27)):
        frame = camera.getFrame()
        if(not(frame == None)):
            frame = camera.faceDetection(frame)
            #cv2.imshow('OpenCV - Press Esc to exit', frame)
            frame = pic.imageProcessing(frame)
            cv2.imshow('OpenCV - Press Esc to exit', frame)
            key = cv2.waitKey(10)
        else:
            key = 27

    # t1.stop()
    camera.webcam.release()
    cv2.destroyAllWindows()
