# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
import sys
import os

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_RegisterClean(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

    def __closeWindow(self):
        self.close()

# ---------------- Method that adds Widgets to the window class --------------------------------------
    def setupUi(self, RegisterClean):
        RegisterClean.setObjectName(_fromUtf8("RegisterClean"))
        RegisterClean.resize(729, 600)
        self.centralwidget = QtGui.QWidget(RegisterClean)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.layoutWidget_8 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_8.setGeometry(QtCore.QRect(178, 70, 521, 111))
        self.layoutWidget_8.setObjectName(_fromUtf8("layoutWidget_8"))
        self.verticalLayout_16 = QtGui.QVBoxLayout(self.layoutWidget_8)
        self.verticalLayout_16.setObjectName(_fromUtf8("verticalLayout_16"))

# ------------------------------- NAME --------------------------------------
        self.nameLabel = QtGui.QLabel(self.layoutWidget_8)
        self.nameLabel.setObjectName(_fromUtf8("nameLabel"))
        self.verticalLayout_16.addWidget(self.nameLabel)

        self.inputName = QtGui.QLineEdit(self.layoutWidget_8)
        self.inputName.setObjectName(_fromUtf8("inputName"))
        self.verticalLayout_16.addWidget(self.inputName)

# -------------------------------- ID -------------------------------------------------
        self.IDLabel = QtGui.QLabel(self.layoutWidget_8)
        self.IDLabel.setObjectName(_fromUtf8("IDLabel"))
        self.verticalLayout_16.addWidget(self.IDLabel)

        self.inputID = QtGui.QLineEdit(self.layoutWidget_8)
        self.inputID.setObjectName(_fromUtf8("inputID"))
        self.verticalLayout_16.addWidget(self.inputID)

# ------------------------------------- TITLE ----------------------------------------------
        self.titleLabel = QtGui.QLabel(self.centralwidget)
        self.titleLabel.setGeometry(QtCore.QRect(258, 10, 311, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("mry_KacstQurn"))
        font.setPointSize(20)
        self.titleLabel.setFont(font)

        self.titleLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.titleLabel.setFrameShape(QtGui.QFrame.Panel)
        self.titleLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.titleLabel.setLineWidth(3)
        self.titleLabel.setTextFormat(QtCore.Qt.AutoText)
        self.titleLabel.setScaledContents(False)
        self.titleLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.titleLabel.setObjectName(_fromUtf8("titleLabel"))

        self.verticalLayoutWidget_3 = QtGui.QWidget(self.centralwidget)
        self.verticalLayoutWidget_3.setGeometry(QtCore.QRect(38, 70, 94, 114))
        self.verticalLayoutWidget_3.setObjectName(_fromUtf8("verticalLayoutWidget_3"))
        self.verticalLayout_12 = QtGui.QVBoxLayout(self.verticalLayoutWidget_3)
        self.verticalLayout_12.setObjectName(_fromUtf8("verticalLayout_12"))

# ------------------------------------- PHOTO ----------------------------------------------
        self.photoLabel = QtGui.QLabel(self.verticalLayoutWidget_3)
        self.photoLabel.setText(_fromUtf8(""))
        self.photoLabel.setPixmap(QtGui.QPixmap(_fromUtf8("homer.png")))
        self.photoLabel.setScaledContents(True)
        self.photoLabel.setObjectName(_fromUtf8("photoLabel"))
        self.verticalLayout_12.addWidget(self.photoLabel)

# ------------------------------------- GROUP ----------------------------------------------
        self.groupLabel = QtGui.QLabel(self.centralwidget)
        self.groupLabel.setGeometry(QtCore.QRect(8, 10, 161, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("mry_KacstQurn"))
        font.setPointSize(17)
        self.groupLabel.setFont(font)

        self.groupLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.groupLabel.setFrameShape(QtGui.QFrame.Box)
        self.groupLabel.setFrameShadow(QtGui.QFrame.Sunken)
        self.groupLabel.setLineWidth(2)
        self.groupLabel.setTextFormat(QtCore.Qt.AutoText)
        self.groupLabel.setScaledContents(False)
        self.groupLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.groupLabel.setObjectName(_fromUtf8("groupLabel"))

        self.layoutWidget_6 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_6.setGeometry(QtCore.QRect(28, 440, 111, 101))
        self.layoutWidget_6.setObjectName(_fromUtf8("layoutWidget_6"))
        self.verticalLayout_14 = QtGui.QVBoxLayout(self.layoutWidget_6)
        self.verticalLayout_14.setObjectName(_fromUtf8("verticalLayout_14"))

# ------------------------------------- BUTTONS ----------------------------------------------
        # SAVE BUTTON
        self.saveButton = QtGui.QPushButton(self.layoutWidget_6)
        self.saveButton.setAcceptDrops(False)
        self.saveButton.setToolTip(_fromUtf8(""))
        self.saveButton.setStatusTip(_fromUtf8(""))
        self.saveButton.setAutoDefault(False)
        self.saveButton.setDefault(True)
        self.saveButton.setObjectName(_fromUtf8("saveButton"))
        self.verticalLayout_14.addWidget(self.saveButton)
        # CANCEL BUTTON
        self.cancelButton = QtGui.QPushButton(self.layoutWidget_6)
        self.cancelButton.setAcceptDrops(False)
        self.cancelButton.setToolTip(_fromUtf8(""))
        self.cancelButton.setStatusTip(_fromUtf8(""))
        self.cancelButton.setObjectName(_fromUtf8("cancelButton"))
        self.cancelButton.clicked.connect(self.__closeWindow)
        self.verticalLayout_14.addWidget(self.cancelButton)

# ------------------------------------- SCHEDUALE ----------------------------------------------
        self.schedualLabel = QtGui.QLabel(self.centralwidget)
        self.schedualLabel.setGeometry(QtCore.QRect(370, 220, 111, 41))
        self.schedualLabel.setObjectName(_fromUtf8("schedualLabel"))

        self.splitter = QtGui.QSplitter(self.centralwidget)
        self.splitter.setGeometry(QtCore.QRect(180, 260, 521, 250))
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.layoutWidget_7 = QtGui.QWidget(self.splitter)
        self.layoutWidget_7.setObjectName(_fromUtf8("layoutWidget_7"))
        self.verticalLayout_15 = QtGui.QVBoxLayout(self.layoutWidget_7)
        self.verticalLayout_15.setObjectName(_fromUtf8("verticalLayout_15"))

        # DAYS LABEL
        self.daysLabel = QtGui.QLabel(self.layoutWidget_7)
        self.daysLabel.setObjectName(_fromUtf8("daysLabel"))
        self.verticalLayout_15.addWidget(self.daysLabel)
        # MONDAY
        self.weekInput1 = QtGui.QLineEdit(self.layoutWidget_7)
        self.weekInput1.setAlignment(QtCore.Qt.AlignCenter)
        self.weekInput1.setReadOnly(True)
        self.weekInput1.setObjectName(_fromUtf8("weekInput1"))
        self.verticalLayout_15.addWidget(self.weekInput1)
        # TUESDAY
        self.weekInput2 = QtGui.QLineEdit(self.layoutWidget_7)
        self.weekInput2.setAlignment(QtCore.Qt.AlignCenter)
        self.weekInput2.setReadOnly(True)
        self.weekInput2.setObjectName(_fromUtf8("weekInput2"))
        self.verticalLayout_15.addWidget(self.weekInput2)
        # WEDNESDAY
        self.weekInput3 = QtGui.QLineEdit(self.layoutWidget_7)
        self.weekInput3.setAlignment(QtCore.Qt.AlignCenter)
        self.weekInput3.setReadOnly(True)
        self.weekInput3.setObjectName(_fromUtf8("weekInput3"))
        self.verticalLayout_15.addWidget(self.weekInput3)
        # THURSDAY
        self.weekInput4 = QtGui.QLineEdit(self.layoutWidget_7)
        self.weekInput4.setAlignment(QtCore.Qt.AlignCenter)
        self.weekInput4.setReadOnly(True)
        self.weekInput4.setObjectName(_fromUtf8("weekInput4"))
        self.verticalLayout_15.addWidget(self.weekInput4)
        # FRIDAY
        self.weekInput5 = QtGui.QLineEdit(self.layoutWidget_7)
        self.weekInput5.setAlignment(QtCore.Qt.AlignCenter)
        self.weekInput5.setReadOnly(True)
        self.weekInput5.setObjectName(_fromUtf8("weekInput5"))
        self.verticalLayout_15.addWidget(self.weekInput5)
        # SATURDAY
        self.weekInput6 = QtGui.QLineEdit(self.layoutWidget_7)
        self.weekInput6.setAlignment(QtCore.Qt.AlignCenter)
        self.weekInput6.setReadOnly(True)
        self.weekInput6.setObjectName(_fromUtf8("weekInput6"))
        self.verticalLayout_15.addWidget(self.weekInput6)
        # SUNDAY
        self.weekInput7 = QtGui.QLineEdit(self.layoutWidget_7)
        self.weekInput7.setAlignment(QtCore.Qt.AlignCenter)
        self.weekInput7.setReadOnly(True)
        self.weekInput7.setObjectName(_fromUtf8("weekInput7"))
        self.verticalLayout_15.addWidget(self.weekInput7)

        self.layoutWidget_5 = QtGui.QWidget(self.splitter)
        self.layoutWidget_5.setObjectName(_fromUtf8("layoutWidget_5"))
        self.verticalLayout_13 = QtGui.QVBoxLayout(self.layoutWidget_5)
        self.verticalLayout_13.setObjectName(_fromUtf8("verticalLayout_13"))

        # TIME LABEL
        self.timeLabel = QtGui.QLabel(self.layoutWidget_5)
        self.timeLabel.setObjectName(_fromUtf8("timeLabel"))
        self.verticalLayout_13.addWidget(self.timeLabel)
        # MONDAY
        self.inputHours1 = QtGui.QLineEdit(self.layoutWidget_5)
        self.inputHours1.setObjectName(_fromUtf8("inputHours1"))
        self.verticalLayout_13.addWidget(self.inputHours1)
        # TUESDAY
        self.inputHours2 = QtGui.QLineEdit(self.layoutWidget_5)
        self.inputHours2.setObjectName(_fromUtf8("inputHours2"))
        self.verticalLayout_13.addWidget(self.inputHours2)
        # WEDNESDAY
        self.inputHours3 = QtGui.QLineEdit(self.layoutWidget_5)
        self.inputHours3.setObjectName(_fromUtf8("inputHours3"))
        self.verticalLayout_13.addWidget(self.inputHours3)
        # THURSDAY
        self.inputHours4 = QtGui.QLineEdit(self.layoutWidget_5)
        self.inputHours4.setObjectName(_fromUtf8("inputHours4"))
        self.verticalLayout_13.addWidget(self.inputHours4)
        # FRIDAY
        self.inputHours5 = QtGui.QLineEdit(self.layoutWidget_5)
        self.inputHours5.setObjectName(_fromUtf8("inputHours5"))
        self.verticalLayout_13.addWidget(self.inputHours5)
        # SATURDAY
        self.inputHours6 = QtGui.QLineEdit(self.layoutWidget_5)
        self.inputHours6.setObjectName(_fromUtf8("inputHours6"))
        self.verticalLayout_13.addWidget(self.inputHours6)
        # SUNDAY
        self.inputHours7 = QtGui.QLineEdit(self.layoutWidget_5)
        self.inputHours7.setObjectName(_fromUtf8("inputHours7"))
        self.verticalLayout_13.addWidget(self.inputHours7)
        RegisterClean.setCentralWidget(self.centralwidget)

# ------------------------------------- STATUS BAR ----------------------------------------------
        self.statusbar = QtGui.QStatusBar(RegisterClean)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        RegisterClean.setStatusBar(self.statusbar)
# ------------------------------------------------------------------------------------------

        self.retranslateUi(RegisterClean)   ## Method that atribuites text to each data
        QtCore.QMetaObject.connectSlotsByName(RegisterClean)

# ---------------------------------- METHOD WITH TEXT ----------------------------------------------
    def retranslateUi(self, RegisterClean):
        RegisterClean.setWindowTitle(_translate("RegisterClean", "Register", None))
        self.nameLabel.setText(_translate("RegisterClean", "Name", None))
        self.IDLabel.setText(_translate("RegisterClean", "ID", None))
        self.titleLabel.setText(_translate("RegisterClean", "NEW REGISTRATION", None))
        self.groupLabel.setText(_translate("RegisterClean", "CLEANER", None))

        self.saveButton.setText(_translate("RegisterClean", "Save", None))
        self.cancelButton.setText(_translate("RegisterClean", "Cancel", None))

        self.schedualLabel.setText(_translate("RegisterClean", "<html><head/><body><p><span style=\" font-size:14pt;\">SCHEDUALE</span></p></body></html>", None))
        self.daysLabel.setText(_translate("RegisterClean", "DAYS", None))
        self.weekInput1.setText(_translate("RegisterClean", "Monday", None))
        self.weekInput2.setText(_translate("RegisterClean", "Tuesday", None))
        self.weekInput3.setText(_translate("RegisterClean", "Wednesday", None))
        self.weekInput4.setText(_translate("RegisterClean", "Thursday", None))
        self.weekInput5.setText(_translate("RegisterClean", "Friday", None))
        self.weekInput6.setText(_translate("RegisterClean", "Saturday", None))
        self.weekInput7.setText(_translate("RegisterClean", "Sunday", None))

        self.timeLabel.setText(_translate("RegisterClean", "TIME", None))
#-----------------------------------------------------------------------------------------------------------
def main():
    app = QtGui.QApplication(sys.argv)
    w = Ui_RegisterClean(None)
    w.show()
    app.exec_()

if __name__ == '__main__':
    main()
