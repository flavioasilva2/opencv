#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtCore, QtGui, uic
import os
import cv2
import numpy as np
import threading
import time
import Queue
import registerMenu
import deleteMenu
import registerStudent
import search
import classes
import accessFree
import accessFreeOther
import accessFreeForeign


q = Queue.Queue()
dictionarySharedObject = {}
global profile
profile = None

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Capture(threading.Thread):
    __deleteButton = None
    __deleteButtonClickable = False
    __registerButton = None
    __registerButtonClickable = False
    __cam = None
    __queue = None
    __width = None
    __height = None
    __fps = None

    def setDeleteButton(self, button):
        self.__deleteButton = button

    def setRegisterButton(self, button):
        self.__registerButton = button

    def __setDeleteButtonEnabled(self):
        if self.__deleteButton != None:
            self.__deleteButtonClickable = True
            self.__deleteButton.setEnabled(self.__deleteButtonClickable)

    def __setDeleteButtonDisabled(self):
        if self.__deleteButton != None:
            self.__deleteButtonClickable = False
            self.__deleteButton.setEnabled(self.__deleteButtonClickable)

    def __setRegisterButtonEnabled(self):
        if self.__registerButton != None:
            self.__registerButtonClickable = True
            self.__registerButton.setEnabled(self.__registerButtonClickable)
    
    def __setRegisterButtonDisabled(self):
        if self.__registerButton != None:
            self.__registerButtonClickable = False
            self.__registerButton.setEnabled(self.__registerButtonClickable)

    def __init__(self, cam, queue, width, height, fps):
        threading.Thread.__init__(self)
        self.__cam = cam
        self.__queue = queue
        self.__width = width
        self.__height = height
        self.__fps = fps

    def run(self):
        # i = 0
        # haarFacePath = "haarcascades/haarcascade_frontalface_alt.xml"
        # haarFace = cv2.CascadeClassifier(haarFacePath)

        # capture = cv2.VideoCapture(0)
        # capture.set(cv2.CAP_PROP_FRAME_WIDTH, self.__width)
        # capture.set(cv2.CAP_PROP_FRAME_HEIGHT, self.__height)
        # capture.set(cv2.CAP_PROP_FPS, self.__fps)

        # tools = DetectionTools()
        while(self.__queue.qsize() < 10):
            img = q.get()
            # if(not(len(img))==0):
            #     face = tools.detect(img, haarFace)
            #     tools.drawRects(img, face, 1, (0, 255, 0))

            if len(img) != 0:
                self.__setDeleteButtonEnabled()
                self.__setRegisterButtonEnabled()
            else:
                self.__setDeleteButtonDisabled()
                self.__setRegisterButtonDisabled()

            if(self.__queue.qsize() < 10):
                self.__queue.put(img)
            else:
                print("oi")
                #capture.release()

# class DetectionTools:
#     def detect(self, frame, cascade):
#         mini = cv2.resize(frame, (int(frame.shape[1] / 2), int(frame.shape[0] / 2)))
#         print(mini)
#         img = cascade.detectMultiScale(mini, scaleFactor=1.1, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
#         if len(img) == 0:
#             return []
#         return img

#     def drawRects(self, img, pos, scale, color):
#         for x1, y1, x2, y2 in pos:
#             cv2.rectangle(img, (x1*scale, y1*scale), ((x2+x1)*scale, (y2+y1)*scale), color, 2)

class OwnImageWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(OwnImageWidget, self).__init__(parent)
        self.image = None

    def setImage(self, image):
        self.image = image
        sz = image.size()
        self.setMinimumSize(sz)
        self.update()

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        if self.image:
            qp.drawImage(QtCore.QPoint(0, 0), self.image)
        qp.end()

class MyWindowClass(QtGui.QMainWindow):
    __captureThread = None
    __captureThreadStarted = False
    __deleteMenuWindow = None
    __accessFreeWindow = None
    __accessFreeForeignWindow = None
    __accessFreeOtherWindow = None
    __accessDeniedWindow = None
    # __registerMenuWindow = None

    def setCaptureThread(self, t):
        self.__captureThread = t

    def getDeleteMenuWindow(self):
        return self.__deleteMenuWindow

    def setDeleteMenuWindow(self, win):
        self.__deleteMenuWindow = win

    def deleteMenuWindowCallback(self):
        self.getDeleteMenuWindow().show()

    #   Access Windows
    def getAccessFreeWindow(self):
        return self.__accessFreeWindow

    def setAccessFreeWindow(self, win):
        self.__accessFreeWindow = win

    def accessFreeWindowCallback(self):
        self.getAccessFreeWindow().show()

    def getAccessFreeForeignWindow(self):
        return self.__accessFreeForeignWindow

    def setAccessFreeForeignWindow(self, win):
        self.__accessFreeForeignWindow = win

    def accessFreeForeignWindowCallback(self):
        self.getAccessFreeForeignWindow().show()

    def getAccessFreeOtherWindow(self):
        return self.__accessFreeOtherWindow

    def setAccessFreeOtherWindow(self, win):
        self.__accessFreeOtherWindow = win

    def accessFreeOtherWindowCallback(self):
        self.getAccessFreeOtherWindow().show()

    def getAccessDeniedWindow(self):
        return self.__accessDeniedWindow

    def setAccessDeniedWindow(self, win):
        self.__accessDeniedWindow = win

    def accessDeniedWindowCallback(self):
        self.getAccessDeniedWindow().show()

    # def getRegisterMenuWindow(self):
    #     return self.__registerMenuWindow
    #
    # def setRegisterMenuWindow(self, win):
    #     self.__registerMenuWindow = win
    #
    # def registerMenuWindowCallback(self):
    #     self.getRegisterMenuWindow().show()

    def __init__(self, parent):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)
        # sem.acquire()
        # self.__captureThread = thread
        # self.__captureThread.start()

        self.window_width = self.ImgWidget.frameSize().width()
        self.window_height = self.ImgWidget.frameSize().height()
        self.ImgWidget = OwnImageWidget(self.ImgWidget)

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(1)

    def __startCaptureThread(self):
        if (self.__captureThreadStarted == False) and (self.__captureThread != None):
            self.__captureThread.start()
            self.__captureThreadStarted = True

    def update_frame(self):
        global q 
        self.__startCaptureThread()
        if not q.empty():
            # frame = q.get()
            # img = frame["img"]
            img = q.get()
            img_height, img_width, img_colors = img.shape
            scale_w = float(self.window_width) / float(img_width)
            scale_h = float(self.window_height) / float(img_height)
            scale = min([scale_w, scale_h])

            if scale == 0:
                scale = 1

            img = cv2.resize(img, None, fx=scale, fy=scale, interpolation = cv2.INTER_CUBIC)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            height, width, bpc = img.shape
            bpl = bpc * width
            image = QtGui.QImage(img.data, width, height, bpl, QtGui.QImage.Format_RGB888)
            self.ImgWidget.setImage(image)
# ----------------------------------------------------------------------------------------------------

# ---------------- Methods that add Widgets to the window class --------------------------------------
    def setupUi(self, Access):
        Access.setObjectName(_fromUtf8("Access"))
        Access.resize(1302, 485)
        self.centralwidget = QtGui.QWidget(Access)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.video = QtGui.QGroupBox(self.centralwidget)
        self.video.setGeometry(QtCore.QRect(20, 20, 641, 391))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.video.sizePolicy().hasHeightForWidth())
        self.video.setSizePolicy(sizePolicy)
        self.video.setObjectName(_fromUtf8("video"))
        self.ImgWidget = QtGui.QWidget(self.video)
        self.ImgWidget.setGeometry(QtCore.QRect(0, 30, 621, 361))
        self.ImgWidget.setObjectName(_fromUtf8("ImgWidget"))

        self.deleteButton = QtGui.QPushButton(self.centralwidget)
        # self.deleteButton.setEnabled(False)
        self.deleteButton.setGeometry(QtCore.QRect(1168, 417, 85, 27))
        self.deleteButton.setToolTip(_fromUtf8(""))
        self.deleteButton.setStatusTip(_fromUtf8(""))
        self.deleteButton.setAutoDefault(False)
        self.deleteButton.setDefault(False)
        self.deleteButton.setFlat(False)
        self.deleteButton.setObjectName(_fromUtf8("deleteButton"))
        self.deleteButton.clicked.connect(self.deleteMenuWindowCallback)

        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(672, 234, 351, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))

        self.registerButton = QtGui.QPushButton(self.centralwidget)
        self.registerButton.setEnabled(False)
        self.registerButton.setGeometry(QtCore.QRect(1077, 417, 85, 27))
        self.registerButton.setCheckable(False)
        self.registerButton.setChecked(False)
        self.registerButton.setObjectName(_fromUtf8("registerButton"))
        # self.registerButton.clicked.connect(self.registerMenuWindowCallback)

        self.line_2 = QtGui.QFrame(self.centralwidget)
        self.line_2.setGeometry(QtCore.QRect(1023, 5, 16, 461))
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.line_3 = QtGui.QFrame(self.centralwidget)
        self.line_3.setGeometry(QtCore.QRect(663, 5, 16, 461))
        self.line_3.setFrameShape(QtGui.QFrame.VLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.accessLabel = QtGui.QLabel(self.centralwidget)
        self.accessLabel.setGeometry(QtCore.QRect(682, 15, 341, 41))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.accessLabel.setPalette(palette)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(25)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.NoAntialias)
        self.accessLabel.setFont(font)
        self.accessLabel.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.accessLabel.setStyleSheet(_fromUtf8("permissionLabel->setStyleSheet(\"color: green\");"))
        self.accessLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.accessLabel.setObjectName(_fromUtf8("accessLabel"))
        self.layoutWidget_3 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_3.setGeometry(QtCore.QRect(673, 115, 351, 121))
        self.layoutWidget_3.setObjectName(_fromUtf8("layoutWidget_3"))
        self.horizontalLayout_12 = QtGui.QHBoxLayout(self.layoutWidget_3)
        self.horizontalLayout_12.setObjectName(_fromUtf8("horizontalLayout_12"))
        spacerItem = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_12.addItem(spacerItem)
        self.photoLabel_2 = QtGui.QLabel(self.layoutWidget_3)
        self.photoLabel_2.setFrameShape(QtGui.QFrame.NoFrame)
        self.photoLabel_2.setText(_fromUtf8(""))
        self.photoLabel_2.setPixmap(QtGui.QPixmap(_fromUtf8("homer.png")))
        self.photoLabel_2.setObjectName(_fromUtf8("photoLabel_2"))
        self.horizontalLayout_12.addWidget(self.photoLabel_2)
        spacerItem1 = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_12.addItem(spacerItem1)
        Access.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(Access)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        Access.setStatusBar(self.statusbar)

        self.retranslateUi(Access)
        QtCore.QMetaObject.connectSlotsByName(Access)

    def retranslateUi(self, Access):
        Access.setWindowTitle(_translate("Access", "Permission", None))
        self.video.setTitle(_translate("Access", "Video", None))

        self.deleteButton.setText(_translate("Access", "Delete", None))
        self.registerButton.setText(_translate("Access", "Register", None))
        self.accessLabel.setText(_translate("Access", "ACCESS:", None))
#-----------------------------------------------------------------------------------------------------------
def initalizeRegisterProfileWindow(mainWindow):
    reg = Ui_Register(None)
    captureT = Capture (0, q, 1920, 1080, 30)
    reg.setCaptureThread(captureT)
    reg.hide()
    mainWindow.setRegisterProfileWindow(reg)

def initalizedeleteMenuWindow(mainWindow):
    delete = Ui_deleteMenu(None)
    captureT = Capture (0, q, 1920, 1080, 30)
    delete.setCaptureThread(captureT)
    delete.hide()
    mainWindow.setDeleteMenuWindow(delete)

def initalizeAccessFreeWindow(mainWindow):
    access = MyWindowClass(None)
    captureT = Capture (0, q, 1920, 1080, 30)
    access.setCaptureThread(captureT)
    access.hide()
    mainWindow.setAccessFreeWindow(access)

def initalizeAccessFreeForeignWindow(mainWindow):
    access = MyWindowClass(None)
    captureT = Capture (0, q, 1920, 1080, 30)
    access.setCaptureThread(captureT)
    access.hide()
    mainWindow.setAccessFreeForeignWindow(access)

def initalizeAccessFreeWindow(mainWindow):
    access = MyWindowClass(None)
    captureT = Capture (0, q, 1920, 1080, 30)
    access.setCaptureThread(captureT)
    access.hide()
    mainWindow.setAccessFreeOtherWindow(access)

def recognition():
    global q 
    global dictionarySharedObject
    key = None
    #Camera objects
    collection = 'users'
    camera = classes.cam()
    pic = classes.picture()
    #database objects
    filedb = classes.FileDB()
    db = classes.Database()
    #User objects
    us = classes.user()

    #training a picture model to face recognition
    load = db.loadDB()
    if(not(len(load) == 0)):
        trainImgs = []
        trainLabels = []
        for u in load:
            for uImg in u['fotos']:
                trainImgs.append(uImg)
                trainLabels.append(u['label'])
        (Imgs, Labels) = [numpy.array(lis) for lis in [trainImgs, trainLabels]]
        model = cv2.face.createLBPHFaceRecognizer()
        model.train(Imgs, Labels)
    #not(key == "exit") and
    while(q.qsize() < 10):
        q.put(camera.getFrame())            
    
    while(not(key)==0):
        q.put(camera.getFrame())
        frame = q.get()
        if(not(len(frame) == 0)):
            #Image processing
            framegray = pic.imageProcessing(frame)
            img = camera.faceDetection(framegray)
            #q.put(img)
            roi = []
            for x1,y1,x2,y2 in img:
                roi.append(frame[y1*2:(y1+y2)*2,x1*2:(x1+x2)*2])
            #User Recognition
            if(not(len(load) == 0)):
                dictionary = pic.imageRecognition(framegray, img, model, load)
                pic.draw(frame, dictionary)
                dictionarySharedObject = dictionary
                #delete = thread()
                #Delete User
                if(not(profile)):
                    #colocar thread aqui q dá os dados para deletar o usuário.
                    filedb.delete(User['ID'])
                    db.deletephotoDB(collection, User['ID'])

            #register = thread()
            #Registering User
            if(profile):
                if(len(dictionarySharedObject) == 0):
                    User = us.Register()
                    filedb.insert(User)
                    while(q.qsize() < 10):
                        q.put(camera.getFrame())
                        frame = q.get()
                        framegray = pic.imageProcessing(frame)
                        img = camera.faceDetection(framegray)
                        roi = []
                        for x1,y1,x2,y2 in img:
                            roi.append(frame[y1*2:(y1+y2)*2,x1*2:(x1+x2)*2])
                        db.saveOnFotoDB(User['ID'], roi[0])
                    
                    #training a picture model to face recognition
                    if(len(load) == 0):                
                        trainImgs = []
                        trainLabels = []
                        for u in load:
                            for uImg in u['fotos']:
                                trainImgs.append(uImg)
                                trainLabels.append(u['label'])
                        (Imgs, Labels) = [numpy.array(lis) for lis in [trainImgs, trainLabels]]
                        model = cv2.face.createLBPHFaceRecognizer()
                        model.train(Imgs, Labels)

        #Showing
            #flip = cv2.flip(img,1,0)
            break
            #cv2.imshow('OpenCV - Press Esc to exit', flip)
        else:
            key = "exit"

def main():
    
    t = threading.Thread(target=recognition)
    t.start()

    app = QtGui.QApplication(sys.argv)
    accessWindow = MyWindowClass(None)
    captureThread = Capture(0, q, 1920, 1080, 30)
    accessWindow.setCaptureThread(captureThread)
    accessWindow.show()


    if(len(dictionarySharedObject)==0):
        accessWindow.getAccessDeniedWindow()
    elif(dictionarySharedObject['UserCategory'] == "Student" or dictionarySharedObject['UserCategory'] == 'Professor'):
        accessWindow.getAccessFree()
    elif(dictionarySharedObject['UserCategory'] == "Foreign"):
        accessWindow.getAccessFreeForeign()
    else:
        accessWindow.getAccessFreeOther()

    #objeto.start() para iniciar a thread do

    # registerMenuWindow = registerMenu.Ui_Register()
    # accessWindow.setRegisterMenuWindow(registerMenuWindow)
    deleteMenuWindow = deleteMenu.Ui_DeleteMenu()
    searchWindow = search.Ui_Search()
    accessWindow.setDeleteMenuWindow(deleteMenuWindow)
    accessWindow.getDeleteMenuWindow().setSearchWindow(searchWindow)
    app.exec_()


if __name__ == '__main__':
    main()
