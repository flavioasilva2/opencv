# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtCore, QtGui, uic
import os
import cv2
import numpy as np
import threading
import time
import Queue
import confirmDeleteForeign

q = Queue.Queue()

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Capture(threading.Thread):
    __deleteButton = None
    __deleteButtonClickable = False
    __cam = None
    __queue = None
    __width = None
    __height = None
    __fps = None

    def setDeleteButton(self, button):
        self.__deleteButton = button

    def __setDeleteButtonEnabled(self):
        if self.__deleteButton != None:
            self.__deleteButtonClickable = True
            self.__deleteButton.setEnabled(self.__deleteButtonClickable)

    def __setDeleteButtonDisabled(self):
        if self.__deleteButton != None:
            self.__deleteButtonClickable = False
            self.__deleteButton.setEnabled(self.__deleteButtonClickable)

    def __init__(self, cam, queue, width, height, fps):
        threading.Thread.__init__(self)
        self.__cam = cam
        self.__queue = queue
        self.__width = width
        self.__height = height
        self.__fps = fps

    def run(self):
        haarFacePath = "haarcascades/haarcascade_frontalface_alt.xml"
        haarFace = cv2.CascadeClassifier(haarFacePath)

        capture = cv2.VideoCapture(self.__cam)
        capture.set(cv2.CAP_PROP_FRAME_WIDTH, self.__width)
        capture.set(cv2.CAP_PROP_FRAME_HEIGHT, self.__height)
        capture.set(cv2.CAP_PROP_FPS, self.__fps)

        tools = DetectionTools()
        while(self.__queue.qsize() < 10):
            retval, img = capture.read()
            face = tools.detect(img, haarFace)
            tools.drawRects(img, face, 1, (0, 255, 0))
            if len(face) != 0:
                self.__setDeleteButtonEnabled()
            else:
                self.__setDeleteButtonDisabled()

            if(self.__queue.qsize() < 10):
                self.__queue.put(img)
            else:
                capture.release()
                break

class DetectionTools:
    def detect(self, img, cascade):
        out = cascade.detectMultiScale(img, scaleFactor=1.1, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
        if len(out) == 0:
            return []
        return out

    def drawRects(self, img, pos, scale, color):
        for x1, y1, x2, y2 in pos:
            cv2.rectangle(img, (x1*scale, y1*scale), ((x2+x1)*scale, (y2+y1)*scale), color, 2)

class OwnImageWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(OwnImageWidget, self).__init__(parent)
        self.image = None

    def setImage(self, image):
        self.image = image
        sz = image.size()
        self.setMinimumSize(sz)
        self.update()

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        if self.image:
            qp.drawImage(QtCore.QPoint(0, 0), self.image)
        qp.end()

class MyWindowClass(QtGui.QMainWindow):
    # __capture_thread = None
    __captureThread = None
    __captureThreadStarted = False
    __deleteProfileWindow = None

    def setCaptureThread(self, t):
        self.__captureThread = t

    def getDeleteProfileWindow(self):
        return self.__deleteProfileWindow

    def setDeleteProfileWindow(self, win):
        self.__deleteProfileWindow = win

    def deleteProfileWindowCallback(self):
        self.getDeleteProfileWindow().show()

    def __init__(self, parent):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.window_width = self.ImgWidget.frameSize().width()
        self.window_height = self.ImgWidget.frameSize().height()
        self.ImgWidget = OwnImageWidget(self.ImgWidget)

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(1)

    def __startCaptureThread(self):
        if (self.__captureThreadStarted == False) and (self.__captureThread != None):
            self.__captureThread.start()
            self.__captureThreadStarted = True

    def update_frame(self):
        self.__startCaptureThread()
        if not q.empty():
            img = q.get()

            img_height, img_width, img_colors = img.shape
            scale_w = float(self.window_width) / float(img_width)
            scale_h = float(self.window_height) / float(img_height)
            scale = min([scale_w, scale_h])

            if scale == 0:
                scale = 1

            img = cv2.resize(img, None, fx=scale, fy=scale, interpolation = cv2.INTER_CUBIC)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            height, width, bpc = img.shape
            bpl = bpc * width
            image = QtGui.QImage(img.data, width, height, bpl, QtGui.QImage.Format_RGB888)
            self.ImgWidget.setImage(image)
# ----------------------------------------------------------------------------------------------------

# ---------------- Methods that add Widgets to the window class --------------------------------------
    def setupUi(self, permOther):
        permOther.setObjectName(_fromUtf8("permOther"))
        permOther.resize(1288, 476)
        self.centralwidget = QtGui.QWidget(permOther)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 20, 641, 391))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.ImgWidget = QtGui.QWidget(self.groupBox)
        self.ImgWidget.setGeometry(QtCore.QRect(0, 30, 621, 361))
        self.ImgWidget.setObjectName(_fromUtf8("ImgWidget"))
        self.accessLabel = QtGui.QLabel(self.centralwidget)
        self.accessLabel.setGeometry(QtCore.QRect(669, 10, 341, 41))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.accessLabel.setPalette(palette)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(25)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.NoAntialias)
        self.accessLabel.setFont(font)
        self.accessLabel.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.accessLabel.setStyleSheet(_fromUtf8("permissionLabel->setStyleSheet(\"color: green\");"))
        self.accessLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.accessLabel.setObjectName(_fromUtf8("accessLabel"))
        self.line_2 = QtGui.QFrame(self.centralwidget)
        self.line_2.setGeometry(QtCore.QRect(1010, 0, 16, 451))
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.line_3 = QtGui.QFrame(self.centralwidget)
        self.line_3.setGeometry(QtCore.QRect(650, 0, 16, 451))
        self.line_3.setFrameShape(QtGui.QFrame.VLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(659, 229, 351, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.layoutWidget = QtGui.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(660, 110, 351, 121))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.horizontalLayout_6 = QtGui.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        spacerItem = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem)
        self.photoLabel = QtGui.QLabel(self.layoutWidget)
        self.photoLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.photoLabel.setText(_fromUtf8(""))
        self.photoLabel.setPixmap(QtGui.QPixmap(_fromUtf8("homer.png")))
        self.photoLabel.setObjectName(_fromUtf8("photoLabel"))
        self.horizontalLayout_6.addWidget(self.photoLabel)
        spacerItem1 = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem1)
        self.grantedLabel = QtGui.QLabel(self.centralwidget)
        self.grantedLabel.setGeometry(QtCore.QRect(669, 50, 341, 41))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.grantedLabel.setPalette(palette)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial Black"))
        font.setPointSize(20)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.NoAntialias)
        self.grantedLabel.setFont(font)
        self.grantedLabel.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.grantedLabel.setStyleSheet(_fromUtf8("permissionLabel->setStyleSheet(\"color: green\");"))
        self.grantedLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.grantedLabel.setObjectName(_fromUtf8("grantedLabel"))
        self.layoutWidget1 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget1.setGeometry(QtCore.QRect(662, 242, 351, 201))
        self.layoutWidget1.setObjectName(_fromUtf8("layoutWidget1"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.layoutWidget1)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.courseLabel_2 = QtGui.QLabel(self.layoutWidget1)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.courseLabel_2.setFont(font)
        self.courseLabel_2.setObjectName(_fromUtf8("courseLabel_2"))
        self.horizontalLayout.addWidget(self.courseLabel_2)
        self.varGroup = QtGui.QLabel(self.layoutWidget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varGroup.setFont(font)
        self.varGroup.setObjectName(_fromUtf8("varGroup"))
        self.horizontalLayout.addWidget(self.varGroup)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.nameLabel = QtGui.QLabel(self.layoutWidget1)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.nameLabel.setFont(font)
        self.nameLabel.setObjectName(_fromUtf8("nameLabel"))
        self.horizontalLayout_2.addWidget(self.nameLabel)
        self.varName = QtGui.QLabel(self.layoutWidget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varName.setFont(font)
        self.varName.setObjectName(_fromUtf8("varName"))
        self.horizontalLayout_2.addWidget(self.varName)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.IDLabel = QtGui.QLabel(self.layoutWidget1)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.IDLabel.setFont(font)
        self.IDLabel.setObjectName(_fromUtf8("IDLabel"))
        self.horizontalLayout_3.addWidget(self.IDLabel)
        self.varID = QtGui.QLabel(self.layoutWidget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varID.setFont(font)
        self.varID.setObjectName(_fromUtf8("varID"))
        self.horizontalLayout_3.addWidget(self.varID)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        self.layoutWidget_2 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_2.setGeometry(QtCore.QRect(1020, 80, 261, 81))
        self.layoutWidget_2.setObjectName(_fromUtf8("layoutWidget_2"))
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.layoutWidget_2)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        self.monLabel_5 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.monLabel_5.setFont(font)
        self.monLabel_5.setObjectName(_fromUtf8("monLabel_5"))
        self.horizontalLayout_7.addWidget(self.monLabel_5)
        self.varMonHours_3 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_3.setFont(font)
        self.varMonHours_3.setObjectName(_fromUtf8("varMonHours_3"))
        self.horizontalLayout_7.addWidget(self.varMonHours_3)
        self.verticalLayout_4.addLayout(self.horizontalLayout_7)
        self.monLabel_6 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        self.monLabel_6.setFont(font)
        self.monLabel_6.setObjectName(_fromUtf8("monLabel_6"))
        self.verticalLayout_4.addWidget(self.monLabel_6)
        self.layoutWidget_3 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_3.setGeometry(QtCore.QRect(1020, 260, 261, 81))
        self.layoutWidget_3.setObjectName(_fromUtf8("layoutWidget_3"))
        self.verticalLayout_5 = QtGui.QVBoxLayout(self.layoutWidget_3)
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.horizontalLayout_8 = QtGui.QHBoxLayout()
        self.horizontalLayout_8.setObjectName(_fromUtf8("horizontalLayout_8"))
        self.monLabel_7 = QtGui.QLabel(self.layoutWidget_3)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.monLabel_7.setFont(font)
        self.monLabel_7.setObjectName(_fromUtf8("monLabel_7"))
        self.horizontalLayout_8.addWidget(self.monLabel_7)
        self.varMonHours_4 = QtGui.QLabel(self.layoutWidget_3)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_4.setFont(font)
        self.varMonHours_4.setObjectName(_fromUtf8("varMonHours_4"))
        self.horizontalLayout_8.addWidget(self.varMonHours_4)
        self.verticalLayout_5.addLayout(self.horizontalLayout_8)
        self.monLabel_8 = QtGui.QLabel(self.layoutWidget_3)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        self.monLabel_8.setFont(font)
        self.monLabel_8.setObjectName(_fromUtf8("monLabel_8"))
        self.verticalLayout_5.addWidget(self.monLabel_8)
        self.schedualLabel = QtGui.QLabel(self.centralwidget)
        self.schedualLabel.setGeometry(QtCore.QRect(1019, 40, 79, 17))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.schedualLabel.setFont(font)
        self.schedualLabel.setObjectName(_fromUtf8("schedualLabel"))
        self.layoutWidget_4 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_4.setGeometry(QtCore.QRect(1020, 170, 261, 81))
        self.layoutWidget_4.setObjectName(_fromUtf8("layoutWidget_4"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.layoutWidget_4)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.monLabel_3 = QtGui.QLabel(self.layoutWidget_4)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.monLabel_3.setFont(font)
        self.monLabel_3.setObjectName(_fromUtf8("monLabel_3"))
        self.horizontalLayout_5.addWidget(self.monLabel_3)
        self.varMonHours_2 = QtGui.QLabel(self.layoutWidget_4)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_2.setFont(font)
        self.varMonHours_2.setObjectName(_fromUtf8("varMonHours_2"))
        self.horizontalLayout_5.addWidget(self.varMonHours_2)
        self.verticalLayout_2.addLayout(self.horizontalLayout_5)
        self.monLabel_4 = QtGui.QLabel(self.layoutWidget_4)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        self.monLabel_4.setFont(font)
        self.monLabel_4.setObjectName(_fromUtf8("monLabel_4"))
        self.verticalLayout_2.addWidget(self.monLabel_4)
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(1058, 410, 178, 41))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout(self.widget)
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.registerButton = QtGui.QPushButton(self.widget)
        self.registerButton.setEnabled(False)
        self.registerButton.setCheckable(False)
        self.registerButton.setChecked(False)
        self.registerButton.setObjectName(_fromUtf8("registerButton"))
        self.horizontalLayout_4.addWidget(self.registerButton)

        self.deleteButton = QtGui.QPushButton(self.widget)
        # self.deleteButton.setEnabled(False)
        self.deleteButton.setToolTip(_fromUtf8(""))
        self.deleteButton.setStatusTip(_fromUtf8(""))
        self.deleteButton.setAutoDefault(False)
        self.deleteButton.setDefault(False)
        self.deleteButton.setFlat(False)
        self.deleteButton.setObjectName(_fromUtf8("deleteButton"))
        self.deleteButton.clicked.connect(self.deleteProfileWindowCallback)
        self.horizontalLayout_4.addWidget(self.deleteButton)
        permOther.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(permOther)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        permOther.setStatusBar(self.statusbar)

        self.retranslateUi(permOther)
        QtCore.QMetaObject.connectSlotsByName(permOther)

    def retranslateUi(self, permOther):
        permOther.setWindowTitle(_translate("permOther", "Permission", None))
        self.groupBox.setTitle(_translate("permOther", "Video", None))
        self.accessLabel.setText(_translate("permOther", "ACCESS:", None))
        self.grantedLabel.setText(_translate("permOther", "GRANTED", None))
        self.courseLabel_2.setText(_translate("permOther", "Group:", None))
        self.varGroup.setText(_translate("permOther", "CLEANER", None))
        self.nameLabel.setText(_translate("permOther", "Name:", None))
        self.varName.setText(_translate("permOther", "HOMER SIMPSON", None))
        self.IDLabel.setText(_translate("permOther", "ID:", None))
        self.varID.setText(_translate("permOther", "17/0160000", None))
        self.monLabel_5.setText(_translate("permOther", "01/02/2017", None))
        self.varMonHours_3.setText(_translate("permOther", "-- : -- | -- : --", None))
        self.monLabel_6.setText(_translate("permOther", "<html><head/><body><p>Installing softwares Lab 1.</p></body></html>", None))
        self.monLabel_7.setText(_translate("permOther", "03/02/2017", None))
        self.varMonHours_4.setText(_translate("permOther", "-- : -- | -- : --", None))
        self.monLabel_8.setText(_translate("permOther", "<html><head/><body><p>Fixing wires in Lab 3.</p></body></html>", None))
        self.schedualLabel.setText(_translate("permOther", "Scheduale:", None))
        self.monLabel_3.setText(_translate("permOther", "02/02/2017", None))
        self.varMonHours_2.setText(_translate("permOther", "-- : -- | -- : --", None))
        self.monLabel_4.setText(_translate("permOther", "<html><head/><body><p>Installing antivirus in Lab 2.</p></body></html>", None))
        self.registerButton.setText(_translate("permOther", "Register", None))
        self.deleteButton.setText(_translate("permOther", "Delete", None))
#-----------------------------------------------------------------------------------------------------------
def initalizeDeleteProfileWindow(mainWindow):
    delete = Ui_DeleteProfile(None)
    captureT = Capture (0, q, 1920, 1080, 30)
    delete.setCaptureThread(captureT)
    delete.hide()
    mainWindow.setdeleteProfileWindow(delete)

def main():
    app = QtGui.QApplication(sys.argv)
    accessWindow = MyWindowClass(None)
    captureT = Capture(0, q, 1920, 1080, 30)
    accessWindow.setCaptureThread(captureT)
    accessWindow.show()
    deleteWindow = confirmDeleteForeign.Ui_DeleteProfile()
    accessWindow.setDeleteProfileWindow(deleteWindow)
    app.exec_()

if __name__ == '__main__':
    main()
