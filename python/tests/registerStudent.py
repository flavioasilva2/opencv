# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
import sys
import os
import pickle

import threading
from time import sleep

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_RegisterStud(QtGui.QMainWindow):

    # __banco = FileDB('registerstud.json')

    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

    def __closeWindow(self):
        self.close()

    __nameText = None
    __idText = None
    __courseText = None
    __d1Text = None
    __d2Text = None
    __d3Text = None
    __d4Text = None
    __d5Text = None
    __d6Text = None
    __d7Text = None
    __d8Text = None
    __c1Text = None
    __c2Text = None
    __c3Text = None
    __c4Text = None
    __c5Text = None
    __c6Text = None
    __c7Text = None
    __c8Text = None
# ---------------- Method that adds Widgets to the window class --------------------------------------
    def setupUi(self, RegisterStud):
        RegisterStud.setObjectName(_fromUtf8("RegisterStud"))
        RegisterStud.resize(735, 600)
        self.centralwidget = QtGui.QWidget(RegisterStud)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayoutWidget = QtGui.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(40, 70, 94, 114))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))

# ------------------------------------- PHOTO ----------------------------------------------
        self.photoLabel = QtGui.QLabel(self.verticalLayoutWidget)
        self.photoLabel.setText(_fromUtf8(""))
        self.photoLabel.setPixmap(QtGui.QPixmap(_fromUtf8("homer.png")))
        self.photoLabel.setScaledContents(True)
        self.photoLabel.setObjectName(_fromUtf8("photoLabel"))
        self.verticalLayout_3.addWidget(self.photoLabel)

# ------------------------------------- TITLE ----------------------------------------------
        self.titleLabel = QtGui.QLabel(self.centralwidget)
        self.titleLabel.setGeometry(QtCore.QRect(260, 10, 311, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("mry_KacstQurn"))
        font.setPointSize(20)
        self.titleLabel.setFont(font)

        self.titleLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.titleLabel.setFrameShape(QtGui.QFrame.Panel)
        self.titleLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.titleLabel.setLineWidth(3)
        self.titleLabel.setTextFormat(QtCore.Qt.AutoText)
        self.titleLabel.setScaledContents(False)
        self.titleLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.titleLabel.setObjectName(_fromUtf8("titleLabel"))

# ------------------------------------- GROUP ----------------------------------------------
        self.groupLabel = QtGui.QLabel(self.centralwidget)
        self.groupLabel.setGeometry(QtCore.QRect(10, 10, 161, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("mry_KacstQurn"))
        font.setPointSize(17)
        self.groupLabel.setFont(font)

        self.groupLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.groupLabel.setFrameShape(QtGui.QFrame.Box)
        self.groupLabel.setFrameShadow(QtGui.QFrame.Sunken)
        self.groupLabel.setLineWidth(2)
        self.groupLabel.setTextFormat(QtCore.Qt.AutoText)
        self.groupLabel.setScaledContents(False)
        self.groupLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.groupLabel.setObjectName(_fromUtf8("groupLabel"))

        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(180, 70, 521, 164))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.verticalLayout_5 = QtGui.QVBoxLayout(self.widget)
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))

# ------------------------------------- NAME ----------------------------------------------
        self.nameLabel = QtGui.QLabel(self.widget)
        self.nameLabel.setObjectName(_fromUtf8("nameLabel"))
        self.verticalLayout_5.addWidget(self.nameLabel)

        self.inputName = QtGui.QTextEdit(self.widget)
        self.inputName.setObjectName(_fromUtf8("inputName"))
        # self.inputName.setPlainText(self.__nameText)
        self.verticalLayout_5.addWidget(self.inputName)

# ------------------------------------- ID ----------------------------------------------
        self.IDLabel = QtGui.QLabel(self.widget)
        self.IDLabel.setObjectName(_fromUtf8("IDLabel"))
        self.verticalLayout_5.addWidget(self.IDLabel)

        self.inputID = QtGui.QTextEdit(self.widget)
        self.inputID.setObjectName(_fromUtf8("inputID"))
        # self.inputID.setPlainText(self.__idText)
        self.verticalLayout_5.addWidget(self.inputID)

# ------------------------------------- COURSE ----------------------------------------------
        self.courseLabel = QtGui.QLabel(self.widget)
        self.courseLabel.setObjectName(_fromUtf8("courseLabel"))
        self.verticalLayout_5.addWidget(self.courseLabel)

        self.inputCourse = QtGui.QTextEdit(self.widget)
        self.inputCourse.setObjectName(_fromUtf8("inputCourse"))
        # self.inputCourse.setPlainText(self.__nameCourse)
        self.verticalLayout_5.addWidget(self.inputCourse)

# ------------------------------------- BUTTONS ----------------------------------------------
        self.widget1 = QtGui.QWidget(self.centralwidget)
        self.widget1.setGeometry(QtCore.QRect(30, 440, 111, 101))
        self.widget1.setObjectName(_fromUtf8("widget1"))
        self.verticalLayout_6 = QtGui.QVBoxLayout(self.widget1)
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))

        # SAVE BUTTON
        self.saveButton = QtGui.QPushButton(self.widget1)
        self.saveButton.setAcceptDrops(False)
        self.saveButton.setToolTip(_fromUtf8(""))
        self.saveButton.setStatusTip(_fromUtf8(""))
        self.saveButton.setAutoDefault(False)
        self.saveButton.setDefault(True)
        self.saveButton.setObjectName(_fromUtf8("saveButton"))
        self.saveButton.clicked.connect(self.gettext)
        self.verticalLayout_6.addWidget(self.saveButton)

        # CANCEL BUTTON
        self.cancelButton = QtGui.QPushButton(self.widget1)
        self.cancelButton.setAcceptDrops(False)
        self.cancelButton.setToolTip(_fromUtf8(""))
        self.cancelButton.setStatusTip(_fromUtf8(""))
        self.cancelButton.setObjectName(_fromUtf8("cancelButton"))
        self.cancelButton.clicked.connect(self.__closeWindow)
        self.verticalLayout_6.addWidget(self.cancelButton)

        self.splitter = QtGui.QSplitter(self.centralwidget)
        self.splitter.setGeometry(QtCore.QRect(180, 250, 521, 283))
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.layoutWidget_4 = QtGui.QWidget(self.splitter)
        self.layoutWidget_4.setObjectName(_fromUtf8("layoutWidget_4"))
        self.verticalLayout_10 = QtGui.QVBoxLayout(self.layoutWidget_4)
        self.verticalLayout_10.setObjectName(_fromUtf8("verticalLayout_10"))

# ---------------------------------- COURSES ----------------------------------------------
        # DISCIPLINE LABEL
        self.disciplineLabel = QtGui.QLabel(self.layoutWidget_4)
        self.disciplineLabel.setObjectName(_fromUtf8("disciplineLabel"))
        self.verticalLayout_10.addWidget(self.disciplineLabel)
        # DISCIPLINE 1
        self.inputDiscipline1 = QtGui.QTextEdit(self.layoutWidget_4)
        self.inputDiscipline1.setObjectName(_fromUtf8("inputDiscipline1"))
        self.verticalLayout_10.addWidget(self.inputDiscipline1)
        # DISCIPLINE 2
        self.inputDiscipline2 = QtGui.QTextEdit(self.layoutWidget_4)
        self.inputDiscipline2.setObjectName(_fromUtf8("inputDiscipline2"))
        self.verticalLayout_10.addWidget(self.inputDiscipline2)
        # DISCIPLINE 3
        self.inputDiscipline3 = QtGui.QTextEdit(self.layoutWidget_4)
        self.inputDiscipline3.setObjectName(_fromUtf8("inputDiscipline3"))
        self.verticalLayout_10.addWidget(self.inputDiscipline3)
        # DISCIPLINE 4
        self.inputDiscipline4 = QtGui.QTextEdit(self.layoutWidget_4)
        self.inputDiscipline4.setObjectName(_fromUtf8("inputDiscipline4"))
        self.verticalLayout_10.addWidget(self.inputDiscipline4)
        # DISCIPLINE 5
        self.inputDiscipline5 = QtGui.QTextEdit(self.layoutWidget_4)
        self.inputDiscipline5.setObjectName(_fromUtf8("inputDiscipline5"))
        self.verticalLayout_10.addWidget(self.inputDiscipline5)
        # DISCIPLINE 6
        self.inputDiscipline6 = QtGui.QTextEdit(self.layoutWidget_4)
        self.inputDiscipline6.setObjectName(_fromUtf8("inputDiscipline6"))
        self.verticalLayout_10.addWidget(self.inputDiscipline6)
        # DISCIPLINE 7
        self.inputDiscipline7 = QtGui.QTextEdit(self.layoutWidget_4)
        self.inputDiscipline7.setObjectName(_fromUtf8("inputDiscipline7"))
        self.verticalLayout_10.addWidget(self.inputDiscipline7)
        # DISCIPLINE 8
        self.inputDiscipline8 = QtGui.QTextEdit(self.layoutWidget_4)
        self.inputDiscipline8.setObjectName(_fromUtf8("inputDiscipline8"))
        self.verticalLayout_10.addWidget(self.inputDiscipline8)

        self.layoutWidget_2 = QtGui.QWidget(self.splitter)
        self.layoutWidget_2.setObjectName(_fromUtf8("layoutWidget_2"))
        self.verticalLayout_8 = QtGui.QVBoxLayout(self.layoutWidget_2)
        self.verticalLayout_8.setObjectName(_fromUtf8("verticalLayout_8"))

        # DISCIPLINE CODE LABEL
        self.codeLabel = QtGui.QLabel(self.layoutWidget_2)
        self.codeLabel.setObjectName(_fromUtf8("codeLabel"))
        self.verticalLayout_8.addWidget(self.codeLabel)
        # DISCIPLINE CODE 1
        self.inputCode1 = QtGui.QTextEdit(self.layoutWidget_2)
        self.inputCode1.setObjectName(_fromUtf8("inputCode1"))
        self.verticalLayout_8.addWidget(self.inputCode1)
        # DISCIPLINE CODE 2
        self.inputCode2 = QtGui.QTextEdit(self.layoutWidget_2)
        self.inputCode2.setObjectName(_fromUtf8("inputCode2"))
        self.verticalLayout_8.addWidget(self.inputCode2)
        # DISCIPLINE CODE 3
        self.inputCode3 = QtGui.QTextEdit(self.layoutWidget_2)
        self.inputCode3.setObjectName(_fromUtf8("inputCode3"))
        self.verticalLayout_8.addWidget(self.inputCode3)
        # DISCIPLINE CODE 4
        self.inputCode4 = QtGui.QTextEdit(self.layoutWidget_2)
        self.inputCode4.setObjectName(_fromUtf8("inputCode4"))
        self.verticalLayout_8.addWidget(self.inputCode4)
        # DISCIPLINE CODE 5
        self.inputCode5 = QtGui.QTextEdit(self.layoutWidget_2)
        self.inputCode5.setObjectName(_fromUtf8("inputCode5"))
        self.verticalLayout_8.addWidget(self.inputCode5)
        # DISCIPLINE CODE 6
        self.inputCode6 = QtGui.QTextEdit(self.layoutWidget_2)
        self.inputCode6.setObjectName(_fromUtf8("inputCode6"))
        self.verticalLayout_8.addWidget(self.inputCode6)
        # DISCIPLINE CODE 7
        self.inputCode7 = QtGui.QTextEdit(self.layoutWidget_2)
        self.inputCode7.setObjectName(_fromUtf8("inputCode7"))
        self.verticalLayout_8.addWidget(self.inputCode7)
        # DISCIPLINE CODE 8
        self.inputCode8 = QtGui.QTextEdit(self.layoutWidget_2)
        self.inputCode8.setObjectName(_fromUtf8("inputCode8"))
        self.verticalLayout_8.addWidget(self.inputCode8)

        self.inputDiscipline1.raise_()
        self.verticalLayoutWidget.raise_()
        self.photoLabel.raise_()
        self.titleLabel.raise_()
        self.groupLabel.raise_()
        self.inputCourse.raise_()
        self.inputID.raise_()
        self.inputName.raise_()
        self.codeLabel.raise_()
        self.splitter.raise_()
        self.saveButton.raise_()
        self.cancelButton.raise_()
        self.layoutWidget_2.raise_()
        self.layoutWidget_4.raise_()
        RegisterStud.setCentralWidget(self.centralwidget)

# ---------------------------------- STATUS BAR ----------------------------------------------
        self.statusbar = QtGui.QStatusBar(RegisterStud)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        RegisterStud.setStatusBar(self.statusbar)
# ---------------------------------------------------------------------------------------------------
        self.retranslateUi(RegisterStud)     ## Method that atribuites text to each data
        QtCore.QMetaObject.connectSlotsByName(RegisterStud)

    # def save(self):
    #     # self.obj = self.inputName
    #     self.obj = 'marcella'
    #     self.output = open('myfile.pkl', 'wb')
    #     pickle.dump(self.obj, self.output)
    #     self.output.close()
    #     print self.obj
    #
    # def load(self):
    #     output = open('myfile.pkl', 'rb')
    #     obj = pickle.load(output)
    #     obj.close()
    #     print obj

    __name = {}
    __id = {}
    __course = {}
    __d1 = {}
    __d2 = {}
    __d3 = {}
    __d4 = {}
    __d5 = {}
    __d6 = {}
    __d7 = {}
    __d8 = {}
    __c1 = {}
    __c2 = {}
    __c3 = {}
    __c4 = {}
    __c5 = {}
    __c6 = {}
    __c7 = {}
    __c8 = {}

    # def getName(self):
    #     return self.__name
    # def getID(self):
    #     return self.__id
    # def getCourse(self):
    #     return self.__course
    # def getCourse(self):
    #     return self.__course

    def gettext(self):
        self.__nameText = self.inputName.toPlainText()
        self.inputName.setPlainText(self.__nameText)
        self.__name['name'] = self.__nameText
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__nameText)
        # self.__banco.isert({})
        print(self.__nameText)

        self.__idText = self.inputName.toPlainText()
        self.inputID.setPlainText(self.__idText)
        self.__id['id'] = self.__idText
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__idText)
        # self.__banco.isert({})
        print(self.__idText)

        self.__courseText = self.inputName.toPlainText()
        self.inputCourse.setPlainText(self.__courseText)
        self.__course['course'] = self.__courseText
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__courseText)
        # self.__banco.isert({})
        print(self.__courseText)

        self.__d1Text = self.inputName.toPlainText()
        self.inputDiscipline1.setPlainText(self.__d1Text)
        self.__d1['discipline1'] = self.__d1Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__d1Text)
        # self.__banco.isert({})
        print(self.__d1Text)

        self.__c1Text = self.inputName.toPlainText()
        self.inputCode1.setPlainText(self.__c1Text)
        self.__c1['code1'] = self.__c1Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__c1Text)
        # self.__banco.isert({})
        print(self.__c1Text)

        self.__d2Text = self.inputName.toPlainText()
        self.inputDiscipline2.setPlainText(self.__d2Text)
        self.__d2['discipline2'] = self.__d1Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__d2Text)
        # self.__banco.isert({})
        print(self.__d2Text)

        self.__c2Text = self.inputName.toPlainText()
        self.inputCode2.setPlainText(self.__c2Text)
        self.__c2['code2'] = self.__c2Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__c2Text)
        # self.__banco.isert({})
        print(self.__c2Text)

        self.__d3Text = self.inputName.toPlainText()
        self.inputDiscipline3.setPlainText(self.__d3Text)
        self.__d3['discipline3'] = self.__d3Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__d3Text)
        # self.__banco.isert({})
        print(self.__d3Text)


        self.__c3Text = self.inputName.toPlainText()
        self.inputCode3.setPlainText(self.__c3Text)
        self.__c3['code3'] = self.__c3Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__c3Text)
        # self.__banco.isert({})
        print(self.__c3Text)

        self.__d4Text = self.inputName.toPlainText()
        self.inputDiscipline4.setPlainText(self.__d4Text)
        self.__d4['discipline4'] = self.__d4Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__d4Text)
        # self.__banco.isert({})
        print(self.__d4Text)


        self.__c4Text = self.inputName.toPlainText()
        self.inputCode4.setPlainText(self.__c4Text)
        self.__c4['code4'] = self.__c4Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__c4Text)
        # self.__banco.isert({})
        print(self.__c4Text)

        self.__d5Text = self.inputName.toPlainText()
        self.inputDiscipline5.setPlainText(self.__d5Text)
        self.__d5['discipline5'] = self.__d5Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__d5Text)
        # self.__banco.isert({})
        print(self.__d5Text)

        self.__c5Text = self.inputName.toPlainText()
        self.inputCode1.setPlainText(self.__c5Text)
        self.__c5['code5'] = self.__c5Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__c5Text)
        # self.__banco.isert({})
        print(self.__c5Text)

        self.__d6Text = self.inputName.toPlainText()
        self.inputDiscipline6.setPlainText(self.__d6Text)
        self.__d6['discipline6'] = self.__d6Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__d6Text)
        # self.__banco.isert({})
        print(self.__d6Text)

        self.__c6Text = self.inputName.toPlainText()
        self.inputCode6.setPlainText(self.__c6Text)
        self.__c6['code6'] = self.__c6Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__c6Text)
        # self.__banco.isert({})
        print(self.__c6Text)

        self.__d7Text = self.inputName.toPlainText()
        self.inputDiscipline7.setPlainText(self.__d7Text)
        self.__d7['discipline7'] = self.__d7Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__d7Text)
        # self.__banco.isert({})
        print(self.__d7Text)

        self.__c7Text = self.inputName.toPlainText()
        self.inputCode7.setPlainText(self.__c7Text)
        self.__c7['code7'] = self.__c7Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__c7Text)
        # self.__banco.isert({})
        print(self.__c7Text)

        self.__d8Text = self.inputName.toPlainText()
        self.inputDiscipline3.setPlainText(self.__d8Text)
        self.__d8['discipline8'] = self.__d8Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__d8Text)
        # self.__banco.isert({})
        print(self.__d8Text)

        self.__c8Text = self.inputName.toPlainText()
        self.inputCode8.setPlainText(self.__c8Text)
        self.__c8['code8'] = self.__c8Text
        f = open('inputs.txt', 'a')
        f.write("\n" + self.__c8Text)
        # self.__banco.isert({})
        print(self.__c8Text)

        f.close()

# ---------------------------------- METHOD WITH TEXT ----------------------------------------------
    def retranslateUi(self, RegisterStud):
        RegisterStud.setWindowTitle(_translate("RegisterStud", "Register", None))
        self.titleLabel.setText(_translate("RegisterStud", "NEW REGISTRATION", None))
        self.groupLabel.setText(_translate("RegisterStud", "STUDENT", None))
        self.nameLabel.setText(_translate("RegisterStud", "Name", None))
        self.IDLabel.setText(_translate("RegisterStud", "ID", None))
        self.courseLabel.setText(_translate("RegisterStud", "Course", None))

        self.saveButton.setText(_translate("RegisterStud", "Save", None))
        self.cancelButton.setText(_translate("RegisterStud", "Cancel", None))

        self.disciplineLabel.setText(_translate("RegisterStud", "DISCIPLINES", None))
        self.codeLabel.setText(_translate("RegisterStud", "CODE", None))
#-----------------------------------------------------------------------------------------------------------
def main():
    app = QtGui.QApplication(sys.argv)
    w = Ui_RegisterStud(None)
    w.show()
    app.exec_()

if __name__ == '__main__':
    main()
