#!/usr/bin/env python
#-*-coding: utf-8-*-

#University of Brasilia - CIC
#Abstract: This program is suposed to recognize and track people on you database system. 
#It is used Mongodb as database system in this program.

#Flavio Amaral e Silva
#Lukas Lorenz de Andrade
#Marcella Pantarotto
import cv2
import classes
import sys
import numpy
import getopt
import os

#import threading

def main():
    reg = False
    key = None
    i = 0
    #Camera objects
    collection = 'users'
    camera = classes.cam()
    pic = classes.picture()
    #database objects
    filedb = classes.FileDB()
    db = classes.Database()
    #User objects
    us = classes.user()
    
    #training a picture model to face recognition
    load = db.loadDB()
    if(not(len(load) == 0)):
        trainImgs = []
        trainLabels = []
        for u in load:
            for uImg in u['fotos']:
                trainImgs.append(uImg)
                trainLabels.append(u['label'])
        (Imgs, Labels) = [numpy.array(lis) for lis in [trainImgs, trainLabels]]
        model = cv2.face.createLBPHFaceRecognizer()
        model.train(Imgs, Labels)
    
    while(not(key == 27)):
        frame = camera.getFrame()
        img = frame.copy()
        if(not(len(frame) == 0)):
            #Image processing 
            framegray = pic.imageProcessing(img)
            img = camera.faceDetection(framegray)
            roi = []
            for x1,y1,x2,y2 in img:
                roi.append(frame[y1*2:(y1+y2)*2,x1*2:(x1+x2)*2])
            
            #User Recognition 
            if(not(len(load) == 0) and (not(reg))):
                picture = pic.imageRecognition(framegray, img, model, load)
                pic.draw(frame, picture)
                
                #delete = thread()
                #Delete User
                if(delete):
                    #colocar thread aqui q dá os dados para deletar o usuário.
                    filedb.delete(User['ID'])
                    db.deletephotoDB(collection, User['ID'])
            
            #register = thread()
            #Registering User
            if(register):
                reg = True
                if(i == 0):
                    User = us.Register()
                    filedb.insert(collection, User) 
                db.saveOnFotoDB(User['ID'], roi[0])
                i = 1+i
                if(i == 10):
                    i = 0
                    reg = False
            #Showing
            flip = cv2.flip(img,1,0)
            cv2.imshow('OpenCV - Press Esc to exit', flip)
            key = cv2.waitKey(1)
        else:
            key = 27

    cv2.destroyAllWindows()
main()