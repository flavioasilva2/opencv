# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
import sys
import os

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_RegisterProf(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

    def __closeWindow(self):
        self.close()

# ---------------- Method that adds Widgets to the window class --------------------------------------
    def setupUi(self, RegisterProf):
        RegisterProf.setObjectName(_fromUtf8("RegisterProf"))
        RegisterProf.resize(736, 600)
        self.centralwidget = QtGui.QWidget(RegisterProf)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))

# ---------------------- TITLE --------------------------------------
        self.titleLabel = QtGui.QLabel(self.centralwidget)
        self.titleLabel.setGeometry(QtCore.QRect(260, 10, 311, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("mry_KacstQurn"))
        font.setPointSize(20)
        self.titleLabel.setFont(font)

        self.titleLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.titleLabel.setFrameShape(QtGui.QFrame.Panel)
        self.titleLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.titleLabel.setLineWidth(3)
        self.titleLabel.setTextFormat(QtCore.Qt.AutoText)
        self.titleLabel.setScaledContents(False)
        self.titleLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.titleLabel.setObjectName(_fromUtf8("titleLabel"))

# --------------------- GROUP --------------------------------------
        self.groupLabel = QtGui.QLabel(self.centralwidget)
        self.groupLabel.setGeometry(QtCore.QRect(10, 10, 161, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("mry_KacstQurn"))
        font.setPointSize(17)
        self.groupLabel.setFont(font)

        self.groupLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.groupLabel.setFrameShape(QtGui.QFrame.Box)
        self.groupLabel.setFrameShadow(QtGui.QFrame.Sunken)
        self.groupLabel.setLineWidth(2)
        self.groupLabel.setTextFormat(QtCore.Qt.AutoText)
        self.groupLabel.setScaledContents(False)
        self.groupLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.groupLabel.setObjectName(_fromUtf8("groupLabel"))

        self.layoutWidget_3 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_3.setGeometry(QtCore.QRect(30, 440, 111, 101))
        self.layoutWidget_3.setObjectName(_fromUtf8("layoutWidget_3"))
        self.verticalLayout_9 = QtGui.QVBoxLayout(self.layoutWidget_3)
        self.verticalLayout_9.setObjectName(_fromUtf8("verticalLayout_9"))

# ----------------------- BUTTONS --------------------------------------
        # SAVE BUTTON
        self.saveButton = QtGui.QPushButton(self.layoutWidget_3)
        self.saveButton.setAcceptDrops(False)
        self.saveButton.setToolTip(_fromUtf8(""))
        self.saveButton.setStatusTip(_fromUtf8(""))
        self.saveButton.setAutoDefault(False)
        self.saveButton.setDefault(True)
        self.saveButton.setObjectName(_fromUtf8("saveButton"))
        self.verticalLayout_9.addWidget(self.saveButton)
        # CANCEL BUTTON
        self.cancelButton = QtGui.QPushButton(self.layoutWidget_3)
        self.cancelButton.setAcceptDrops(False)
        self.cancelButton.setToolTip(_fromUtf8(""))
        self.cancelButton.setStatusTip(_fromUtf8(""))
        self.cancelButton.setObjectName(_fromUtf8("cancelButton"))
        self.cancelButton.clicked.connect(self.__closeWindow)
        self.verticalLayout_9.addWidget(self.cancelButton)

        self.layoutWidget = QtGui.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(180, 70, 521, 164))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.verticalLayout_7 = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout_7.setObjectName(_fromUtf8("verticalLayout_7"))

# ------------------------ NAME --------------------------------------
        self.nameLabel = QtGui.QLabel(self.layoutWidget)
        self.nameLabel.setObjectName(_fromUtf8("nameLabel"))
        self.verticalLayout_7.addWidget(self.nameLabel)

        self.inputName = QtGui.QLineEdit(self.layoutWidget)
        self.inputName.setObjectName(_fromUtf8("inputName"))
        self.verticalLayout_7.addWidget(self.inputName)

# -------------------------- ID ----------------------------------------------
        self.IDLabel = QtGui.QLabel(self.layoutWidget)
        self.IDLabel.setObjectName(_fromUtf8("IDLabel"))
        self.verticalLayout_7.addWidget(self.IDLabel)

        self.inputID = QtGui.QLineEdit(self.layoutWidget)
        self.inputID.setObjectName(_fromUtf8("inputID"))
        self.verticalLayout_7.addWidget(self.inputID)

# -------------------------- DEPARTMENT ----------------------------------------------
        self.depLabel = QtGui.QLabel(self.layoutWidget)
        self.depLabel.setObjectName(_fromUtf8("depLabel"))
        self.verticalLayout_7.addWidget(self.depLabel)

        self.inputDep = QtGui.QLineEdit(self.layoutWidget)
        self.inputDep.setObjectName(_fromUtf8("inputDep"))
        self.verticalLayout_7.addWidget(self.inputDep)

        self.verticalLayoutWidget_2 = QtGui.QWidget(self.centralwidget)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(40, 70, 94, 114))
        self.verticalLayoutWidget_2.setObjectName(_fromUtf8("verticalLayoutWidget_2"))
        self.verticalLayout_11 = QtGui.QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_11.setObjectName(_fromUtf8("verticalLayout_11"))

# ------------------------------------- PHOTO ----------------------------------------------
        self.photoLabel = QtGui.QLabel(self.verticalLayoutWidget_2)
        self.photoLabel.setText(_fromUtf8(""))
        self.photoLabel.setPixmap(QtGui.QPixmap(_fromUtf8("homer.png")))
        self.photoLabel.setScaledContents(True)
        self.photoLabel.setObjectName(_fromUtf8("photoLabel"))
        self.verticalLayout_11.addWidget(self.photoLabel)

        self.splitter = QtGui.QSplitter(self.centralwidget)
        self.splitter.setGeometry(QtCore.QRect(182, 250, 521, 283))
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.layoutWidget_4 = QtGui.QWidget(self.splitter)
        self.layoutWidget_4.setObjectName(_fromUtf8("layoutWidget_4"))
        self.verticalLayout_10 = QtGui.QVBoxLayout(self.layoutWidget_4)
        self.verticalLayout_10.setObjectName(_fromUtf8("verticalLayout_10"))

# ---------------------------------- COURSES ----------------------------------------------
        # DISCIPLINE LABEL
        self.disciplineLabel = QtGui.QLabel(self.layoutWidget_4)
        self.disciplineLabel.setObjectName(_fromUtf8("disciplineLabel"))
        self.verticalLayout_10.addWidget(self.disciplineLabel)
        # DISCIPLINE 1
        self.inputDiscipline1 = QtGui.QLineEdit(self.layoutWidget_4)
        self.inputDiscipline1.setObjectName(_fromUtf8("inputDiscipline1"))
        self.verticalLayout_10.addWidget(self.inputDiscipline1)
        # DISCIPLINE 2
        self.inputDiscipline2 = QtGui.QLineEdit(self.layoutWidget_4)
        self.inputDiscipline2.setObjectName(_fromUtf8("inputDiscipline2"))
        self.verticalLayout_10.addWidget(self.inputDiscipline2)
        # DISCIPLINE 3
        self.inputDiscipline3 = QtGui.QLineEdit(self.layoutWidget_4)
        self.inputDiscipline3.setObjectName(_fromUtf8("inputDiscipline3"))
        self.verticalLayout_10.addWidget(self.inputDiscipline3)
        # DISCIPLINE 4
        self.inputDiscipline4 = QtGui.QLineEdit(self.layoutWidget_4)
        self.inputDiscipline4.setObjectName(_fromUtf8("inputDiscipline4"))
        self.verticalLayout_10.addWidget(self.inputDiscipline4)
        # DISCIPLINE 5
        self.inputDiscipline5 = QtGui.QLineEdit(self.layoutWidget_4)
        self.inputDiscipline5.setObjectName(_fromUtf8("inputDiscipline5"))
        self.verticalLayout_10.addWidget(self.inputDiscipline5)
        # DISCIPLINE 6
        self.inputDiscipline6 = QtGui.QLineEdit(self.layoutWidget_4)
        self.inputDiscipline6.setObjectName(_fromUtf8("inputDiscipline6"))
        self.verticalLayout_10.addWidget(self.inputDiscipline6)
        # DISCIPLINE 7
        self.inputDiscipline7 = QtGui.QLineEdit(self.layoutWidget_4)
        self.inputDiscipline7.setObjectName(_fromUtf8("inputDiscipline7"))
        self.verticalLayout_10.addWidget(self.inputDiscipline7)
        # DISCIPLINE 8
        self.inputDiscipline8 = QtGui.QLineEdit(self.layoutWidget_4)
        self.inputDiscipline8.setObjectName(_fromUtf8("inputDiscipline8"))
        self.verticalLayout_10.addWidget(self.inputDiscipline8)

        self.layoutWidget_2 = QtGui.QWidget(self.splitter)
        self.layoutWidget_2.setObjectName(_fromUtf8("layoutWidget_2"))
        self.verticalLayout_8 = QtGui.QVBoxLayout(self.layoutWidget_2)
        self.verticalLayout_8.setObjectName(_fromUtf8("verticalLayout_8"))

        # DISCIPLINE CODE LABEL
        self.codeLabel = QtGui.QLabel(self.layoutWidget_2)
        self.codeLabel.setObjectName(_fromUtf8("codeLabel"))
        self.verticalLayout_8.addWidget(self.codeLabel)
        # DISCIPLINE CODE 1
        self.inputCode1 = QtGui.QLineEdit(self.layoutWidget_2)
        self.inputCode1.setObjectName(_fromUtf8("inputCode1"))
        self.verticalLayout_8.addWidget(self.inputCode1)
        # DISCIPLINE CODE 2
        self.inputCode2 = QtGui.QLineEdit(self.layoutWidget_2)
        self.inputCode2.setObjectName(_fromUtf8("inputCode2"))
        self.verticalLayout_8.addWidget(self.inputCode2)
        # DISCIPLINE CODE 3
        self.inputCode3 = QtGui.QLineEdit(self.layoutWidget_2)
        self.inputCode3.setObjectName(_fromUtf8("inputCode3"))
        self.verticalLayout_8.addWidget(self.inputCode3)
        # DISCIPLINE CODE 4
        self.inputCode4 = QtGui.QLineEdit(self.layoutWidget_2)
        self.inputCode4.setObjectName(_fromUtf8("inputCode4"))
        self.verticalLayout_8.addWidget(self.inputCode4)
        # DISCIPLINE CODE 5
        self.inputCode5 = QtGui.QLineEdit(self.layoutWidget_2)
        self.inputCode5.setObjectName(_fromUtf8("inputCode5"))
        self.verticalLayout_8.addWidget(self.inputCode5)
        # DISCIPLINE CODE 6
        self.inputCode6 = QtGui.QLineEdit(self.layoutWidget_2)
        self.inputCode6.setObjectName(_fromUtf8("inputCode6"))
        self.verticalLayout_8.addWidget(self.inputCode6)
        # DISCIPLINE CODE 7
        self.inputCode7 = QtGui.QLineEdit(self.layoutWidget_2)
        self.inputCode7.setObjectName(_fromUtf8("inputCode7"))
        self.verticalLayout_8.addWidget(self.inputCode7)
        # DISCIPLINE CODE 8
        self.inputCode8 = QtGui.QLineEdit(self.layoutWidget_2)
        self.inputCode8.setObjectName(_fromUtf8("inputCode8"))
        self.verticalLayout_8.addWidget(self.inputCode8)
        RegisterProf.setCentralWidget(self.centralwidget)

# ---------------------------------- STATUS BAR ----------------------------------------------
        self.statusbar = QtGui.QStatusBar(RegisterProf)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        RegisterProf.setStatusBar(self.statusbar)
# ---------------------------------------------------------------------------------------------------
        self.retranslateUi(RegisterProf)    ## Method that atribuites text to each data
        QtCore.QMetaObject.connectSlotsByName(RegisterProf)

# ---------------------------------- METHOD WITH TEXT ----------------------------------------------
    def retranslateUi(self, RegisterProf):
        RegisterProf.setWindowTitle(_translate("RegisterProf", "Register", None))
        self.titleLabel.setText(_translate("RegisterProf", "NEW REGISTRATION", None))
        self.groupLabel.setText(_translate("RegisterProf", "TEACHER", None))
        self.nameLabel.setText(_translate("RegisterProf", "Name", None))
        self.IDLabel.setText(_translate("RegisterProf", "ID", None))
        self.depLabel.setText(_translate("RegisterProf", "Department", None))

        self.saveButton.setText(_translate("RegisterProf", "Save", None))
        self.cancelButton.setText(_translate("RegisterProf", "Cancel", None))

        self.disciplineLabel.setText(_translate("RegisterProf", "DISCIPLINES", None))
        self.codeLabel.setText(_translate("RegisterProf", "CODE", None))
#-----------------------------------------------------------------------------------------------------------
def main():
    app = QtGui.QApplication(sys.argv)
    w = Ui_RegisterProf(None)
    w.show()
    app.exec_()

if __name__ == '__main__':
    main()
