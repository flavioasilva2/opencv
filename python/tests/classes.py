#!/usr/bin/env python
#-*-coding: utf-8-*-
import cv2
import sys
import numpy
import getopt
import os
import pymongo
import bson
import json

classifier = cv2.CascadeClassifier("haarcascade_frontalface_alt.xml")
webcam = cv2.VideoCapture(0)

class cam:
	__camera = None
	__faces = []
	__size = None

	def __init__(self):
		self.__size = 2

	def setCamera(self, camera):
		self.__camera = camera
	def getCamera(self):
		return self.__camera
	def setFaces(self, face):
		self.__faces = face
	def getFaces(self):
		return self.__faces
	def setSize(self, size):
		self.__size = size
	def getSize(self):
		return self.__size

	def getFrame(self):
		frame = []
		try:
			(self.__camera, frame) = webcam.read()
		except:
			print("Failed to open webcam. Trying again...")
		return frame

	def faceDetection(self, frame):

		mini = cv2.resize(frame, (int(frame.shape[1] / self.getSize()), int(frame.shape[0] / self.getSize())))
		img = classifier.detectMultiScale(mini, scaleFactor=1.1, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
		self.setFaces(img)
		if(len(self.getFaces()) == 0):
		 	self.setFaces([])
		#draw rects
		for x1, y1, x2, y2 in self.getFaces():
			cv2.rectangle(frame, (x1*self.getSize(), y1*self.getSize()), ((x2+x1), (y2*self.getSize()+y1*self.getSize())), (0,255,0),thickness=2)
		return img

	#Roi (Region of interest)
	def Roi(self,frame):
		out = []
		for x1,y1,x2,y2 in self.getFaces():
			out.append(frame[y1*self.getSize():(y1+y2)*self.getSize(),x1*self.getSize():(x1+x2)*self.getSize()])
		return out

class user(cam):
	__dbPath = []
	__userName = []
	__photoAdress = 1
	__database = None
	camera = None
	def __init__(self):
		camera = cam()
		self.face = camera.getFaces()

	def setDBPath(self,dbPath):
		self.__dbPath = dbPath
	def getDBPath(self):
		return self.__dbPath
	def setPhotoPath(self, args):
		for v in args:
			if (v.split('=')[0] == '--fotodb') and (len(v.split('=')) > 1):
				self.__photoAdress = v.split('=')[1]
	def getPhotoPath(self):
		return self.__photoAdress
	def setName(self, args):
		for v in args:
			if (v.split('=')[0] == '--Subject') and (len(v.split('=')) > 1):
				self.__userName = v.split('=')[1]
	def getName(self):
		return self.__userName

	def Register(self):
		if (len(self.face)) > 1:
			print('Please, register only one user.')
		else:
			#chamar tread aqui para coletar os dados
			self.setPhotoPath(sys.argv)	
			self.setName(sys.argv)
			userData = {"userName" : self.getName(), "UserCategory" : 'Student', "Schedule":{"Classes":{},"Time":{},"Days":{}}, "Observations": None}
		return userData

class FileDB:

	__file = None
	__db = None

	def __init__(self, f='db.json'):
		self.__file = f
		self.__db = self.__readFile()

	def insert(self, obj):
		if not obj.has_key('_id'):
			obj['_id'] = bson.objectid.ObjectId()
		self.__db.append(obj)
		self.__sync()
		return obj['_id']

	def find(self, id):
		for o in self.__db:
			if str(o['_id']) == id:
				return o

	def getAll(self):
		return self.__db

	def update(self, id, updatesDoc):
		pass

	def delete(self, id):
		for d in self.__db:
			if str(d['_id']) == id:
				self.__db.remove(d)
				self.__sync()

	def __readFile(self):
		out = []
		if not os.path.exists(self.__file):
			out = []
		else:
			fDB = open(self.__file, 'r')
			strDB = fDB.read()
			fDB.close()
			if len(strDB) == 0:
				out = []
			else:
				tmpDB = json.loads(strDB)
				for i in tmpDB:
					tmp = i
					tmp['_id'] = bson.objectid.ObjectId(tmp['_id'])
					out.append(tmp)
		return out

	def __sync(self):
		toWriteObj = []
		for o in self.__db:
			tmp = o
			tmp['_id'] = str(tmp['_id'])
			toWriteObj.append(tmp)
		strDB = json.dumps(toWriteObj)
		fDB = open(self.__file, 'w')
		fDB.write(strDB)
		fDB.close()

class Database:

	haarFace = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_alt.xml")
	__camera = None
	__us = None
	__img = []
	__subjectName = None
	__photoDBPath = None
	__connectionString = None
	__databaseName = None
	__connection = None
	__subjectDBPath = 'Subject'
	def __init__(self):
		self.__connectionString = None
		self.__camera = cam()
		self.__us = user()
		self.__img = self.__camera.getFaces()
		self.__photoDBPath = 'fotodb'

	def setsubjectDBPath(self,directoryPath):
		self.__subjectDBPath = directoryPath
	
	def getsubjectDBPath(self):
		return self.__subjectDBPath

	#Using MONGODB
	# def setconnectionString(self,string):
	# 	self.__connectionString = string

	# def getconnectionString(self):
	# 	return self.__connectionString

	# def setdatabaseName(self, dbname):
	# 	self.__databaseName = dbname

	# def getdatabaseName(self):
	# 	return self.__databaseName

	# def dbConnection(self):
	# 	if self.__connection == None:
	# 		self.__connection = pymongo.MongoClient(self.getconnectionString())
	# 	return self.__connection

	# def getconnectionString(self):
	# 	return self.__connectionString

	# def find(self, collection, id):
	# 	client = self.dbConnection()
	# 	db = self.getdatabase(client)
	# 	document = None
	# 	# objID = bson.objectid.ObjectId()
	# 	try:
	# 		document = db[collection].find_one(obj)
	# 	except:
	# 		print('DocumentID did not load.')
	# 	return document

	# def getdatabase(self, client):
	# 	return client[self.__databaseName]

	# def insert(self, collection, doc):
	# 	client = self.dbConnection()
	# 	db = self.getdatabaseName()
	# 	documentID = None
	# 	try:
	# 		documentID = db[collection].insert_one(doc)
	# 	except:
	# 		print('It was not __photoAdressible to register the user.')

	# def deleteUserDB(self, collection, id):
	# 	client = self.dbConnection()
	# 	db = self.getdatabase(client)
	# 	oid = json.objectid.Objectid(id)
	# 	result = None
	# 	try:
	# 		result = db[collection].delete_one({'_id': oid})
	# 	except:
	# 		print('It was not possible to delete the object.')
	# 	return result
	def deletephotoDB(self,collection,id):
		os.rmdir(id)

	def saveOnFotoDB(self, directoryPath, user):
		photos = []
		self.setsubjectDBPath(os.path.join(self.__photoDBPath, directoryPath))
		self.createDB()
		(sdir, empty, photos) = os.walk(self.getsubjectDBPath()).next()
		currentIndex = 0
		
		if(not(len(photos) == 0)):
			indices = []
			for name in photos:
				indices.append(name.split('.')[0])
			indices.sort()
			currentIndex = int(indices.pop()) + 1
		photoName = os.path.join(self.getsubjectDBPath() , str(currentIndex) + '.png')
		cv2.imwrite(photoName,user)


	def createDB(self):
		bolean = os.path.exists(self.__photoDBPath)
		if(bolean == False):
			print('It was not possible to open photo DB')
			os.mkdir(self.__photoDBPath)
		else:
			bolean = os.path.exists(self.getsubjectDBPath())
			if(bolean == False):
				print('Photo DB not found, building one called as ' + self.getsubjectDBPath())
				os.mkdir(self.getsubjectDBPath())
	
	def loadDB(self):

		load = []
		if(not (os.path.exists(self.__photoDBPath))):
			self.createDB()
		(db, usersDBFolders, empty) = os.walk(self.__photoDBPath).next()
		if os.listdir(self.__photoDBPath) == []:
			print('Any user registered')
		else:
			for u in usersDBFolders:
				userDBFolder = os.path.join(db, u)
				(folder, empty, fotosFromDB) = os.walk(userDBFolder).next()
				tmpUser = {}
				tmpUser['user'] = u
				tmpUser['label'] = usersDBFolders.index(u)
				fotos = []
				for f in fotosFromDB:
					fotoPath = os.path.join(userDBFolder, f)
					fotos.append(cv2.imread(fotoPath, cv2.IMREAD_REDUCED_GRAYSCALE_2))
					tmpUser['fotos'] = fotos
					load.append(tmpUser)
		return load

class picture(cam):
	haarFace = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_alt.xml")
	(im_width, im_height) = (None, None)
	__images = None
	__model = None
	__lables = None
	camera = None
	__mongodb = None
	scale = None

	def __init__(self):
		camera = cam()
		self.face = camera.getFaces()
		self.__mongodb = Database()
		(self.im_width, self.im_height) = (112, 92)
		self.scale = camera.getSize()

	def setImage(self,image):
		self.__images = image
	def getImage(self):
		return self.__images
	def setLable(self,lable):
		self.__lables = lable
	def getLable(self):
		return self.__lables

	def imageProcessing(self, frame):
		frameGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		frameGrayEq = cv2.equalizeHist(frameGray)
		return frameGrayEq

	def getUserByLabel(self, label, load):
		for u in load:
			if u['label'] == label:
				return u['user']

	def imageRecognition(self, img, frame, recognizer, load):
		out = []
		imgs = []
		user = {}

		for x1, y1, x2, y2 in frame:
			imgs.append(img[y1*self.scale:(y1+y2)*self.scale, x1*self.scale:(x1+x2)*self.scale])

		for i in imgs:
			index = imgs.index(i)
			(label, recognitionThreshold) = recognizer.predict(i)
			user['coord'] = frame[index]
			userName = self.getUserByLabel(label,load)
			user['name'] = userName
			user['threshold'] = recognitionThreshold
			out.append(user)

		return out

	def draw(self, frame, todraw):
		color = (0, 255, 0)
		for obj in todraw:
			print(obj)
			(x1, y1, x2, y2) = obj['coord']
			cv2.rectangle(frame, (x1*self.scale, y1*self.scale), ((x2+x1)*self.scale, (y2+y1)*self.scale), (0,255,0), 2)
			cv2.putText(frame, obj['name'] + ', ' + ("%3.2f" % obj['threshold']) + '%', (x1*self.scale+10, y1*self.scale-10), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,255,0) , 1, cv2.LINE_AA)
