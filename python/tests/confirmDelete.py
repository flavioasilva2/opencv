# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
import sys
import os

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_DeleteProfile(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

    def __closeWindow(self):
        self.close()

    def setupUi(self, DeleteProfile):
        DeleteProfile.setObjectName(_fromUtf8("DeleteProfile"))
        DeleteProfile.resize(844, 595)
        self.centralwidget = QtGui.QWidget(DeleteProfile)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.courseLabel_3 = QtGui.QLabel(self.centralwidget)
        self.courseLabel_3.setGeometry(QtCore.QRect(480, 32, 58, 17))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.courseLabel_3.setFont(font)
        self.courseLabel_3.setObjectName(_fromUtf8("courseLabel_3"))
        self.splitter_2 = QtGui.QSplitter(self.centralwidget)
        self.splitter_2.setGeometry(QtCore.QRect(53, 423, 241, 0))
        self.splitter_2.setOrientation(QtCore.Qt.Vertical)
        self.splitter_2.setObjectName(_fromUtf8("splitter_2"))
        self.splitter_4 = QtGui.QSplitter(self.splitter_2)
        self.splitter_4.setOrientation(QtCore.Qt.Horizontal)
        self.splitter_4.setObjectName(_fromUtf8("splitter_4"))
        self.accessLabel = QtGui.QLabel(self.centralwidget)
        self.accessLabel.setGeometry(QtCore.QRect(10, 21, 431, 101))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.accessLabel.setPalette(palette)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.NoAntialias)
        self.accessLabel.setFont(font)
        self.accessLabel.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.accessLabel.setStyleSheet(_fromUtf8("permissionLabel->setStyleSheet(\"color: green\");"))
        self.accessLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.accessLabel.setObjectName(_fromUtf8("accessLabel"))
        self.layoutWidget_3 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_3.setGeometry(QtCore.QRect(11, 121, 431, 121))
        self.layoutWidget_3.setObjectName(_fromUtf8("layoutWidget_3"))
        self.horizontalLayout_7 = QtGui.QHBoxLayout(self.layoutWidget_3)
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        spacerItem = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem)
        self.photoLabel_2 = QtGui.QLabel(self.layoutWidget_3)
        self.photoLabel_2.setFrameShape(QtGui.QFrame.NoFrame)
        self.photoLabel_2.setText(_fromUtf8(""))
        self.photoLabel_2.setPixmap(QtGui.QPixmap(_fromUtf8("homer.png")))
        self.photoLabel_2.setObjectName(_fromUtf8("photoLabel_2"))
        self.horizontalLayout_7.addWidget(self.photoLabel_2)
        spacerItem1 = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem1)
        self.line_2 = QtGui.QFrame(self.centralwidget)
        self.line_2.setGeometry(QtCore.QRect(450, -19, 20, 591))
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.layoutWidget = QtGui.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(14, 254, 431, 311))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.verticalLayout_16 = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout_16.setObjectName(_fromUtf8("verticalLayout_16"))
        self.horizontalLayout_8 = QtGui.QHBoxLayout()
        self.horizontalLayout_8.setObjectName(_fromUtf8("horizontalLayout_8"))
        self.courseLabel_4 = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.courseLabel_4.setFont(font)
        self.courseLabel_4.setObjectName(_fromUtf8("courseLabel_4"))
        self.horizontalLayout_8.addWidget(self.courseLabel_4)
        self.varGroup_2 = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varGroup_2.setFont(font)
        self.varGroup_2.setObjectName(_fromUtf8("varGroup_2"))
        self.horizontalLayout_8.addWidget(self.varGroup_2)
        self.verticalLayout_16.addLayout(self.horizontalLayout_8)
        self.horizontalLayout_9 = QtGui.QHBoxLayout()
        self.horizontalLayout_9.setObjectName(_fromUtf8("horizontalLayout_9"))
        self.nameLabel_2 = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.nameLabel_2.setFont(font)
        self.nameLabel_2.setObjectName(_fromUtf8("nameLabel_2"))
        self.horizontalLayout_9.addWidget(self.nameLabel_2)
        self.varName_2 = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varName_2.setFont(font)
        self.varName_2.setObjectName(_fromUtf8("varName_2"))
        self.horizontalLayout_9.addWidget(self.varName_2)
        self.verticalLayout_16.addLayout(self.horizontalLayout_9)
        self.horizontalLayout_10 = QtGui.QHBoxLayout()
        self.horizontalLayout_10.setObjectName(_fromUtf8("horizontalLayout_10"))
        self.IDLabel_2 = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.IDLabel_2.setFont(font)
        self.IDLabel_2.setObjectName(_fromUtf8("IDLabel_2"))
        self.horizontalLayout_10.addWidget(self.IDLabel_2)
        self.varID_2 = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varID_2.setFont(font)
        self.varID_2.setObjectName(_fromUtf8("varID_2"))
        self.horizontalLayout_10.addWidget(self.varID_2)
        self.verticalLayout_16.addLayout(self.horizontalLayout_10)
        self.horizontalLayout_11 = QtGui.QHBoxLayout()
        self.horizontalLayout_11.setObjectName(_fromUtf8("horizontalLayout_11"))
        self.courseLabel_5 = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.courseLabel_5.setFont(font)
        self.courseLabel_5.setObjectName(_fromUtf8("courseLabel_5"))
        self.horizontalLayout_11.addWidget(self.courseLabel_5)
        self.varCourse_2 = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varCourse_2.setFont(font)
        self.varCourse_2.setObjectName(_fromUtf8("varCourse_2"))
        self.horizontalLayout_11.addWidget(self.varCourse_2)
        self.verticalLayout_16.addLayout(self.horizontalLayout_11)
        self.layoutWidget_2 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_2.setGeometry(QtCore.QRect(480, 50, 341, 441))
        self.layoutWidget_2.setObjectName(_fromUtf8("layoutWidget_2"))
        self.horizontalLayout_12 = QtGui.QHBoxLayout(self.layoutWidget_2)
        self.horizontalLayout_12.setObjectName(_fromUtf8("horizontalLayout_12"))
        self.verticalLayout_11 = QtGui.QVBoxLayout()
        self.verticalLayout_11.setObjectName(_fromUtf8("verticalLayout_11"))
        self.verticalLayout_12 = QtGui.QVBoxLayout()
        self.verticalLayout_12.setObjectName(_fromUtf8("verticalLayout_12"))
        self.varClass1_2 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass1_2.setFont(font)
        self.varClass1_2.setObjectName(_fromUtf8("varClass1_2"))
        self.verticalLayout_12.addWidget(self.varClass1_2)
        self.varMonHours_9 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_9.setFont(font)
        self.varMonHours_9.setObjectName(_fromUtf8("varMonHours_9"))
        self.verticalLayout_12.addWidget(self.varMonHours_9)
        self.verticalLayout_11.addLayout(self.verticalLayout_12)
        self.verticalLayout_13 = QtGui.QVBoxLayout()
        self.verticalLayout_13.setObjectName(_fromUtf8("verticalLayout_13"))
        self.varClass2_2 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass2_2.setFont(font)
        self.varClass2_2.setObjectName(_fromUtf8("varClass2_2"))
        self.verticalLayout_13.addWidget(self.varClass2_2)
        self.varMonHours_10 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_10.setFont(font)
        self.varMonHours_10.setObjectName(_fromUtf8("varMonHours_10"))
        self.verticalLayout_13.addWidget(self.varMonHours_10)
        self.verticalLayout_11.addLayout(self.verticalLayout_13)
        self.verticalLayout_14 = QtGui.QVBoxLayout()
        self.verticalLayout_14.setObjectName(_fromUtf8("verticalLayout_14"))
        self.varClass3_2 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass3_2.setFont(font)
        self.varClass3_2.setObjectName(_fromUtf8("varClass3_2"))
        self.verticalLayout_14.addWidget(self.varClass3_2)
        self.varMonHours_11 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_11.setFont(font)
        self.varMonHours_11.setObjectName(_fromUtf8("varMonHours_11"))
        self.verticalLayout_14.addWidget(self.varMonHours_11)
        self.verticalLayout_11.addLayout(self.verticalLayout_14)
        self.verticalLayout_17 = QtGui.QVBoxLayout()
        self.verticalLayout_17.setObjectName(_fromUtf8("verticalLayout_17"))
        self.varClass4_2 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass4_2.setFont(font)
        self.varClass4_2.setObjectName(_fromUtf8("varClass4_2"))
        self.verticalLayout_17.addWidget(self.varClass4_2)
        self.varMonHours_12 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_12.setFont(font)
        self.varMonHours_12.setObjectName(_fromUtf8("varMonHours_12"))
        self.verticalLayout_17.addWidget(self.varMonHours_12)
        self.verticalLayout_11.addLayout(self.verticalLayout_17)
        self.horizontalLayout_12.addLayout(self.verticalLayout_11)
        self.verticalLayout_18 = QtGui.QVBoxLayout()
        self.verticalLayout_18.setObjectName(_fromUtf8("verticalLayout_18"))
        self.verticalLayout_19 = QtGui.QVBoxLayout()
        self.verticalLayout_19.setObjectName(_fromUtf8("verticalLayout_19"))
        self.varClass5_2 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass5_2.setFont(font)
        self.varClass5_2.setObjectName(_fromUtf8("varClass5_2"))
        self.verticalLayout_19.addWidget(self.varClass5_2)
        self.varMonHours_13 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_13.setFont(font)
        self.varMonHours_13.setObjectName(_fromUtf8("varMonHours_13"))
        self.verticalLayout_19.addWidget(self.varMonHours_13)
        self.verticalLayout_18.addLayout(self.verticalLayout_19)
        self.verticalLayout_20 = QtGui.QVBoxLayout()
        self.verticalLayout_20.setObjectName(_fromUtf8("verticalLayout_20"))
        self.varClass6_2 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass6_2.setFont(font)
        self.varClass6_2.setObjectName(_fromUtf8("varClass6_2"))
        self.verticalLayout_20.addWidget(self.varClass6_2)
        self.varMonHours_14 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_14.setFont(font)
        self.varMonHours_14.setObjectName(_fromUtf8("varMonHours_14"))
        self.verticalLayout_20.addWidget(self.varMonHours_14)
        self.verticalLayout_18.addLayout(self.verticalLayout_20)
        self.verticalLayout_21 = QtGui.QVBoxLayout()
        self.verticalLayout_21.setObjectName(_fromUtf8("verticalLayout_21"))
        self.varClass7_2 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass7_2.setFont(font)
        self.varClass7_2.setObjectName(_fromUtf8("varClass7_2"))
        self.verticalLayout_21.addWidget(self.varClass7_2)
        self.varMonHours_15 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_15.setFont(font)
        self.varMonHours_15.setObjectName(_fromUtf8("varMonHours_15"))
        self.verticalLayout_21.addWidget(self.varMonHours_15)
        self.verticalLayout_18.addLayout(self.verticalLayout_21)
        self.verticalLayout_22 = QtGui.QVBoxLayout()
        self.verticalLayout_22.setObjectName(_fromUtf8("verticalLayout_22"))
        self.varClass8_2 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass8_2.setFont(font)
        self.varClass8_2.setObjectName(_fromUtf8("varClass8_2"))
        self.verticalLayout_22.addWidget(self.varClass8_2)
        self.varMonHours_16 = QtGui.QLabel(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_16.setFont(font)
        self.varMonHours_16.setObjectName(_fromUtf8("varMonHours_16"))
        self.verticalLayout_22.addWidget(self.varMonHours_16)
        self.verticalLayout_18.addLayout(self.verticalLayout_22)
        self.horizontalLayout_12.addLayout(self.verticalLayout_18)
        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(0, 240, 461, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.layoutWidget_4 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_4.setGeometry(QtCore.QRect(560, 518, 178, 41))
        self.layoutWidget_4.setObjectName(_fromUtf8("layoutWidget_4"))
        self.horizontalLayout_13 = QtGui.QHBoxLayout(self.layoutWidget_4)
        self.horizontalLayout_13.setObjectName(_fromUtf8("horizontalLayout_13"))

        self.deleteButton = QtGui.QPushButton(self.layoutWidget_4)
        self.deleteButton.setEnabled(False)
        self.deleteButton.setToolTip(_fromUtf8(""))
        self.deleteButton.setStatusTip(_fromUtf8(""))
        self.deleteButton.setAutoDefault(False)
        self.deleteButton.setDefault(False)
        self.deleteButton.setFlat(False)
        self.deleteButton.setObjectName(_fromUtf8("deleteButton"))
        self.horizontalLayout_13.addWidget(self.deleteButton)

        self.cancelButton = QtGui.QPushButton(self.layoutWidget_4)
        self.cancelButton.setCheckable(False)
        self.cancelButton.setChecked(False)
        self.cancelButton.setDefault(True)
        self.cancelButton.setObjectName(_fromUtf8("cancelButton"))
        self.cancelButton.clicked.connect(self.__closeWindow)
        self.horizontalLayout_13.addWidget(self.cancelButton)

        DeleteProfile.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(DeleteProfile)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        DeleteProfile.setStatusBar(self.statusbar)

        self.retranslateUi(DeleteProfile)
        QtCore.QMetaObject.connectSlotsByName(DeleteProfile)

    def retranslateUi(self, DeleteProfile):
        DeleteProfile.setWindowTitle(_translate("DeleteProfile", "Delete Profile", None))
        self.courseLabel_3.setText(_translate("DeleteProfile", "Classes:", None))
        self.accessLabel.setText(_translate("DeleteProfile", "<html><head/><body><p><span style=\" font-size:18pt;\">ARE YOU SURE YOU WANT TO</span></p><p><span style=\" font-size:18pt;\">DELETE THIS PROFILE?</span></p></body></html>", None))
        self.courseLabel_4.setText(_translate("DeleteProfile", "Group:", None))
        self.varGroup_2.setText(_translate("DeleteProfile", "STUDENT", None))
        self.nameLabel_2.setText(_translate("DeleteProfile", "Name:", None))
        self.varName_2.setText(_translate("DeleteProfile", "HOMER SIMPSON", None))
        self.IDLabel_2.setText(_translate("DeleteProfile", "ID:", None))
        self.varID_2.setText(_translate("DeleteProfile", "17/0160000", None))
        self.courseLabel_5.setText(_translate("DeleteProfile", "Course:", None))
        self.varCourse_2.setText(_translate("DeleteProfile", "COMPUTER SCIENCE", None))
        self.varClass1_2.setText(_translate("DeleteProfile", "Class 1", None))
        self.varMonHours_9.setText(_translate("DeleteProfile", "-- : -- | -- : --", None))
        self.varClass2_2.setText(_translate("DeleteProfile", "Class 2", None))
        self.varMonHours_10.setText(_translate("DeleteProfile", "-- : -- | -- : --", None))
        self.varClass3_2.setText(_translate("DeleteProfile", "Class 3", None))
        self.varMonHours_11.setText(_translate("DeleteProfile", "-- : -- | -- : --", None))
        self.varClass4_2.setText(_translate("DeleteProfile", "Class 4", None))
        self.varMonHours_12.setText(_translate("DeleteProfile", "-- : -- | -- : --", None))
        self.varClass5_2.setText(_translate("DeleteProfile", "Class 5", None))
        self.varMonHours_13.setText(_translate("DeleteProfile", "-- : -- | -- : --", None))
        self.varClass6_2.setText(_translate("DeleteProfile", "Class 6", None))
        self.varMonHours_14.setText(_translate("DeleteProfile", "-- : -- | -- : --", None))
        self.varClass7_2.setText(_translate("DeleteProfile", "Class 7", None))
        self.varMonHours_15.setText(_translate("DeleteProfile", "-- : -- | -- : --", None))
        self.varClass8_2.setText(_translate("DeleteProfile", "Class 8", None))
        self.varMonHours_16.setText(_translate("DeleteProfile", "-- : -- | -- : --", None))
        self.deleteButton.setText(_translate("DeleteProfile", "Delete", None))
        self.cancelButton.setText(_translate("DeleteProfile", "Cancel", None))
#-----------------------------------------------------------------------------------------------------------
def main():
    app = QtGui.QApplication(sys.argv)
    w = Ui_DeleteProfile(None)
    w.show()
    app.exec_()

if __name__ == '__main__':
    main()
