#!/usr/bin/env python
#-*-coding: utf-8-*-
#University of Brasilia - CIC
#Abstract: This program is suposed to recognize and track people on you database system. It is used Mongodb as database system in this program.
#Flavio Amaral e Silva
#Lukas Lorenz de Andrade
#Marcella Pantarotto
import cv2
import classes
import sys
import numpy
# import getopt
import os


# import threading
# from time import sleep
# from Tkinter import *

def main():
    key = None
    collection = 'users'
    camera = classes.cam()
    pic = classes.picture()
    db = classes.Database()
    db.setconnectionString("mongodb://opencv:umyo6tu69@mongo.fatest.xyz:27017/linfFaces")
    db.setdatabaseName('linfFaces')
    us = classes.user()
    while(not(key == 27)):
        frame = camera.getFrame()
        img = frame.copy()
        if(not(len(frame) == 0)):
            framegray = pic.imageProcessing(img)
            img = camera.faceDetection(framegray)
            roi = []
            for x1,y1,x2,y2 in img:
                roi.append(frame[y1*2:(y1+y2)*2,x1*2:(x1+x2)*2])
            #roi = camera.Roi(img)
            load = db.loadDB()
            if(not(len(load) == 0)):
                #training a picture model to face recognition
                trainImgs = []
                trainLabels = []
                for u in load:
                    for uImg in u['fotos']:
                        trainImgs.append(uImg)
                        trainLabels.append(u['label'])

                (Imgs, Labels) = [numpy.array(lis) for lis in [trainImgs, trainLabels]]
                model = cv2.face.createLBPHFaceRecognizer()
                model.train(Imgs, Labels)
                picture = pic.imageRecognition(framegray, img, model, load)
                pic.draw(frame, picture)
                key = cv2.waitKey(1)
                print('Press D to delete\n')
                if((key & 0xFF == ord('d'))):
                    #colocar thread aqui q dá os dados para deletar o usuário.
                    db.deleteUserDB(collection,User['ID'])
                    db.delephotoDB(collection,User['ID'])
            print('Press "r" to register\n')
            key = cv2.waitKey(1)
            # if(pic.imageRecognition(frame)['threshold']== 0  means person not registered
            # (picture[0]['threshold'] == 0)
            if((key & 0xFF == ord('r'))):
                i = 0
                User = us.Register()
                db.insert(collection, User)
                while(not (i ==10)):
                    db.saveOnFotoDB(User['ID'], roi[0])
                    i = 1+i
            flip = cv2.flip(frame,1,0)
            cv2.imshow('OpenCV - Press Esc to exit', frame)
            key = cv2.waitKey(1)
        else:
            key = 27
    #camera.webcam.release()
    cv2.destroyAllWindows()
main()
