# ***ABSTRACT***
The project is a Security system model writen in Python 2.7 to LINF (Laboratório de informática) at UnB - Universidade de Brasília. It permits the user to register and delete some record.
The project uses the Opencv-3.2.0 and its extension (contrib). Besides, it uses MongoDB - a oriented programing database - and python QT4. You can use the tutorial list to download and configure all of them.

OpenCV:

C++ = https://gitlab.com/flavioasilva2/opencv/blob/master/Cpp/TutorialDeInstalacaoEConfiguracaoC++

Python = https://gitlab.com/flavioasilva2/opencv/blob/master/python/tutorialOpenCVPython.txt


# ***COMMAND LINE***
To run the program use the following command line:
> ./FaceRecognition.py

If it not run, use:
> chmod +x FaceRecognition.py

Then run it again. Do not forget that haarcascade_frontalface_alt.xml, interface.py and classes.py must be at the same directory as FaceRecognition.py.

The programs FaceRecognition.py and classes.py can be found in python folder and test folder - inside of python. In test folder it is integraded with the db and the interface.
# ***DESCRIPTION***

- FaceRecogniton.py: It is the main program that you must call in the command line if you want to run this project. Besides, It does the interface between classes.py and interface.py.
I other words, it calls the methods whose are statemented in classes.py and inteface.py's classes.

- classes.py: It is compound by four classes which are cam, picture, dataBase and user. The first one is responsible for to open the webcam and taking the frames. The second one takes that frames and process the image and recognize,
changing the scale and color to optmize the process - all of this is made using the Opencv methods. Then, the dataBase class does the connection with MongoDB - wich is an OOP database -
and loads the picture directories to the frames be compared with the ones inside of fotodb. Finally, the user class is responsible for take the user's personal data and redirect it to dataBase class.

- Interface.py: It does the inface between user and the program.

- haarcascade_frontalface_alt.xml: given by opencv
# ***Diagrams***

> ***Class UML Diagram***

![Class Diagram](images/BlankDiagram-Page1.1.png)

> ***Sequency UML Diagram***

![Sequency Diagram](images/BlankDiagram-Page1.png)


# ***INTERFACE***
## **Development**
The project's interface was desenvolved with Qt for Python and it's Designer.It can be found in the folder test and GUI. The GUI folder has the detection algorithm and the test folder - inside of python - has the recognition and detection.
The command lines for installation were:
```
sudo apt-get install pip3 pyqt5
sudo apt-get install python-qt4 qt4-designer
```
With the Qt Designer we were able to create the skeleton of all the windows layouts. After that, with the folloiwing command line, we were able to convert the `.ui` file into
a Python `.py` file.
```
pyuic4 file_name.ui -o file_name.py
```
Once we had all the `.py` files of the layouts, that we were able to develop the methods and functionalities of each element of the layout.

The use of different threads was implemented in this project, so that we could open the webcam to capture the video, do the detection and recognition of the faces and open the
layout windows. To caputre the image from the webcam we used a queue to save the camera shots, to be screened after. And we used call back methods to make the functionalities work.

## **Sequence**
The first and main window is the `access.py` file. That initializes the webcam and constantly looks for a face to show up in the video. Once a face is detected, the recognition algorithm starts.
![access.py](GUI/printscreens/access.png)

If the person is not inside the data base, the Access Denied Window from `accessDenied.py` opens up and through a button, allows the person to be registered into the system.
![accessDenied.py](GUI/printscreens/accessDenied.png)

Clicking in **register button** a menu opens up, giving the oportunity to choose in which category the profile will be registered into. Clicking in any of the options, it's respective register input window will open up, with spaces to insert the person's data.


![registerMenu.py](GUI/printscreens/registerMenu.png)
![registerWindow](GUI/printscreens/register.gif)


The **cancel button**, in all windows, stops the action and goes back one window.

Now if the person is inside the data base, it shows up the Access Granted Window. Depending in the category that the profile is registered into, it can be the `accessFree.py` file, for students and teachers. The `accessFreeOther.py` file for all the employees that may work inside the laboratory. And the `accessFreeForeing.py`, for people that aren't a part of the university's staff, that may work sporadically inside the labs.

![accessFreeWindow](GUI/printscreens/accessFree.gif)

Since the display of the data changes in each category, we have different files that represent the different ways the data is inputed and shown.

Once the person's profile data shows up in the window with the webcam video, we have a button that allows us to delete that profile. Clicking on it, a confirmation window opens up, giving a chance for the action to be canceled. If not, that persons profile is erased from the dada base.

![confirmDeleteWindow](GUI/printscreens/confirmDelete.gif)


Now if we want to delete someone's profile, that isn't present in the laboratory being recognized
by the webcam video, in the initial window in `access.py`, there is a **delete button**, that opens a delete menu, with each catagory being an option to choosen from. Once that's selected, a window from `search.py` file opens up with boxes to type the person's *name* and *id*. Once that person's profile is found in the data base, a confirmation window
opens up, giving one last chance to not remove that person's profile from the system.



![deleteMenu.py](GUI/printscreens/deleteMenu.png)
![search.py](GUI/printscreens/search.png)

# ***References***

https://noahingham.com/blog/facerec-python.html

https://pythonspot.com/en/pyqt4/

https://wiki.python.org/moin/PyQt/Tutorials

https://wiki.python.org.br/ComoUsarPyQt

https://www.lucidchart.com

http://zetcode.com/gui/pyqt4/
