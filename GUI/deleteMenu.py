#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
import sys
import os
import search

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_DeleteMenu(QtGui.QMainWindow):
    __searchWindow = None

    def getSearchWindow(self):
        return self.__searchWindow

    def setSearchWindow(self, win):
        self.__searchWindow = win

    def searchWindowCallback(self):
        self.getSearchWindow().show()

    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

    def __closeWindow(self):
        self.close()

# ---------------- Method that adds Widgets to the window class --------------------------------------
    def setupUi(self, DeleteMenu):
        DeleteMenu.setObjectName(_fromUtf8("DeleteMenu"))
        DeleteMenu.resize(801, 600)
        self.centralwidget = QtGui.QWidget(DeleteMenu)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.splitter_3 = QtGui.QSplitter(self.centralwidget)
        self.splitter_3.setGeometry(QtCore.QRect(200, 20, 431, 551))
        self.splitter_3.setOrientation(QtCore.Qt.Vertical)
        self.splitter_3.setObjectName(_fromUtf8("splitter_3"))
        self.titleLabel = QtGui.QLabel(self.splitter_3)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("mry_KacstQurn"))
        font.setPointSize(20)
        self.titleLabel.setFont(font)
        self.titleLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.titleLabel.setFrameShape(QtGui.QFrame.Panel)
        self.titleLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.titleLabel.setLineWidth(3)
        self.titleLabel.setTextFormat(QtCore.Qt.AutoText)
        self.titleLabel.setScaledContents(False)
        self.titleLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.titleLabel.setObjectName(_fromUtf8("titleLabel"))
        self.layoutWidget = QtGui.QWidget(self.splitter_3)
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        spacerItem = QtGui.QSpacerItem(20, 28, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.chooseLabel = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setBold(False)
        font.setWeight(50)
        self.chooseLabel.setFont(font)
        self.chooseLabel.setObjectName(_fromUtf8("chooseLabel"))
        self.verticalLayout_2.addWidget(self.chooseLabel)
        spacerItem1 = QtGui.QSpacerItem(20, 28, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem1)
        self.splitter_2 = QtGui.QSplitter(self.splitter_3)
        self.splitter_2.setOrientation(QtCore.Qt.Horizontal)
        self.splitter_2.setObjectName(_fromUtf8("splitter_2"))
        self.splitter = QtGui.QSplitter(self.splitter_2)
        self.splitter.setOrientation(QtCore.Qt.Vertical)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.layoutWidget_2 = QtGui.QWidget(self.splitter)
        self.layoutWidget_2.setObjectName(_fromUtf8("layoutWidget_2"))
        self.verticalLayout = QtGui.QVBoxLayout(self.layoutWidget_2)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))

        self.studentButton = QtGui.QPushButton(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(False)
        font.setWeight(50)
        self.studentButton.setFont(font)
        self.studentButton.setObjectName(_fromUtf8("studentButton"))
        self.studentButton.clicked.connect(self.searchWindowCallback)
        self.verticalLayout.addWidget(self.studentButton)

        self.teacherButton = QtGui.QPushButton(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.teacherButton.setFont(font)
        self.teacherButton.setObjectName(_fromUtf8("teacherButton"))
        self.teacherButton.clicked.connect(self.searchWindowCallback)
        self.verticalLayout.addWidget(self.teacherButton)

        self.cleanerButton = QtGui.QPushButton(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.cleanerButton.setFont(font)
        self.cleanerButton.setObjectName(_fromUtf8("cleanerButton"))
        self.cleanerButton.clicked.connect(self.searchWindowCallback)
        self.verticalLayout.addWidget(self.cleanerButton)

        self.guardButton = QtGui.QPushButton(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.guardButton.setFont(font)
        self.guardButton.setObjectName(_fromUtf8("guardButton"))
        self.guardButton.clicked.connect(self.searchWindowCallback)
        self.verticalLayout.addWidget(self.guardButton)

        self.technicianButton = QtGui.QPushButton(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.technicianButton.setFont(font)
        self.technicianButton.setObjectName(_fromUtf8("technicianButton"))
        self.technicianButton.clicked.connect(self.searchWindowCallback)
        self.verticalLayout.addWidget(self.technicianButton)

        self.foreignButton = QtGui.QPushButton(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.foreignButton.setFont(font)
        self.foreignButton.setObjectName(_fromUtf8("foreignButton"))
        self.foreignButton.clicked.connect(self.searchWindowCallback)
        self.verticalLayout.addWidget(self.foreignButton)

        self.cancelButton = QtGui.QPushButton(self.centralwidget)
        self.cancelButton.setGeometry(QtCore.QRect(670, 520, 111, 27))
        self.cancelButton.setCheckable(False)
        self.cancelButton.setChecked(False)
        self.cancelButton.setDefault(True)
        self.cancelButton.setObjectName(_fromUtf8("cancelButton"))
        self.cancelButton.clicked.connect(self.__closeWindow)
        DeleteMenu.setCentralWidget(self.centralwidget)

        self.statusbar = QtGui.QStatusBar(DeleteMenu)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        DeleteMenu.setStatusBar(self.statusbar)

        self.retranslateUi(DeleteMenu)
        QtCore.QMetaObject.connectSlotsByName(DeleteMenu)

    def retranslateUi(self, DeleteMenu):
        DeleteMenu.setWindowTitle(_translate("DeleteMenu", "Delete Menu", None))
        self.titleLabel.setText(_translate("DeleteMenu", "DELETE PROFILE", None))
        self.chooseLabel.setText(_translate("DeleteMenu", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">Please choose the desired category:</span></p></body></html>", None))
        self.studentButton.setText(_translate("DeleteMenu", "Student", None))
        self.teacherButton.setText(_translate("DeleteMenu", "Teacher", None))
        self.cleanerButton.setText(_translate("DeleteMenu", "Cleaner", None))
        self.guardButton.setText(_translate("DeleteMenu", "Guard", None))
        self.technicianButton.setText(_translate("DeleteMenu", "Technician", None))
        self.foreignButton.setText(_translate("DeleteMenu", "Foreign", None))
        self.cancelButton.setText(_translate("DeleteMenu", "Cancel", None))

#-----------------------------------------------------------------------------------------------------------
def initalizeSearchWindow(mainWindow):
    s = search.Ui_Search(None)
    s.hide()
    mainWindow.setSearchWindow(s)

def main():
    app = QtGui.QApplication(sys.argv)
    deleteWindow = Ui_DeleteMenu(None)
    deleteWindow.show()
    initalizeSearchWindow(deleteWindow)
    app.exec_()

if __name__ == '__main__':
    main()
