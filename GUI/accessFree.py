# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtCore, QtGui, uic
import os
import cv2
import numpy as np
import threading
import time
import Queue
import confirmDelete

q = Queue.Queue()

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Capture(threading.Thread):
    __deleteButton = None
    __deleteButtonClickable = False
    __cam = None
    __queue = None
    __width = None
    __height = None
    __fps = None

    def setDeleteButton(self, button):
        self.__deleteButton = button

    def __setDeleteButtonEnabled(self):
        if self.__deleteButton != None:
            self.__deleteButtonClickable = True
            self.__deleteButton.setEnabled(self.__deleteButtonClickable)

    def __setDeleteButtonDisabled(self):
        if self.__deleteButton != None:
            self.__deleteButtonClickable = False
            self.__deleteButton.setEnabled(self.__deleteButtonClickable)

    def __init__(self, cam, queue, width, height, fps):
        threading.Thread.__init__(self)
        self.__cam = cam
        self.__queue = queue
        self.__width = width
        self.__height = height
        self.__fps = fps

    def run(self):
        haarFacePath = "haarcascades/haarcascade_frontalface_alt.xml"
        haarFace = cv2.CascadeClassifier(haarFacePath)

        capture = cv2.VideoCapture(self.__cam)
        capture.set(cv2.CAP_PROP_FRAME_WIDTH, self.__width)
        capture.set(cv2.CAP_PROP_FRAME_HEIGHT, self.__height)
        capture.set(cv2.CAP_PROP_FPS, self.__fps)

        tools = DetectionTools()
        while(self.__queue.qsize() < 10):
            retval, img = capture.read()
            face = tools.detect(img, haarFace)
            tools.drawRects(img, face, 1, (0, 255, 0))
            if len(face) != 0:
                self.__setDeleteButtonEnabled()
            else:
                self.__setDeleteButtonDisabled()

            if(self.__queue.qsize() < 10):
                self.__queue.put(img)
            else:
                capture.release()
                break

class DetectionTools:
    def detect(self, img, cascade):
        out = cascade.detectMultiScale(img, scaleFactor=1.1, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
        if len(out) == 0:
            return []
        return out

    def drawRects(self, img, pos, scale, color):
        for x1, y1, x2, y2 in pos:
            cv2.rectangle(img, (x1*scale, y1*scale), ((x2+x1)*scale, (y2+y1)*scale), color, 2)

class OwnImageWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(OwnImageWidget, self).__init__(parent)
        self.image = None

    def setImage(self, image):
        self.image = image
        sz = image.size()
        self.setMinimumSize(sz)
        self.update()

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        if self.image:
            qp.drawImage(QtCore.QPoint(0, 0), self.image)
        qp.end()

class MyWindowClass(QtGui.QMainWindow):
    __captureThread = None
    __captureThreadStarted = False
    __deleteProfileWindow = None

    def setCaptureThread(self, t):
        self.__captureThread = t

    def getDeleteProfileWindow(self):
        return self.__deleteProfileWindow

    def setDeleteProfileWindow(self, win):
        self.__deleteProfileWindow = win

    def deleteProfileWindowCallback(self):
        self.getDeleteProfileWindow().show()

    def __init__(self, parent):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.window_width = self.ImgWidget.frameSize().width()
        self.window_height = self.ImgWidget.frameSize().height()
        self.ImgWidget = OwnImageWidget(self.ImgWidget)

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(1)

    def __startCaptureThread(self):
        if (self.__captureThreadStarted == False) and (self.__captureThread != None):
            self.__captureThread.start()
            self.__captureThreadStarted = True

    def update_frame(self):
        self.__startCaptureThread()
        if not q.empty():
            img = q.get()

            img_height, img_width, img_colors = img.shape
            scale_w = float(self.window_width) / float(img_width)
            scale_h = float(self.window_height) / float(img_height)
            scale = min([scale_w, scale_h])

            if scale == 0:
                scale = 1

            img = cv2.resize(img, None, fx=scale, fy=scale, interpolation = cv2.INTER_CUBIC)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            height, width, bpc = img.shape
            bpl = bpc * width
            image = QtGui.QImage(img.data, width, height, bpl, QtGui.QImage.Format_RGB888)
            self.ImgWidget.setImage(image)
# ----------------------------------------------------------------------------------------------------

# ---------------- Methods that add Widgets to the window class --------------------------------------
    def setupUi(self, permStand):
        permStand.setObjectName(_fromUtf8("permStand"))
        permStand.resize(1286, 475)
        self.centralwidget = QtGui.QWidget(permStand)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(9, 19, 641, 391))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.ImgWidget = QtGui.QWidget(self.groupBox)
        self.ImgWidget.setGeometry(QtCore.QRect(0, 30, 621, 361))
        self.ImgWidget.setObjectName(_fromUtf8("ImgWidget"))
        self.layoutWidget = QtGui.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(658, 108, 351, 121))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.horizontalLayout_6 = QtGui.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        spacerItem = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem)
        self.photoLabel = QtGui.QLabel(self.layoutWidget)
        self.photoLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.photoLabel.setText(_fromUtf8(""))
        self.photoLabel.setPixmap(QtGui.QPixmap(_fromUtf8("homer.png")))
        self.photoLabel.setObjectName(_fromUtf8("photoLabel"))
        self.horizontalLayout_6.addWidget(self.photoLabel)
        spacerItem1 = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem1)
        self.line_2 = QtGui.QFrame(self.centralwidget)
        self.line_2.setGeometry(QtCore.QRect(1008, -2, 16, 471))
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))

        self.registerButton = QtGui.QPushButton(self.centralwidget)
        self.registerButton.setEnabled(False)
        self.registerButton.setGeometry(QtCore.QRect(1062, 410, 85, 27))
        self.registerButton.setCheckable(False)
        self.registerButton.setChecked(False)
        self.registerButton.setObjectName(_fromUtf8("registerButton"))

        self.grantedLabel = QtGui.QLabel(self.centralwidget)
        self.grantedLabel.setGeometry(QtCore.QRect(660, 50, 341, 41))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.grantedLabel.setPalette(palette)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial Black"))
        font.setPointSize(20)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.NoAntialias)
        self.grantedLabel.setFont(font)
        self.grantedLabel.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.grantedLabel.setStyleSheet(_fromUtf8("permissionLabel->setStyleSheet(\"color: green\");"))
        self.grantedLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.grantedLabel.setObjectName(_fromUtf8("grantedLabel"))
        self.line_3 = QtGui.QFrame(self.centralwidget)
        self.line_3.setGeometry(QtCore.QRect(648, -2, 16, 471))
        self.line_3.setFrameShape(QtGui.QFrame.VLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.splitter_2 = QtGui.QSplitter(self.centralwidget)
        self.splitter_2.setGeometry(QtCore.QRect(700, 410, 241, 0))
        self.splitter_2.setOrientation(QtCore.Qt.Vertical)
        self.splitter_2.setObjectName(_fromUtf8("splitter_2"))
        self.splitter_3 = QtGui.QSplitter(self.splitter_2)
        self.splitter_3.setOrientation(QtCore.Qt.Horizontal)
        self.splitter_3.setObjectName(_fromUtf8("splitter_3"))
        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(657, 227, 351, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))

        self.deleteButton = QtGui.QPushButton(self.centralwidget)
        # self.deleteButton.setEnabled(False)
        self.deleteButton.setGeometry(QtCore.QRect(1153, 410, 85, 27))
        self.deleteButton.setToolTip(_fromUtf8(""))
        self.deleteButton.setStatusTip(_fromUtf8(""))
        self.deleteButton.setAutoDefault(False)
        self.deleteButton.setDefault(False)
        self.deleteButton.setFlat(False)
        self.deleteButton.setObjectName(_fromUtf8("deleteButton"))
        self.deleteButton.clicked.connect(self.deleteProfileWindowCallback)

        self.accessLabel = QtGui.QLabel(self.centralwidget)
        self.accessLabel.setGeometry(QtCore.QRect(667, 8, 341, 41))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.accessLabel.setPalette(palette)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(25)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.NoAntialias)
        self.accessLabel.setFont(font)
        self.accessLabel.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.accessLabel.setStyleSheet(_fromUtf8("permissionLabel->setStyleSheet(\"color: green\");"))
        self.accessLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.accessLabel.setObjectName(_fromUtf8("accessLabel"))
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(661, 241, 351, 201))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.verticalLayout_15 = QtGui.QVBoxLayout(self.widget)
        self.verticalLayout_15.setObjectName(_fromUtf8("verticalLayout_15"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.courseLabel_2 = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.courseLabel_2.setFont(font)
        self.courseLabel_2.setObjectName(_fromUtf8("courseLabel_2"))
        self.horizontalLayout_3.addWidget(self.courseLabel_2)
        self.varGroup = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varGroup.setFont(font)
        self.varGroup.setObjectName(_fromUtf8("varGroup"))
        self.horizontalLayout_3.addWidget(self.varGroup)
        self.verticalLayout_15.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.nameLabel = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.nameLabel.setFont(font)
        self.nameLabel.setObjectName(_fromUtf8("nameLabel"))
        self.horizontalLayout_4.addWidget(self.nameLabel)
        self.varName = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varName.setFont(font)
        self.varName.setObjectName(_fromUtf8("varName"))
        self.horizontalLayout_4.addWidget(self.varName)
        self.verticalLayout_15.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.IDLabel = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.IDLabel.setFont(font)
        self.IDLabel.setObjectName(_fromUtf8("IDLabel"))
        self.horizontalLayout_5.addWidget(self.IDLabel)
        self.varID = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varID.setFont(font)
        self.varID.setObjectName(_fromUtf8("varID"))
        self.horizontalLayout_5.addWidget(self.varID)
        self.verticalLayout_15.addLayout(self.horizontalLayout_5)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.courseLabel = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.courseLabel.setFont(font)
        self.courseLabel.setObjectName(_fromUtf8("courseLabel"))
        self.horizontalLayout.addWidget(self.courseLabel)
        self.varCourse = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varCourse.setFont(font)
        self.varCourse.setObjectName(_fromUtf8("varCourse"))
        self.horizontalLayout.addWidget(self.varCourse)
        self.verticalLayout_15.addLayout(self.horizontalLayout)
        self.courseLabel_3 = QtGui.QLabel(self.centralwidget)
        self.courseLabel_3.setGeometry(QtCore.QRect(1031, 51, 58, 17))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.courseLabel_3.setFont(font)
        self.courseLabel_3.setObjectName(_fromUtf8("courseLabel_3"))
        self.widget1 = QtGui.QWidget(self.centralwidget)
        self.widget1.setGeometry(QtCore.QRect(1031, 69, 251, 321))
        self.widget1.setObjectName(_fromUtf8("widget1"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.widget1)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.varClass1 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass1.setFont(font)
        self.varClass1.setObjectName(_fromUtf8("varClass1"))
        self.verticalLayout.addWidget(self.varClass1)
        self.varMonHours = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours.setFont(font)
        self.varMonHours.setObjectName(_fromUtf8("varMonHours"))
        self.verticalLayout.addWidget(self.varMonHours)
        self.verticalLayout_5.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.varClass2 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass2.setFont(font)
        self.varClass2.setObjectName(_fromUtf8("varClass2"))
        self.verticalLayout_2.addWidget(self.varClass2)
        self.varMonHours_2 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_2.setFont(font)
        self.varMonHours_2.setObjectName(_fromUtf8("varMonHours_2"))
        self.verticalLayout_2.addWidget(self.varMonHours_2)
        self.verticalLayout_5.addLayout(self.verticalLayout_2)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.varClass3 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass3.setFont(font)
        self.varClass3.setObjectName(_fromUtf8("varClass3"))
        self.verticalLayout_3.addWidget(self.varClass3)
        self.varMonHours_3 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_3.setFont(font)
        self.varMonHours_3.setObjectName(_fromUtf8("varMonHours_3"))
        self.verticalLayout_3.addWidget(self.varMonHours_3)
        self.verticalLayout_5.addLayout(self.verticalLayout_3)
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.varClass4 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass4.setFont(font)
        self.varClass4.setObjectName(_fromUtf8("varClass4"))
        self.verticalLayout_4.addWidget(self.varClass4)
        self.varMonHours_4 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_4.setFont(font)
        self.varMonHours_4.setObjectName(_fromUtf8("varMonHours_4"))
        self.verticalLayout_4.addWidget(self.varMonHours_4)
        self.verticalLayout_5.addLayout(self.verticalLayout_4)
        self.horizontalLayout_2.addLayout(self.verticalLayout_5)
        self.verticalLayout_10 = QtGui.QVBoxLayout()
        self.verticalLayout_10.setObjectName(_fromUtf8("verticalLayout_10"))
        self.verticalLayout_6 = QtGui.QVBoxLayout()
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.varClass5 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass5.setFont(font)
        self.varClass5.setObjectName(_fromUtf8("varClass5"))
        self.verticalLayout_6.addWidget(self.varClass5)
        self.varMonHours_5 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_5.setFont(font)
        self.varMonHours_5.setObjectName(_fromUtf8("varMonHours_5"))
        self.verticalLayout_6.addWidget(self.varMonHours_5)
        self.verticalLayout_10.addLayout(self.verticalLayout_6)
        self.verticalLayout_7 = QtGui.QVBoxLayout()
        self.verticalLayout_7.setObjectName(_fromUtf8("verticalLayout_7"))
        self.varClass6 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass6.setFont(font)
        self.varClass6.setObjectName(_fromUtf8("varClass6"))
        self.verticalLayout_7.addWidget(self.varClass6)
        self.varMonHours_6 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_6.setFont(font)
        self.varMonHours_6.setObjectName(_fromUtf8("varMonHours_6"))
        self.verticalLayout_7.addWidget(self.varMonHours_6)
        self.verticalLayout_10.addLayout(self.verticalLayout_7)
        self.verticalLayout_8 = QtGui.QVBoxLayout()
        self.verticalLayout_8.setObjectName(_fromUtf8("verticalLayout_8"))
        self.varClass7 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass7.setFont(font)
        self.varClass7.setObjectName(_fromUtf8("varClass7"))
        self.verticalLayout_8.addWidget(self.varClass7)
        self.varMonHours_7 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_7.setFont(font)
        self.varMonHours_7.setObjectName(_fromUtf8("varMonHours_7"))
        self.verticalLayout_8.addWidget(self.varMonHours_7)
        self.verticalLayout_10.addLayout(self.verticalLayout_8)
        self.verticalLayout_9 = QtGui.QVBoxLayout()
        self.verticalLayout_9.setObjectName(_fromUtf8("verticalLayout_9"))
        self.varClass8 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varClass8.setFont(font)
        self.varClass8.setObjectName(_fromUtf8("varClass8"))
        self.verticalLayout_9.addWidget(self.varClass8)
        self.varMonHours_8 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours_8.setFont(font)
        self.varMonHours_8.setObjectName(_fromUtf8("varMonHours_8"))
        self.verticalLayout_9.addWidget(self.varMonHours_8)
        self.verticalLayout_10.addLayout(self.verticalLayout_9)
        self.horizontalLayout_2.addLayout(self.verticalLayout_10)
        self.layoutWidget.raise_()
        self.groupBox.raise_()
        self.varMonHours.raise_()
        self.varMonHours_2.raise_()
        self.varMonHours_3.raise_()
        self.varMonHours_4.raise_()
        self.varMonHours_5.raise_()
        self.varMonHours_6.raise_()
        self.varMonHours_7.raise_()
        self.varMonHours_8.raise_()
        self.layoutWidget.raise_()
        self.line_2.raise_()
        self.registerButton.raise_()
        self.grantedLabel.raise_()
        self.line_3.raise_()
        self.splitter_2.raise_()
        self.line.raise_()
        self.deleteButton.raise_()
        self.accessLabel.raise_()
        self.varCourse.raise_()
        permStand.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(permStand)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        permStand.setStatusBar(self.statusbar)

        self.retranslateUi(permStand)
        QtCore.QMetaObject.connectSlotsByName(permStand)

    def retranslateUi(self, permStand):
        permStand.setWindowTitle(_translate("permStand", "Permission", None))
        self.groupBox.setTitle(_translate("permStand", "Video", None))
        self.registerButton.setText(_translate("permStand", "Register", None))
        self.grantedLabel.setText(_translate("permStand", "GRANTED", None))
        self.deleteButton.setText(_translate("permStand", "Delete", None))
        self.accessLabel.setText(_translate("permStand", "ACCESS:", None))
        self.courseLabel_2.setText(_translate("permStand", "Group:", None))
        self.varGroup.setText(_translate("permStand", "STUDENT", None))
        self.nameLabel.setText(_translate("permStand", "Name:", None))
        self.varName.setText(_translate("permStand", "HOMER SIMPSON", None))
        self.IDLabel.setText(_translate("permStand", "ID:", None))
        self.varID.setText(_translate("permStand", "17/0160000", None))
        self.courseLabel.setText(_translate("permStand", "Course:", None))
        self.varCourse.setText(_translate("permStand", "COMPUTER SCIENCE", None))
        self.courseLabel_3.setText(_translate("permStand", "Classes:", None))
        self.varClass1.setText(_translate("permStand", "Class 1", None))
        self.varMonHours.setText(_translate("permStand", "-- : -- | -- : --", None))
        self.varClass2.setText(_translate("permStand", "Class 2", None))
        self.varMonHours_2.setText(_translate("permStand", "-- : -- | -- : --", None))
        self.varClass3.setText(_translate("permStand", "Class 3", None))
        self.varMonHours_3.setText(_translate("permStand", "-- : -- | -- : --", None))
        self.varClass4.setText(_translate("permStand", "Class 4", None))
        self.varMonHours_4.setText(_translate("permStand", "-- : -- | -- : --", None))
        self.varClass5.setText(_translate("permStand", "Class 5", None))
        self.varMonHours_5.setText(_translate("permStand", "-- : -- | -- : --", None))
        self.varClass6.setText(_translate("permStand", "Class 6", None))
        self.varMonHours_6.setText(_translate("permStand", "-- : -- | -- : --", None))
        self.varClass7.setText(_translate("permStand", "Class 7", None))
        self.varMonHours_7.setText(_translate("permStand", "-- : -- | -- : --", None))
        self.varClass8.setText(_translate("permStand", "Class 8", None))
        self.varMonHours_8.setText(_translate("permStand", "-- : -- | -- : --", None))
#-----------------------------------------------------------------------------------------------------------
def initalizeDeleteProfileWindow(mainWindow):
    delete = Ui_DeleteProfile(None)
    captureT = Capture (0, q, 1920, 1080, 30)
    delete.setCaptureThread(captureT)
    delete.hide()
    mainWindow.setDeleteProfileWindow(delete)

def main():
    app = QtGui.QApplication(sys.argv)
    accessWindow = MyWindowClass(None)
    captureT = Capture(0, q, 1920, 1080, 30)
    accessWindow.setCaptureThread(captureT)
    accessWindow.show()
    deleteWindow = confirmDelete.Ui_DeleteProfile()
    accessWindow.setDeleteProfileWindow(deleteWindow)
    app.exec_()

if __name__ == '__main__':
    main()
