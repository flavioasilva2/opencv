#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtCore, QtGui, uic
import os
import cv2
import numpy as np
import threading
import time
import Queue
import registerMenu
import deleteMenu
import registerStudent
import search
# q = Queue.Queue()

class BalancedFrameQueue:

    __internalQueue = None
    __lastCaller = None
    __lastGet = None
    __lock = None

    def __init__(self):
        self.__internalQueue = Queue.Queue()
        self.__lock = threading.Lock()

    def getFrame(self, caller):
        #busy waiting
        while(self.__lock.locked()):
            pass
        self.__lock.acquire()
        if(self.__lastCaller == caller):
            self.__lastGet = self.__internalQueue.get()
        else:
            self.__lastCaller = caller
        self.__lock.release()
        return self.__lastGet

    def setFrame(self, f):
        self.__internalQueue.put(f)

    def empty(self):
        if self.__internalQueue != None:
            return self.__internalQueue.empty()

global Q = BalancedFrameQueue()

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Capture(threading.Thread):
    __deleteButton = None
    __deleteButtonClickable = False
    # __registerButton = None
    # __registerButtonClickable = False
    __cam = None
    __queue = None
    __width = None
    __height = None
    __fps = None

    def setDeleteButton(self, button):
        self.__deleteButton = button

    # def setRegisterButton(self, button):
    #     self.__registerButton = button

    def __setDeleteButtonEnabled(self):
        if self.__deleteButton != None:
            self.__deleteButtonClickable = True
            self.__deleteButton.setEnabled(self.__deleteButtonClickable)

    def __setDeleteButtonDisabled(self):
        if self.__deleteButton != None:
            self.__deleteButtonClickable = False
            self.__deleteButton.setEnabled(self.__deleteButtonClickable)

    # def __setRegisterButtonEnabled(self):
    #     if self.__registerButton != None:
    #         self.__registerButtonClickable = True
    #         self.__registerButton.setEnabled(self.__registerButtonClickable)
    #
    # def __setRegisterButtonDisabled(self):
    #     if self.__registerButton != None:
    #         self.__registerButtonClickable = False
    #         self.__registerButton.setEnabled(self.__registerButtonClickable)

    def __init__(self, cam, queue, width, height, fps):
        threading.Thread.__init__(self)
        self.__cam = cam
        self.__queue = queue
        self.__width = width
        self.__height = height
        self.__fps = fps

    def run(self):
        haarFacePath = "haarcascades/haarcascade_frontalface_alt.xml"
        haarFace = cv2.CascadeClassifier(haarFacePath)

        capture = cv2.VideoCapture(self.__cam)
        capture.set(cv2.CAP_PROP_FRAME_WIDTH, self.__width)
        capture.set(cv2.CAP_PROP_FRAME_HEIGHT, self.__height)
        capture.set(cv2.CAP_PROP_FPS, self.__fps)

        tools = DetectionTools()
        while(self.__queue.qsize() < 10):
            retval, img = capture.read()
            face = tools.detect(img, haarFace)
            tools.drawRects(img, face, 1, (0, 255, 0))
            if len(face) != 0:
                self.__setDeleteButtonEnabled()
                # self.__setRegisterButtonEnabled()
            else:
                self.__setDeleteButtonDisabled()
                # self.__setRegisterButtonDisabled()

            if(self.__queue.qsize() < 10):
                self.__queue.putFrame(img)
            else:
                capture.release()
                break

class DetectionTools:

    __model = None
    __photoDBPath = None
    __photoDB = None

    def __init__(self, fotoDBPath):
        self.__photoDBPath = fotoDBPath
        self.__loadDB()

    def detect(self, img, cascade):
        out = cascade.detectMultiScale(img, scaleFactor=1.1, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
        if len(out) == 0:
            return []
        return out

    def drawRects(self, img, pos, scale, color):
        for x1, y1, x2, y2 in pos:
            cv2.rectangle(img, (x1*scale, y1*scale), ((x2+x1)*scale, (y2+y1)*scale), color, 2)

    def __loadDB(self):
        # out = {
        #     user: string,
        #     label: int,
        #     fotos: imgArray
        # }
        load = []
        # dbPath = getArgs('--fotodb', sys.argv)
        if not os.path.exists(self.__photoDBPath):
            print('Nao foi possivel abrir o DB de fotos')
            sys.exit(1)
        (db, usersDBFolders, empty) = os.walk(dbPath).next()
        if os.listdir(dbPath) == []:
            print('No registered users')
            sys.exit(1)
        for u in usersDBFolders:
            userDBFolder = os.path.join(db, u)
            (folder, empty, fotosFromDB) = os.walk(userDBFolder).next()
            tmpUser = {}
            tmpUser['user'] = u
            tmpUser['label'] = usersDBFolders.index(u)
            fotos = []
            for f in fotosFromDB:
                fotoPath = os.path.join(userDBFolder, f)
                fotos.append(cv2.imread(fotoPath, cv2.IMREAD_REDUCED_GRAYSCALE_2))
            tmpUser['fotos'] = fotos
            load.append(tmpUser)
        self.__photoDB = load

class OwnImageWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(OwnImageWidget, self).__init__(parent)
        self.image = None

    def setImage(self, image):
        self.image = image
        sz = image.size()
        self.setMinimumSize(sz)
        self.update()

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        if self.image:
            qp.drawImage(QtCore.QPoint(0, 0), self.image)
        qp.end()

class MyWindowClass(QtGui.QMainWindow):
    __captureThread = None
    __captureThreadStarted = False
    __deleteMenuWindow = None
    # __registerMenuWindow = None

    def setCaptureThread(self, t):
        self.__captureThread = t

    def getDeleteMenuWindow(self):
        return self.__deleteMenuWindow

    def setDeleteMenuWindow(self, win):
        self.__deleteMenuWindow = win

    def deleteMenuWindowCallback(self):
        self.getDeleteMenuWindow().show()

    # def getRegisterMenuWindow(self):
    #     return self.__registerMenuWindow
    #
    # def setRegisterMenuWindow(self, win):
    #     self.__registerMenuWindow = win
    #
    # def registerMenuWindowCallback(self):
    #     self.getRegisterMenuWindow().show()

    def __init__(self, parent):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)
        # sem.acquire()
        # self.__captureThread = thread
        # self.__captureThread.start()

        self.window_width = self.ImgWidget.frameSize().width()
        self.window_height = self.ImgWidget.frameSize().height()
        self.ImgWidget = OwnImageWidget(self.ImgWidget)

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(1)

    def __startCaptureThread(self):
        if (self.__captureThreadStarted == False) and (self.__captureThread != None):
            self.__captureThread.start()
            self.__captureThreadStarted = True

    def update_frame(self):
        self.__startCaptureThread()
        if not Q.empty():
            # frame = q.get()
            # img = frame["img"]
            img = Q.getFrame('update_frame')

            img_height, img_width, img_colors = img.shape
            scale_w = float(self.window_width) / float(img_width)
            scale_h = float(self.window_height) / float(img_height)
            scale = min([scale_w, scale_h])

            if scale == 0:
                scale = 1

            img = cv2.resize(img, None, fx=scale, fy=scale, interpolation = cv2.INTER_CUBIC)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            height, width, bpc = img.shape
            bpl = bpc * width
            image = QtGui.QImage(img.data, width, height, bpl, QtGui.QImage.Format_RGB888)
            self.ImgWidget.setImage(image)
# ----------------------------------------------------------------------------------------------------

# ---------------- Methods that add Widgets to the window class --------------------------------------
    def setupUi(self, Access):
        Access.setObjectName(_fromUtf8("Access"))
        Access.resize(1302, 485)
        self.centralwidget = QtGui.QWidget(Access)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.video = QtGui.QGroupBox(self.centralwidget)
        self.video.setGeometry(QtCore.QRect(20, 20, 641, 391))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.video.sizePolicy().hasHeightForWidth())
        self.video.setSizePolicy(sizePolicy)
        self.video.setObjectName(_fromUtf8("video"))
        self.ImgWidget = QtGui.QWidget(self.video)
        self.ImgWidget.setGeometry(QtCore.QRect(0, 30, 621, 361))
        self.ImgWidget.setObjectName(_fromUtf8("ImgWidget"))

        self.deleteButton = QtGui.QPushButton(self.centralwidget)
        # self.deleteButton.setEnabled(False)
        self.deleteButton.setGeometry(QtCore.QRect(1168, 417, 85, 27))
        self.deleteButton.setToolTip(_fromUtf8(""))
        self.deleteButton.setStatusTip(_fromUtf8(""))
        self.deleteButton.setAutoDefault(False)
        self.deleteButton.setDefault(False)
        self.deleteButton.setFlat(False)
        self.deleteButton.setObjectName(_fromUtf8("deleteButton"))
        self.deleteButton.clicked.connect(self.deleteMenuWindowCallback)

        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(672, 234, 351, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))

        self.registerButton = QtGui.QPushButton(self.centralwidget)
        self.registerButton.setEnabled(False)
        self.registerButton.setGeometry(QtCore.QRect(1077, 417, 85, 27))
        self.registerButton.setCheckable(False)
        self.registerButton.setChecked(False)
        self.registerButton.setObjectName(_fromUtf8("registerButton"))
        # self.registerButton.clicked.connect(self.registerMenuWindowCallback)

        self.line_2 = QtGui.QFrame(self.centralwidget)
        self.line_2.setGeometry(QtCore.QRect(1023, 5, 16, 461))
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.line_3 = QtGui.QFrame(self.centralwidget)
        self.line_3.setGeometry(QtCore.QRect(663, 5, 16, 461))
        self.line_3.setFrameShape(QtGui.QFrame.VLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.accessLabel = QtGui.QLabel(self.centralwidget)
        self.accessLabel.setGeometry(QtCore.QRect(682, 15, 341, 41))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.accessLabel.setPalette(palette)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(25)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.NoAntialias)
        self.accessLabel.setFont(font)
        self.accesThread-1: Thu Jan 22 15:42:17 2009
Thread-1: Thu Jan 22 15:42:19 2009sLabel.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.accessLabel.setStyleSheet(_fromUtf8("permissionLabel->setStyleSheet(\"color: green\");"))
        self.accessLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.accessLabel.setObjectName(_fromUtf8("accessLabel"))
        self.layoutWidget_3 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_3.setGeometry(QtCore.QRect(673, 115, 351, 121))
        self.layoutWidget_3.setObjectName(_fromUtf8("layoutWidget_3"))
        self.horizontalLayout_12 = QtGui.QHBoxLayout(self.layoutWidget_3)
        self.horizontalLayout_12.setObjectName(_fromUtf8("horizontalLayout_12"))
        spacerItem = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_12.addItem(spacerItem)
        self.photoLabel_2 = QtGui.QLabel(self.layoutWidget_3)
        self.photoLabel_2.setFrameShape(QtGui.QFrame.NoFrame)
        self.photoLabel_2.setText(_fromUtf8(""))
        self.photoLabel_2.setPixmap(QtGui.QPixmap(_fromUtf8("homer.png")))
        self.photoLabel_2.setObjectName(_fromUtf8("photoLabel_2"))
        self.horizontalLayout_12.addWidget(self.photoLabel_2)
        spacerItem1 = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_12.addItem(spacerItem1)
        Access.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(Access)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        Access.setStatusBar(self.statusbar)

        self.retranslateUi(Access)
        QtCore.QMetaObject.connectSlotsByName(Access)

    def retranslateUi(self, Access):
        Access.setWindowTitle(_translate("Access", "Permission", None))
        self.video.setTitle(_translate("Access", "Video", None))

        self.deleteButton.setText(_translate("Access", "Delete", None))
        self.registerButton.setText(_translate("Access", "Register", None))
        self.accessLabel.setText(_translate("Access", "ACCESS:", None))
#-----------------------------------------------------------------------------------------------------------
def initalizeRegisterProfileWindow(mainWindow):
    reg = Ui_Register(None)
    captureT = Capture (0, Q, 1920, 1080, 30)
    reg.setCaptureThread(captureT)
    reg.hide()
    mainWindow.setRegisterProfileWindow(reg)

def initalizedeleteMenuWindow(mainWindow):
    delete = Ui_deleteMenu(None)
    captureT = Capture (0, Q, 1920, 1080, 30)
    delete.setCaptureThread(captureT)
    delete.hide()
    mainWindow.setDeleteMenuWindow(delete)

def main():
    app = QtGui.QApplication(sys.argv)
    accessWindow = MyWindowClass(None)
    captureT = Capture(0, Q, 1920, 1080, 30)
    accessWindow.setCaptureThread(captureT)
    accessWindow.show()

    # registerMenuWindow = registerMenu.Ui_Register()
    # accessWindow.setRegisterMenuWindow(registerMenuWindow)
    deleteMenuWindow = deleteMenu.Ui_DeleteMenu()
    searchWindow = search.Ui_Search()
    accessWindow.setDeleteMenuWindow(deleteMenuWindow)
    accessWindow.getDeleteMenuWindow().setSearchWindow(searchWindow)
    app.exec_()


if __name__ == '__main__':
    main()
