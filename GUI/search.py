# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
import sys
import os
import confirmDelete
import confirmDeleteOther
import confirmDeleteForeign

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Search(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

    def __closeWindow(self):
        self.close()

# ---------------- Method that adds Widgets to the window class --------------------------------------
    def setupUi(self, Search):
        Search.setObjectName(_fromUtf8("Search"))
        Search.resize(621, 530)
        self.centralwidget = QtGui.QWidget(Search)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.nameLabel_2 = QtGui.QLabel(self.centralwidget)
        self.nameLabel_2.setGeometry(QtCore.QRect(50, 227, 519, 17))
        self.nameLabel_2.setObjectName(_fromUtf8("nameLabel_2"))
        self.inputID = QtGui.QLineEdit(self.centralwidget)
        self.inputID.setGeometry(QtCore.QRect(50, 306, 519, 27))
        self.inputID.setObjectName(_fromUtf8("inputID"))
        self.IDLabel_2 = QtGui.QLabel(self.centralwidget)
        self.IDLabel_2.setGeometry(QtCore.QRect(50, 283, 519, 17))
        self.IDLabel_2.setObjectName(_fromUtf8("IDLabel_2"))
        self.inputName = QtGui.QLineEdit(self.centralwidget)
        self.inputName.setGeometry(QtCore.QRect(50, 250, 519, 27))
        self.inputName.setObjectName(_fromUtf8("inputName"))
        self.titleLabel = QtGui.QLabel(self.centralwidget)
        self.titleLabel.setGeometry(QtCore.QRect(90, 20, 431, 56))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("mry_KacstQurn"))
        font.setPointSize(20)
        self.titleLabel.setFont(font)
        self.titleLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.titleLabel.setFrameShape(QtGui.QFrame.Panel)
        self.titleLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.titleLabel.setLineWidth(3)
        self.titleLabel.setTextFormat(QtCore.Qt.AutoText)
        self.titleLabel.setScaledContents(False)
        self.titleLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.titleLabel.setObjectName(_fromUtf8("titleLabel"))
        self.chooseLabel = QtGui.QLabel(self.centralwidget)
        self.chooseLabel.setGeometry(QtCore.QRect(140, 130, 331, 28))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setBold(False)
        font.setWeight(50)
        self.chooseLabel.setFont(font)
        self.chooseLabel.setObjectName(_fromUtf8("chooseLabel"))
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(190, 410, 231, 41))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.widget)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))

        self.continueButton = QtGui.QPushButton(self.widget)
        self.continueButton.setObjectName(_fromUtf8("continueButton"))
        self.horizontalLayout.addWidget(self.continueButton)

        self.cancelButton = QtGui.QPushButton(self.widget)
        self.cancelButton.setCheckable(False)
        self.cancelButton.setChecked(False)
        self.cancelButton.setDefault(True)
        self.cancelButton.setObjectName(_fromUtf8("cancelButton"))
        self.cancelButton.clicked.connect(self.__closeWindow)
        self.horizontalLayout.addWidget(self.cancelButton)
        Search.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(Search)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        Search.setStatusBar(self.statusbar)

        self.retranslateUi(Search)
        QtCore.QMetaObject.connectSlotsByName(Search)

    def retranslateUi(self, Search):
        Search.setWindowTitle(_translate("Search", "Search", None))
        self.nameLabel_2.setText(_translate("Search", "Name", None))
        self.IDLabel_2.setText(_translate("Search", "ID", None))
        self.titleLabel.setText(_translate("Search", "DELETE PROFILE", None))
        self.chooseLabel.setText(_translate("Search", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">Please insert name and ID:</span></p></body></html>", None))
        self.continueButton.setText(_translate("Search", "Continue", None))
        self.cancelButton.setText(_translate("Search", "Cancel", None))
#-----------------------------------------------------------------------------------------------------------

def main():
    app = QtGui.QApplication(sys.argv)
    w = Ui_Search(None)
    w.show()
    app.exec_()

if __name__ == '__main__':
    main()
