#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
import sys
import os
import registerStudent
import registerTeacher
import registerCleaner
import registerGuard
import registerTechnician
import registerForeign

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Register(QtGui.QMainWindow):
    __studentWindow = None
    __teacherWindow = None
    __cleanerWindow = None
    __guardWindow = None
    __technicianWindow = None
    __foreignWindow = None

    def getStudentWindow(self):
        return self.__studentWindow

    def setStudentWindow(self, win):
        self.__studentWindow = win

    def getteacherWindow(self):
        return self.__teacherWindow

    def setTeacherWindow(self, win):
        self.__teacherWindow = win

    def getCleanerWindow(self):
        return self.__cleanerWindow

    def setCleanerWindow(self, win):
        self.__cleanerWindow = win

    def getGuardWindow(self):
        return self.__guardWindow

    def setGuardWindow(self, win):
        self.__guardWindow = win

    def getTechnicianWindow(self):
        return self.__technicianWindow

    def setTechnicianWindow(self, win):
        self.__technicianWindow = win

    def getForeignWindow(self):
        return self.__foreignWindow

    def setForeignWindow(self, win):
        self.__foreignWindow = win

    def studentWindowCallback(self):
        self.getStudentWindow().show()

    def teacherWindowCallback(self):
        self.getteacherWindow().show()

    def cleanerWindowCallback(self):
        self.getCleanerWindow().show()

    def guardWindowCallback(self):
        self.getGuardWindow().show()

    def technicianWindowCallback(self):
        self.getTechnicianWindow().show()

    def foreingWindowCallback(self):
        self.getForeignWindow().show()

    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

    def __closeWindow(self):
        self.close()

# ---------------- Method that adds Widgets to the window class --------------------------------------
    def setupUi(self, Register):
        Register.setObjectName(_fromUtf8("Register"))
        Register.resize(800, 600)
        self.centralwidget = QtGui.QWidget(Register)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.splitter_3 = QtGui.QSplitter(self.centralwidget)
        self.splitter_3.setGeometry(QtCore.QRect(210, 11, 431, 551))
        self.splitter_3.setOrientation(QtCore.Qt.Vertical)
        self.splitter_3.setObjectName(_fromUtf8("splitter_3"))

# ------------------------------- TITLE ----------------------------------------------------------------
        self.titleLabel = QtGui.QLabel(self.splitter_3)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("mry_KacstQurn"))
        font.setPointSize(20)
        self.titleLabel.setFont(font)
        self.titleLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.titleLabel.setFrameShape(QtGui.QFrame.Panel)
        self.titleLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.titleLabel.setLineWidth(3)
        self.titleLabel.setTextFormat(QtCore.Qt.AutoText)
        self.titleLabel.setScaledContents(False)
        self.titleLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.titleLabel.setObjectName(_fromUtf8("titleLabel"))

        self.widget = QtGui.QWidget(self.splitter_3)
        self.widget.setObjectName(_fromUtf8("widget"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.widget)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        spacerItem = QtGui.QSpacerItem(20, 28, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
# ------------------------------- CHOOSE TEXT ----------------------------------------------------------------
        self.chooseLabel = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setBold(False)
        font.setWeight(50)
        self.chooseLabel.setFont(font)
        self.chooseLabel.setObjectName(_fromUtf8("chooseLabel"))
        self.verticalLayout_2.addWidget(self.chooseLabel)
        spacerItem1 = QtGui.QSpacerItem(20, 28, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem1)

        self.splitter_2 = QtGui.QSplitter(self.splitter_3)
        self.splitter_2.setOrientation(QtCore.Qt.Horizontal)
        self.splitter_2.setObjectName(_fromUtf8("splitter_2"))
        self.splitter = QtGui.QSplitter(self.splitter_2)
        self.splitter.setOrientation(QtCore.Qt.Vertical)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.widget1 = QtGui.QWidget(self.splitter)
        self.widget1.setObjectName(_fromUtf8("widget1"))

# ------------------------------- BUTTONS ----------------------------------------------------------------
        self.verticalLayout = QtGui.QVBoxLayout(self.widget1)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        # STUDENT
        self.studentButton = QtGui.QPushButton(self.widget1)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(False)
        font.setWeight(50)
        self.studentButton.setFont(font)
        self.studentButton.setObjectName(_fromUtf8("studentButton"))
        self.studentButton.clicked.connect(self.studentWindowCallback)
        self.verticalLayout.addWidget(self.studentButton)
        # TEACHER
        self.teacherButton = QtGui.QPushButton(self.widget1)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.teacherButton.setFont(font)
        self.teacherButton.setObjectName(_fromUtf8("teacherButton"))
        self.teacherButton.clicked.connect(self.teacherWindowCallback)
        self.verticalLayout.addWidget(self.teacherButton)
        # CLEANER
        self.cleanerButton = QtGui.QPushButton(self.widget1)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.cleanerButton.setFont(font)
        self.cleanerButton.setObjectName(_fromUtf8("cleanerButton"))
        self.cleanerButton.clicked.connect(self.cleanerWindowCallback)
        self.verticalLayout.addWidget(self.cleanerButton)
        # GUARD
        self.guardButton = QtGui.QPushButton(self.widget1)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.guardButton.setFont(font)
        self.guardButton.setObjectName(_fromUtf8("guardButton"))
        self.guardButton.clicked.connect(self.guardWindowCallback)
        self.verticalLayout.addWidget(self.guardButton)
        # TECHNICIAN
        self.technicianButton = QtGui.QPushButton(self.widget1)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.technicianButton.setFont(font)
        self.technicianButton.setObjectName(_fromUtf8("technicianButton"))
        self.technicianButton.clicked.connect(self.technicianWindowCallback)
        self.verticalLayout.addWidget(self.technicianButton)
        # FOREIGN
        self.foreignButton = QtGui.QPushButton(self.widget1)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.foreignButton.setFont(font)
        self.foreignButton.setObjectName(_fromUtf8("foreignButton"))
        self.foreignButton.clicked.connect(self.foreingWindowCallback)
        self.verticalLayout.addWidget(self.foreignButton)
        Register.setCentralWidget(self.centralwidget)

        self.cancelButton = QtGui.QPushButton(self.centralwidget)
        self.cancelButton.setGeometry(QtCore.QRect(670, 520, 111, 27))
        self.cancelButton.setCheckable(False)
        self.cancelButton.setChecked(False)
        self.cancelButton.setDefault(True)
        self.cancelButton.setObjectName(_fromUtf8("cancelButton"))
        self.cancelButton.clicked.connect(self.__closeWindow)
        Register.setCentralWidget(self.centralwidget)

# ------------------------------ STATUS BAR------------------------------------------------------
        self.statusbar = QtGui.QStatusBar(Register)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        Register.setStatusBar(self.statusbar)
# ---------------------------------------------------------------------------------------------------

        self.retranslateUi(Register)    ## Method that atribuites text to each data
        QtCore.QMetaObject.connectSlotsByName(Register)

# ---------------------------------- METHOD WITH TEXT ----------------------------------------------
    def retranslateUi(self, Register):
        Register.setWindowTitle(_translate("Register", "Register", None))
        self.titleLabel.setText(_translate("Register", "NEW REGISTRATION", None))
        self.chooseLabel.setText(_translate("Register", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">Choose a category:</span></p></body></html>", None))

        self.studentButton.setText(_translate("Register", "Student", None))
        self.teacherButton.setText(_translate("Register", "Teacher", None))
        self.cleanerButton.setText(_translate("Register", "Cleaner", None))
        self.guardButton.setText(_translate("Register", "Guard", None))
        self.technicianButton.setText(_translate("Register", "Technician", None))
        self.foreignButton.setText(_translate("Register", "Foreign", None))
        self.cancelButton.setText(_translate("Register", "Cancel", None))
#-----------------------------------------------------------------------------------------------------------
def initalizeStudentWindow(mainWindow):
    student = registerStudent.Ui_RegisterStud(None)
    student.hide()
    mainWindow.setStudentWindow(student)

def initalizeteacherWindow(mainWindow):
    teacher = registerTeacher.Ui_RegisterProf(None)
    teacher.hide()
    mainWindow.setTeacherWindow(teacher)

def initalizeCleanerWindow(mainWindow):
    cleaner = registerCleaner.Ui_RegisterClean(None)
    cleaner.hide()
    mainWindow.setCleanerWindow(cleaner)

def initalizeGuardWindow(mainWindow):
    guard = registerGuard.Ui_RegisterGuard(None)
    guard.hide()
    mainWindow.setGuardWindow(guard)

def initalizeTechnicianWindow(mainWindow):
    technician = registerTechnician.Ui_RegisterTec(None)
    technician.hide()
    mainWindow.setTechnicianWindow(technician)

def initalizeForeingWindow(mainWindow):
    foreing = registerForeign.Ui_RegisterForeign(None)
    foreing.hide()
    mainWindow.setForeignWindow(foreing)

def main():
    app = QtGui.QApplication(sys.argv)
    registerWindow = Ui_Register(None)
    registerWindow.show()
    initalizeStudentWindow(registerWindow)
    initalizeteacherWindow(registerWindow)
    initalizeCleanerWindow(registerWindow)
    initalizeGuardWindow(registerWindow)
    initalizeTechnicianWindow(registerWindow)
    initalizeForeingWindow(registerWindow)
    app.exec_()


if __name__ == '__main__':
    main()
