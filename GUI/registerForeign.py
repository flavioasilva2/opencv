# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
import sys
import os

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_RegisterForeign(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

    def __closeWindow(self):
        self.close()

# ---------------- Method that adds Widgets to the window class --------------------------------------
    def setupUi(self, RegisterForeign):
        RegisterForeign.setObjectName(_fromUtf8("RegisterForeign"))
        RegisterForeign.resize(726, 600)
        self.centralwidget = QtGui.QWidget(RegisterForeign)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))

# -------------------------------- SCHEDULE  ----------------------------------------------
        # LABEL
        self.schedualLabel = QtGui.QLabel(self.centralwidget)
        self.schedualLabel.setGeometry(QtCore.QRect(370, 230, 111, 41))
        self.schedualLabel.setObjectName(_fromUtf8("schedualLabel"))
        # TXT BOX FOR HOURS
        self.f_hoursLabel = QtGui.QTextEdit(self.centralwidget)
        self.f_hoursLabel.setGeometry(QtCore.QRect(180, 270, 521, 261))
        self.f_hoursLabel.setObjectName(_fromUtf8("f_hoursLabel"))
        RegisterForeign.setCentralWidget(self.centralwidget)
# ----------------------------------- GROUP ----------------------------------------------
        self.groupLabel = QtGui.QLabel(self.centralwidget)
        self.groupLabel.setGeometry(QtCore.QRect(8, 10, 161, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("mry_KacstQurn"))
        font.setPointSize(17)
        self.groupLabel.setFont(font)

        self.groupLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.groupLabel.setFrameShape(QtGui.QFrame.Box)
        self.groupLabel.setFrameShadow(QtGui.QFrame.Sunken)
        self.groupLabel.setLineWidth(2)
        self.groupLabel.setTextFormat(QtCore.Qt.AutoText)
        self.groupLabel.setScaledContents(False)
        self.groupLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.groupLabel.setObjectName(_fromUtf8("groupLabel"))

        self.layoutWidget_9 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_9.setGeometry(QtCore.QRect(178, 70, 521, 111))
        self.layoutWidget_9.setObjectName(_fromUtf8("layoutWidget_9"))

# ------------------------------------- NAME ----------------------------------------------
        self.verticalLayout_17 = QtGui.QVBoxLayout(self.layoutWidget_9)
        self.verticalLayout_17.setObjectName(_fromUtf8("verticalLayout_17"))

        self.nameLabel = QtGui.QLabel(self.layoutWidget_9)
        self.nameLabel.setObjectName(_fromUtf8("nameLabel"))
        self.verticalLayout_17.addWidget(self.nameLabel)

        self.inputName = QtGui.QLineEdit(self.layoutWidget_9)
        self.inputName.setObjectName(_fromUtf8("inputName"))
        self.verticalLayout_17.addWidget(self.inputName)

# ------------------------------------ ID ----------------------------------------------
        self.IDLabel = QtGui.QLabel(self.layoutWidget_9)
        self.IDLabel.setObjectName(_fromUtf8("IDLabel"))
        self.verticalLayout_17.addWidget(self.IDLabel)

        self.inputID = QtGui.QLineEdit(self.layoutWidget_9)
        self.inputID.setObjectName(_fromUtf8("inputID"))
        self.verticalLayout_17.addWidget(self.inputID)

        self.verticalLayoutWidget_4 = QtGui.QWidget(self.centralwidget)
        self.verticalLayoutWidget_4.setGeometry(QtCore.QRect(38, 70, 94, 114))
        self.verticalLayoutWidget_4.setObjectName(_fromUtf8("verticalLayoutWidget_4"))

# ------------------------------------ PHOTO ----------------------------------------------
        self.verticalLayout_19 = QtGui.QVBoxLayout(self.verticalLayoutWidget_4)
        self.verticalLayout_19.setObjectName(_fromUtf8("verticalLayout_19"))

        self.photoLabel = QtGui.QLabel(self.verticalLayoutWidget_4)
        self.photoLabel.setText(_fromUtf8(""))
        self.photoLabel.setPixmap(QtGui.QPixmap(_fromUtf8("homer.png")))
        self.photoLabel.setScaledContents(True)
        self.photoLabel.setObjectName(_fromUtf8("photoLabel"))
        self.verticalLayout_19.addWidget(self.photoLabel)

        self.layoutWidget_10 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_10.setGeometry(QtCore.QRect(28, 440, 111, 101))
        self.layoutWidget_10.setObjectName(_fromUtf8("layoutWidget_10"))

# ------------------------------------- BUTTONS ----------------------------------------------
        self.verticalLayout_18 = QtGui.QVBoxLayout(self.layoutWidget_10)
        self.verticalLayout_18.setObjectName(_fromUtf8("verticalLayout_18"))
        # SAVE BUTTON
        self.saveButton = QtGui.QPushButton(self.layoutWidget_10)
        self.saveButton.setAcceptDrops(False)
        self.saveButton.setToolTip(_fromUtf8(""))
        self.saveButton.setStatusTip(_fromUtf8(""))
        self.saveButton.setAutoDefault(False)
        self.saveButton.setDefault(True)
        self.saveButton.setObjectName(_fromUtf8("saveButton"))
        self.verticalLayout_18.addWidget(self.saveButton)
        # CANCEL BUTTON
        self.cancelButton = QtGui.QPushButton(self.layoutWidget_10)
        self.cancelButton.setAcceptDrops(False)
        self.cancelButton.setToolTip(_fromUtf8(""))
        self.cancelButton.setStatusTip(_fromUtf8(""))
        self.cancelButton.setObjectName(_fromUtf8("cancelButton"))
        self.cancelButton.clicked.connect(self.__closeWindow)
        self.verticalLayout_18.addWidget(self.cancelButton)
# ------------------------------------- TITLE ----------------------------------------------
        self.titleLabel = QtGui.QLabel(self.centralwidget)
        self.titleLabel.setGeometry(QtCore.QRect(258, 10, 311, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("mry_KacstQurn"))
        font.setPointSize(20)
        self.titleLabel.setFont(font)

        self.titleLabel.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.titleLabel.setFrameShape(QtGui.QFrame.Panel)
        self.titleLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.titleLabel.setLineWidth(3)
        self.titleLabel.setTextFormat(QtCore.Qt.AutoText)
        self.titleLabel.setScaledContents(False)
        self.titleLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.titleLabel.setObjectName(_fromUtf8("titleLabel"))

# ------------------------------------- STATUS BAR ----------------------------------------------
        self.statusbar = QtGui.QStatusBar(RegisterForeign)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        RegisterForeign.setStatusBar(self.statusbar)
# ------------------------------------------------------------------------------------------
        self.retranslateUi(RegisterForeign) ## Method that atribuites text to each data
        QtCore.QMetaObject.connectSlotsByName(RegisterForeign)

# ---------------------------------- METHOD WITH TEXT ----------------------------------------------
    def retranslateUi(self, RegisterForeign):
        RegisterForeign.setWindowTitle(_translate("RegisterForeign", "Register", None))
        self.schedualLabel.setText(_translate("RegisterForeign", "<html><head/><body><p><span style=\" font-size:14pt;\">SCHEDUALE</span></p></body></html>", None))
        self.groupLabel.setText(_translate("RegisterForeign", "FOREIGN", None))
        self.nameLabel.setText(_translate("RegisterForeign", "Name", None))
        self.IDLabel.setText(_translate("RegisterForeign", "ID", None))

        self.saveButton.setText(_translate("RegisterForeign", "Save", None))
        self.cancelButton.setText(_translate("RegisterForeign", "Cancel", None))
        self.titleLabel.setText(_translate("RegisterForeign", "NEW REGISTRATION", None))
#-----------------------------------------------------------------------------------------------------------
def main():
    app = QtGui.QApplication(sys.argv)
    w = Ui_RegisterForeign(None)
    w.show()
    app.exec_()

if __name__ == '__main__':
    main()
