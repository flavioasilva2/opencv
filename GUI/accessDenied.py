# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtCore, QtGui, uic
import os
import cv2
import numpy as np
import threading
import time
import Queue
import deleteMenu
import registerMenu
import registerStudent
import registerTeacher
import registerCleaner
import registerGuard
import registerTechnician
import registerForeign

q = Queue.Queue()

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Capture(threading.Thread):
    __deleteButton = None
    __deleteButtonClickable = False
    __registerButton = None
    __registerButtonClickable = False
    __cam = None
    __queue = None
    __width = None
    __height = None
    __fps = None

    def setDeleteButton(self, button):
        self.__deleteButton = button

    def __setDeleteButtonEnabled(self):
        if self.__deleteButton != None:
            self.__deleteButtonClickable = True
            self.__deleteButton.setEnabled(self.__deleteButtonClickable)

    def __setDeleteButtonDisabled(self):
        if self.__deleteButton != None:
            self.__deleteButtonClickable = False
            self.__deleteButton.setEnabled(self.__deleteButtonClickable)

    def setRegisterButton(self, button):
        self.__registerButton = button

    def __setRegisterButtonEnabled(self):
        if self.__registerButton != None:
            self.__registerButtonClickable = True
            self.__registerButton.setEnabled(self.__registerButtonClickable)

    def __setRegisterButtonDisabled(self):
        if self.__registerButton != None:
            self.__registerButtonClickable = False
            self.__registerButton.setEnabled(self.__registerButtonClickable)

    def __init__(self, cam, queue, width, height, fps):
        threading.Thread.__init__(self)
        self.__cam = cam
        self.__queue = queue
        self.__width = width
        self.__height = height
        self.__fps = fps

    def run(self):
        haarFacePath = "haarcascades/haarcascade_frontalface_alt.xml"
        haarFace = cv2.CascadeClassifier(haarFacePath)

        capture = cv2.VideoCapture(self.__cam)
        capture.set(cv2.CAP_PROP_FRAME_WIDTH, self.__width)
        capture.set(cv2.CAP_PROP_FRAME_HEIGHT, self.__height)
        capture.set(cv2.CAP_PROP_FPS, self.__fps)

        tools = DetectionTools()
        while(self.__queue.qsize() < 10):
            retval, img = capture.read()
            face = tools.detect(img, haarFace)
            tools.drawRects(img, face, 1, (0, 255, 0))
            if len(face) != 0:
                self.__setDeleteButtonEnabled()
            else:
                self.__setDeleteButtonDisabled()

            if(self.__queue.qsize() < 10):
                self.__queue.put(img)
            else:
                capture.release()
                break

class DetectionTools:
    def detect(self, img, cascade):
        out = cascade.detectMultiScale(img, scaleFactor=1.1, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
        if len(out) == 0:
            return []
        return out

    def drawRects(self, img, pos, scale, color):
        for x1, y1, x2, y2 in pos:
            cv2.rectangle(img, (x1*scale, y1*scale), ((x2+x1)*scale, (y2+y1)*scale), color, 2)

class OwnImageWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(OwnImageWidget, self).__init__(parent)
        self.image = None

    def setImage(self, image):
        self.image = image
        sz = image.size()
        self.setMinimumSize(sz)
        self.update()

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        if self.image:
            qp.drawImage(QtCore.QPoint(0, 0), self.image)
        qp.end()

class MyWindowClass(QtGui.QMainWindow):
    __captureThread = None
    __captureThreadStarted = False
    __deleteMenuWindow = None
    __registerMenuWindow = None
    __studentWindow = None
    __teacherWindow = None
    __cleanerWindow = None
    __guardWindow = None
    __technicianWindow = None
    __foreignWindow = None

    def setCaptureThread(self, t):
        self.__captureThread = t
# delete
    def getDeleteMenuWindow(self):
        return self.__deleteMenuWindow

    def setDeleteMenuWindow(self, win):
        self.__deleteMenuWindow = win

    def deleteMenuWindowCallback(self):
        self.getDeleteMenuWindow().show()
# register
    def getRegisterMenuWindow(self):
        return self.__registerMenuWindow

    def setRegisterMenuWindow(self, win):
        self.__registerMenuWindow = win

    def registerMenuWindowCallback(self):
        self.getRegisterMenuWindow().show()
# student
    def getStudentWindow(self):
        return self.__studentWindow

    def setStudentWindow(self, win):
        self.__studentWindow = win

    def studentWindowCallback(self):
        self.getStudentWindow().show()
# teacher
    def getTeacherWindow(self):
        return self.__teacherWindow

    def setTeacherWindow(self, win):
        self.__teacherWindow = win

    def teacherWindowCallback(self):
        self.getTeacherWindow().show()
# cleaner
    def getCleanerWindow(self):
        return self.__cleanerWindow

    def setCleanerWindow(self, win):
        self.__cleanerWindow = win

    def cleanerWindowCallback(self):
        self.getCleanerWindow().show()
# guard
    def getGuardWindow(self):
        return self.__guardWindow

    def setGuardWindow(self, win):
        self.__guardWindow = win

    def guardWindowCallback(self):
        self.getGuardWindow().show()
# technician
    def getTechnicianWindow(self):
        return self.__technicianWindow

    def setTechnicianWindow(self, win):
        self.__technicianWindow = win

    def technicianWindowCallback(self):
        self.getTechnicianWindow().show()
# foreign
    def getForeignWindow(self):
        return self.__foreignWindow

    def setForeignWindow(self, win):
        self.__foreignWindow = win

    def foreignWindowCallback(self):
        self.getForeignWindow().show()

    def __init__(self, parent):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.window_width = self.ImgWidget.frameSize().width()
        self.window_height = self.ImgWidget.frameSize().height()
        self.ImgWidget = OwnImageWidget(self.ImgWidget)

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(1)

    def __startCaptureThread(self):
        if (self.__captureThreadStarted == False) and (self.__captureThread != None):
            self.__captureThread.start()
            self.__captureThreadStarted = True

    def update_frame(self):
        self.__startCaptureThread()
        if not q.empty():
            img = q.get()

            img_height, img_width, img_colors = img.shape
            scale_w = float(self.window_width) / float(img_width)
            scale_h = float(self.window_height) / float(img_height)
            scale = min([scale_w, scale_h])

            if scale == 0:
                scale = 1

            img = cv2.resize(img, None, fx=scale, fy=scale, interpolation = cv2.INTER_CUBIC)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            height, width, bpc = img.shape
            bpl = bpc * width
            image = QtGui.QImage(img.data, width, height, bpl, QtGui.QImage.Format_RGB888)
            self.ImgWidget.setImage(image)
# ----------------------------------------------------------------------------------------------------

# ---------------- Methods that add Widgets to the window class --------------------------------------
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(1286, 476)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 20, 641, 391))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.ImgWidget = QtGui.QWidget(self.groupBox)
        self.ImgWidget.setGeometry(QtCore.QRect(0, 20, 621, 361))
        self.ImgWidget.setObjectName(_fromUtf8("ImgWidget"))

        self.registerButton = QtGui.QPushButton(self.centralwidget)
        # self.registerButton.setEnabled(False)
        self.registerButton.setGeometry(QtCore.QRect(1064, 412, 85, 27))
        self.registerButton.setCheckable(False)
        self.registerButton.setChecked(False)
        self.registerButton.setAutoDefault(False)
        self.registerButton.setDefault(True)
        self.registerButton.setFlat(False)
        self.registerButton.setObjectName(_fromUtf8("registerButton"))
        self.registerButton.clicked.connect(self.registerMenuWindowCallback)

        self.deleteButton = QtGui.QPushButton(self.centralwidget)
        self.deleteButton.setEnabled(False)
        self.deleteButton.setGeometry(QtCore.QRect(1155, 412, 85, 27))
        self.deleteButton.setToolTip(_fromUtf8(""))
        self.deleteButton.setStatusTip(_fromUtf8(""))
        self.deleteButton.setAutoDefault(False)
        self.deleteButton.setDefault(False)
        self.deleteButton.setFlat(False)
        self.deleteButton.setObjectName(_fromUtf8("deleteButton"))
        # self.deleteButton.clicked.connect(self.deleteMenuWindowCallback)

        self.deniedLabel = QtGui.QLabel(self.centralwidget)
        self.deniedLabel.setGeometry(QtCore.QRect(662, 52, 341, 41))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.deniedLabel.setPalette(palette)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial Black"))
        font.setPointSize(20)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.NoAntialias)
        self.deniedLabel.setFont(font)
        self.deniedLabel.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.deniedLabel.setStyleSheet(_fromUtf8("permissionLabel->setStyleSheet(\"color: green\");"))
        self.deniedLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.deniedLabel.setObjectName(_fromUtf8("deniedLabel"))
        self.line_3 = QtGui.QFrame(self.centralwidget)
        self.line_3.setGeometry(QtCore.QRect(650, 0, 16, 461))
        self.line_3.setFrameShape(QtGui.QFrame.VLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(659, 229, 351, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.line_2 = QtGui.QFrame(self.centralwidget)
        self.line_2.setGeometry(QtCore.QRect(1010, 0, 16, 461))
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.accessLabel = QtGui.QLabel(self.centralwidget)
        self.accessLabel.setGeometry(QtCore.QRect(669, 10, 341, 41))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.accessLabel.setPalette(palette)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(25)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.NoAntialias)
        self.accessLabel.setFont(font)
        self.accessLabel.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.accessLabel.setStyleSheet(_fromUtf8("permissionLabel->setStyleSheet(\"color: green\");"))
        self.accessLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.accessLabel.setObjectName(_fromUtf8("accessLabel"))
        self.layoutWidget_3 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget_3.setGeometry(QtCore.QRect(660, 110, 351, 121))
        self.layoutWidget_3.setObjectName(_fromUtf8("layoutWidget_3"))
        self.horizontalLayout_12 = QtGui.QHBoxLayout(self.layoutWidget_3)
        self.horizontalLayout_12.setObjectName(_fromUtf8("horizontalLayout_12"))
        spacerItem = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_12.addItem(spacerItem)
        self.photoLabel_2 = QtGui.QLabel(self.layoutWidget_3)
        self.photoLabel_2.setFrameShape(QtGui.QFrame.NoFrame)
        self.photoLabel_2.setText(_fromUtf8(""))
        self.photoLabel_2.setPixmap(QtGui.QPixmap(_fromUtf8("homer.png")))
        self.photoLabel_2.setObjectName(_fromUtf8("photoLabel_2"))
        self.horizontalLayout_12.addWidget(self.photoLabel_2)
        spacerItem1 = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_12.addItem(spacerItem1)
        self.warningLabel = QtGui.QLabel(self.centralwidget)
        self.warningLabel.setGeometry(QtCore.QRect(1030, 140, 241, 221))
        self.warningLabel.setAlignment(QtCore.Qt.AlignJustify|QtCore.Qt.AlignVCenter)
        self.warningLabel.setObjectName(_fromUtf8("warningLabel"))
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(665, 245, 351, 201))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.verticalLayout_16 = QtGui.QVBoxLayout(self.widget)
        self.verticalLayout_16.setObjectName(_fromUtf8("verticalLayout_16"))
        self.horizontalLayout_10 = QtGui.QHBoxLayout()
        self.horizontalLayout_10.setObjectName(_fromUtf8("horizontalLayout_10"))
        self.courseLabel_4 = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.courseLabel_4.setFont(font)
        self.courseLabel_4.setObjectName(_fromUtf8("courseLabel_4"))
        self.horizontalLayout_10.addWidget(self.courseLabel_4)
        self.verticalLayout_16.addLayout(self.horizontalLayout_10)
        self.horizontalLayout_9 = QtGui.QHBoxLayout()
        self.horizontalLayout_9.setObjectName(_fromUtf8("horizontalLayout_9"))
        self.nameLabel_2 = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.nameLabel_2.setFont(font)
        self.nameLabel_2.setObjectName(_fromUtf8("nameLabel_2"))
        self.horizontalLayout_9.addWidget(self.nameLabel_2)
        self.verticalLayout_16.addLayout(self.horizontalLayout_9)
        self.horizontalLayout_8 = QtGui.QHBoxLayout()
        self.horizontalLayout_8.setObjectName(_fromUtf8("horizontalLayout_8"))
        self.IDLabel_2 = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.IDLabel_2.setFont(font)
        self.IDLabel_2.setObjectName(_fromUtf8("IDLabel_2"))
        self.horizontalLayout_8.addWidget(self.IDLabel_2)
        self.verticalLayout_16.addLayout(self.horizontalLayout_8)
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        self.courseLabel_5 = QtGui.QLabel(self.widget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.courseLabel_5.setFont(font)
        self.courseLabel_5.setObjectName(_fromUtf8("courseLabel_5"))
        self.horizontalLayout_7.addWidget(self.courseLabel_5)
        self.verticalLayout_16.addLayout(self.horizontalLayout_7)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.groupBox.setTitle(_translate("MainWindow", "Video", None))
        self.registerButton.setText(_translate("MainWindow", "Register", None))
        self.deleteButton.setText(_translate("MainWindow", "Delete", None))
        self.deniedLabel.setText(_translate("MainWindow", "DENIED", None))
        self.accessLabel.setText(_translate("MainWindow", "ACCESS:", None))
        self.warningLabel.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600;\">This person does not</span></p><p align=\"center\"><span style=\" font-size:18pt; font-weight:600;\">have permission to</span></p><p align=\"center\"><span style=\" font-size:18pt; font-weight:600;\">access.</span></p><p align=\"center\"><span style=\" font-size:18pt;\"><br/></span></p><p align=\"center\"><span style=\" font-size:18pt; font-weight:600;\">Please register.</span></p></body></html>", None))
        self.courseLabel_4.setText(_translate("MainWindow", "Group:", None))
        self.nameLabel_2.setText(_translate("MainWindow", "Name:", None))
        self.IDLabel_2.setText(_translate("MainWindow", "ID:", None))
        self.courseLabel_5.setText(_translate("MainWindow", "Course:", None))
#-----------------------------------------------------------------------------------------------------------
def initalizeRegisterProfileWindow(mainWindow):
    reg = Ui_Register(None)
    captureT = Capture (0, q, 1920, 1080, 30)
    reg.setCaptureThread(captureT)
    reg.hide()
    mainWindow.setRegisterProfileWindow(reg)

# def initalizedeleteMenuWindow(mainWindow):
#     delete = Ui_deleteMenu(None)
#     captureT = Capture (0, q, 1920, 1080, 30)
#     delete.setCaptureThread(captureT)
#     delete.hide()
#     mainWindow.setDeleteMenuWindow(delete)

def main():
    app = QtGui.QApplication(sys.argv)
    accessWindow = MyWindowClass(None)
    captureT = Capture(0, q, 1920, 1080, 30)
    accessWindow.setCaptureThread(captureT)
    accessWindow.show()

    registerMenuWindow = registerMenu.Ui_Register()
    accessWindow.setRegisterMenuWindow(registerMenuWindow)

    studentWindow = registerStudent.Ui_RegisterStud()
    accessWindow.getRegisterMenuWindow().setStudentWindow(studentWindow)

    teacherWindow = registerTeacher.Ui_RegisterProf()
    accessWindow.getRegisterMenuWindow().setTeacherWindow(teacherWindow)

    cleanerWindow = registerCleaner.Ui_RegisterClean()
    accessWindow.getRegisterMenuWindow().setCleanerWindow(cleanerWindow)

    guardWindow = registerGuard.Ui_RegisterGuard()
    accessWindow.getRegisterMenuWindow().setGuardWindow(guardWindow)

    technicianWindow = registerTechnician.Ui_RegisterTec()
    accessWindow.getRegisterMenuWindow().setTechnicianWindow(technicianWindow)

    foreignWindow = registerForeign.Ui_RegisterForeign()
    accessWindow.getRegisterMenuWindow().setForeignWindow(foreignWindow)

    app.exec_()

if __name__ == '__main__':
    main()
