# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtCore, QtGui, uic
import os
import cv2
import numpy as np
import threading
import time
import Queue
import confirmDeleteOther

q = Queue.Queue()

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Capture(threading.Thread):
    __deleteButton = None
    __deleteButtonClickable = False
    __cam = None
    __queue = None
    __width = None
    __height = None
    __fps = None

    def setDeleteButton(self, button):
        self.__deleteButton = button

    def __setDeleteButtonEnabled(self):
        if self.__deleteButton != None:
            self.__deleteButtonClickable = True
            self.__deleteButton.setEnabled(self.__deleteButtonClickable)

    def __setDeleteButtonDisabled(self):
        if self.__deleteButton != None:
            self.__deleteButtonClickable = False
            self.__deleteButton.setEnabled(self.__deleteButtonClickable)

    def __init__(self, cam, queue, width, height, fps):
        threading.Thread.__init__(self)
        self.__cam = cam
        self.__queue = queue
        self.__width = width
        self.__height = height
        self.__fps = fps

    def run(self):
        haarFacePath = "haarcascades/haarcascade_frontalface_alt.xml"
        haarFace = cv2.CascadeClassifier(haarFacePath)

        capture = cv2.VideoCapture(self.__cam)
        capture.set(cv2.CAP_PROP_FRAME_WIDTH, self.__width)
        capture.set(cv2.CAP_PROP_FRAME_HEIGHT, self.__height)
        capture.set(cv2.CAP_PROP_FPS, self.__fps)

        tools = DetectionTools()
        while(self.__queue.qsize() < 10):
            retval, img = capture.read()
            face = tools.detect(img, haarFace)
            tools.drawRects(img, face, 1, (0, 255, 0))
            if len(face) != 0:
                self.__setDeleteButtonEnabled()
            else:
                self.__setDeleteButtonDisabled()

            if(self.__queue.qsize() < 10):
                self.__queue.put(img)
            else:
                capture.release()
                break

class DetectionTools:
    def detect(self, img, cascade):
        out = cascade.detectMultiScale(img, scaleFactor=1.1, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
        if len(out) == 0:
            return []
        return out

    def drawRects(self, img, pos, scale, color):
        for x1, y1, x2, y2 in pos:
            cv2.rectangle(img, (x1*scale, y1*scale), ((x2+x1)*scale, (y2+y1)*scale), color, 2)

class OwnImageWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(OwnImageWidget, self).__init__(parent)
        self.image = None

    def setImage(self, image):
        self.image = image
        sz = image.size()
        self.setMinimumSize(sz)
        self.update()

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        if self.image:
            qp.drawImage(QtCore.QPoint(0, 0), self.image)
        qp.end()

class MyWindowClass(QtGui.QMainWindow):
    # __capture_thread = None
    __captureThread = None
    __captureThreadStarted = False
    __deleteProfileWindow = None

    def setCaptureThread(self, t):
        self.__captureThread = t

    def getDeleteProfileWindow(self):
        return self.__deleteProfileWindow

    def setDeleteProfileWindow(self, win):
        self.__deleteProfileWindow = win

    def deleteProfileWindowCallback(self):
        self.getDeleteProfileWindow().show()

    def __init__(self, parent):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.window_width = self.ImgWidget.frameSize().width()
        self.window_height = self.ImgWidget.frameSize().height()
        self.ImgWidget = OwnImageWidget(self.ImgWidget)

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(1)

    def __startCaptureThread(self):
        if (self.__captureThreadStarted == False) and (self.__captureThread != None):
            self.__captureThread.start()
            self.__captureThreadStarted = True

    def update_frame(self):
        self.__startCaptureThread()
        if not q.empty():
            img = q.get()

            img_height, img_width, img_colors = img.shape
            scale_w = float(self.window_width) / float(img_width)
            scale_h = float(self.window_height) / float(img_height)
            scale = min([scale_w, scale_h])

            if scale == 0:
                scale = 1

            img = cv2.resize(img, None, fx=scale, fy=scale, interpolation = cv2.INTER_CUBIC)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            height, width, bpc = img.shape
            bpl = bpc * width
            image = QtGui.QImage(img.data, width, height, bpl, QtGui.QImage.Format_RGB888)
            self.ImgWidget.setImage(image)
# ----------------------------------------------------------------------------------------------------

# ---------------- Methods that add Widgets to the window class --------------------------------------
    def setupUi(self, permOther):
        permOther.setObjectName(_fromUtf8("permOther"))
        permOther.resize(1288, 476)
        self.centralwidget = QtGui.QWidget(permOther)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 20, 641, 391))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.ImgWidget = QtGui.QWidget(self.groupBox)
        self.ImgWidget.setGeometry(QtCore.QRect(0, 30, 621, 361))
        self.ImgWidget.setObjectName(_fromUtf8("ImgWidget"))
        self.accessLabel = QtGui.QLabel(self.centralwidget)
        self.accessLabel.setGeometry(QtCore.QRect(669, 10, 341, 41))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.accessLabel.setPalette(palette)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(25)
        font.setBold(True)
        font.setWeight(75)
        font.setStyleStrategy(QtGui.QFont.NoAntialias)
        self.accessLabel.setFont(font)
        self.accessLabel.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.accessLabel.setStyleSheet(_fromUtf8("permissionLabel->setStyleSheet(\"color: green\");"))
        self.accessLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.accessLabel.setObjectName(_fromUtf8("accessLabel"))
        self.line_2 = QtGui.QFrame(self.centralwidget)
        self.line_2.setGeometry(QtCore.QRect(1010, 0, 16, 451))
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.line_3 = QtGui.QFrame(self.centralwidget)
        self.line_3.setGeometry(QtCore.QRect(650, 0, 16, 451))
        self.line_3.setFrameShape(QtGui.QFrame.VLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(659, 229, 351, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(660, 110, 351, 121))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.horizontalLayout_6 = QtGui.QHBoxLayout(self.widget)
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        spacerItem = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem)
        self.photoLabel = QtGui.QLabel(self.widget)
        self.photoLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.photoLabel.setText(_fromUtf8(""))
        self.photoLabel.setPixmap(QtGui.QPixmap(_fromUtf8("homer.png")))
        self.photoLabel.setObjectName(_fromUtf8("photoLabel"))
        self.horizontalLayout_6.addWidget(self.photoLabel)
        spacerItem1 = QtGui.QSpacerItem(118, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem1)
        self.grantedLabel = QtGui.QLabel(self.centralwidget)
        self.grantedLabel.setGeometry(QtCore.QRect(669, 50, 341, 41))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.grantedLabel.setPalette(palette)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial Black"))
        font.setPointSize(20)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.NoAntialias)
        self.grantedLabel.setFont(font)
        self.grantedLabel.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.grantedLabel.setStyleSheet(_fromUtf8("permissionLabel->setStyleSheet(\"color: green\");"))
        self.grantedLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.grantedLabel.setObjectName(_fromUtf8("grantedLabel"))

        self.registerButton = QtGui.QPushButton(self.centralwidget)
        self.registerButton.setEnabled(False)
        self.registerButton.setGeometry(QtCore.QRect(1058, 422, 96, 27))
        self.registerButton.setCheckable(False)
        self.registerButton.setChecked(False)
        self.registerButton.setObjectName(_fromUtf8("registerButton"))

        self.deleteButton = QtGui.QPushButton(self.centralwidget)
        # self.deleteButton.setEnabled(False)
        self.deleteButton.setGeometry(QtCore.QRect(1160, 422, 85, 27))
        self.deleteButton.setToolTip(_fromUtf8(""))
        self.deleteButton.setStatusTip(_fromUtf8(""))
        self.deleteButton.setAutoDefault(False)
        self.deleteButton.setDefault(False)
        self.deleteButton.setFlat(False)
        self.deleteButton.setObjectName(_fromUtf8("deleteButton"))
        self.deleteButton.clicked.connect(self.deleteProfileWindowCallback)

        self.widget1 = QtGui.QWidget(self.centralwidget)
        self.widget1.setGeometry(QtCore.QRect(662, 242, 351, 201))
        self.widget1.setObjectName(_fromUtf8("widget1"))

        self.verticalLayout_3 = QtGui.QVBoxLayout(self.widget1)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.courseLabel_2 = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.courseLabel_2.setFont(font)
        self.courseLabel_2.setObjectName(_fromUtf8("courseLabel_2"))
        self.horizontalLayout.addWidget(self.courseLabel_2)
        self.varGroup = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varGroup.setFont(font)
        self.varGroup.setObjectName(_fromUtf8("varGroup"))
        self.horizontalLayout.addWidget(self.varGroup)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.nameLabel = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.nameLabel.setFont(font)
        self.nameLabel.setObjectName(_fromUtf8("nameLabel"))
        self.horizontalLayout_2.addWidget(self.nameLabel)
        self.varName = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varName.setFont(font)
        self.varName.setObjectName(_fromUtf8("varName"))
        self.horizontalLayout_2.addWidget(self.varName)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.IDLabel = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.IDLabel.setFont(font)
        self.IDLabel.setObjectName(_fromUtf8("IDLabel"))
        self.horizontalLayout_3.addWidget(self.IDLabel)
        self.varID = QtGui.QLabel(self.widget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varID.setFont(font)
        self.varID.setObjectName(_fromUtf8("varID"))
        self.horizontalLayout_3.addWidget(self.varID)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        self.splitter_2 = QtGui.QSplitter(self.centralwidget)
        self.splitter_2.setGeometry(QtCore.QRect(1029, 50, 241, 351))
        self.splitter_2.setOrientation(QtCore.Qt.Vertical)
        self.splitter_2.setObjectName(_fromUtf8("splitter_2"))
        self.schedualLabel = QtGui.QLabel(self.splitter_2)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.schedualLabel.setFont(font)
        self.schedualLabel.setObjectName(_fromUtf8("schedualLabel"))
        self.splitter = QtGui.QSplitter(self.splitter_2)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.widget2 = QtGui.QWidget(self.splitter)
        self.widget2.setObjectName(_fromUtf8("widget2"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.widget2)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.monLabel = QtGui.QLabel(self.widget2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.monLabel.setFont(font)
        self.monLabel.setObjectName(_fromUtf8("monLabel"))
        self.verticalLayout_2.addWidget(self.monLabel)
        self.tueLabel = QtGui.QLabel(self.widget2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.tueLabel.setFont(font)
        self.tueLabel.setObjectName(_fromUtf8("tueLabel"))
        self.verticalLayout_2.addWidget(self.tueLabel)
        self.wedLabel = QtGui.QLabel(self.widget2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.wedLabel.setFont(font)
        self.wedLabel.setObjectName(_fromUtf8("wedLabel"))
        self.verticalLayout_2.addWidget(self.wedLabel)
        self.thuLabel = QtGui.QLabel(self.widget2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.thuLabel.setFont(font)
        self.thuLabel.setObjectName(_fromUtf8("thuLabel"))
        self.verticalLayout_2.addWidget(self.thuLabel)
        self.friLabel = QtGui.QLabel(self.widget2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.friLabel.setFont(font)
        self.friLabel.setObjectName(_fromUtf8("friLabel"))
        self.verticalLayout_2.addWidget(self.friLabel)
        self.satLabel = QtGui.QLabel(self.widget2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.satLabel.setFont(font)
        self.satLabel.setObjectName(_fromUtf8("satLabel"))
        self.verticalLayout_2.addWidget(self.satLabel)
        self.sunLabel = QtGui.QLabel(self.widget2)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.sunLabel.setFont(font)
        self.sunLabel.setObjectName(_fromUtf8("sunLabel"))
        self.verticalLayout_2.addWidget(self.sunLabel)
        self.widget3 = QtGui.QWidget(self.splitter)
        self.widget3.setObjectName(_fromUtf8("widget3"))
        self.verticalLayout = QtGui.QVBoxLayout(self.widget3)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.varMonHours = QtGui.QLabel(self.widget3)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varMonHours.setFont(font)
        self.varMonHours.setObjectName(_fromUtf8("varMonHours"))
        self.verticalLayout.addWidget(self.varMonHours)
        self.varTueHours = QtGui.QLabel(self.widget3)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varTueHours.setFont(font)
        self.varTueHours.setObjectName(_fromUtf8("varTueHours"))
        self.verticalLayout.addWidget(self.varTueHours)
        self.varWedHours = QtGui.QLabel(self.widget3)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varWedHours.setFont(font)
        self.varWedHours.setObjectName(_fromUtf8("varWedHours"))
        self.verticalLayout.addWidget(self.varWedHours)
        self.varThuHours = QtGui.QLabel(self.widget3)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varThuHours.setFont(font)
        self.varThuHours.setObjectName(_fromUtf8("varThuHours"))
        self.verticalLayout.addWidget(self.varThuHours)
        self.varFriHours = QtGui.QLabel(self.widget3)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varFriHours.setFont(font)
        self.varFriHours.setObjectName(_fromUtf8("varFriHours"))
        self.verticalLayout.addWidget(self.varFriHours)
        self.varSatHours = QtGui.QLabel(self.widget3)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varSatHours.setFont(font)
        self.varSatHours.setObjectName(_fromUtf8("varSatHours"))
        self.verticalLayout.addWidget(self.varSatHours)
        self.varSunHours = QtGui.QLabel(self.widget3)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Sawasdee"))
        font.setBold(True)
        font.setWeight(75)
        self.varSunHours.setFont(font)
        self.varSunHours.setObjectName(_fromUtf8("varSunHours"))
        self.verticalLayout.addWidget(self.varSunHours)
        permOther.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(permOther)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        permOther.setStatusBar(self.statusbar)

        self.retranslateUi(permOther)
        QtCore.QMetaObject.connectSlotsByName(permOther)

    def retranslateUi(self, permOther):
        permOther.setWindowTitle(_translate("permOther", "Permission", None))
        self.groupBox.setTitle(_translate("permOther", "Video", None))
        self.accessLabel.setText(_translate("permOther", "ACCESS:", None))
        self.grantedLabel.setText(_translate("permOther", "GRANTED", None))
        self.registerButton.setText(_translate("permOther", "Register", None))
        self.deleteButton.setText(_translate("permOther", "Delete", None))
        self.courseLabel_2.setText(_translate("permOther", "Group:", None))
        self.varGroup.setText(_translate("permOther", "CLEANER", None))
        self.nameLabel.setText(_translate("permOther", "Name:", None))
        self.varName.setText(_translate("permOther", "HOMER SIMPSON", None))
        self.IDLabel.setText(_translate("permOther", "ID:", None))
        self.varID.setText(_translate("permOther", "17/0160000", None))
        self.schedualLabel.setText(_translate("permOther", "Scheduale:", None))
        self.monLabel.setText(_translate("permOther", "Mon", None))
        self.tueLabel.setText(_translate("permOther", "Tue", None))
        self.wedLabel.setText(_translate("permOther", "Wed", None))
        self.thuLabel.setText(_translate("permOther", "Thu", None))
        self.friLabel.setText(_translate("permOther", "Fri", None))
        self.satLabel.setText(_translate("permOther", "Sat", None))
        self.sunLabel.setText(_translate("permOther", "Sun", None))
        self.varMonHours.setText(_translate("permOther", "-- : -- | -- : --", None))
        self.varTueHours.setText(_translate("permOther", "-- : -- | -- : --", None))
        self.varWedHours.setText(_translate("permOther", "-- : -- | -- : --", None))
        self.varThuHours.setText(_translate("permOther", "-- : -- | -- : --", None))
        self.varFriHours.setText(_translate("permOther", "-- : -- | -- : --", None))
        self.varSatHours.setText(_translate("permOther", "-- : -- | -- : --", None))
        self.varSunHours.setText(_translate("permOther", "-- : -- | -- : --", None))
#-----------------------------------------------------------------------------------------------------------
def initalizeDeleteProfileWindow(mainWindow):
    delete = Ui_DeleteProfile(None)
    captureT = Capture (0, q, 1920, 1080, 30)
    delete.setCaptureThread(captureT)
    delete.hide()
    mainWindow.setdeleteProfileWindow(delete)

def main():
    app = QtGui.QApplication(sys.argv)
    accessWindow = MyWindowClass(None)
    captureT = Capture(0, q, 1920, 1080, 30)
    accessWindow.setCaptureThread(captureT)
    accessWindow.show()
    deleteWindow = confirmDeleteOther.Ui_DeleteProfile()
    accessWindow.setDeleteProfileWindow(deleteWindow)
    app.exec_()

if __name__ == '__main__':
    main()
